<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

	#*****************************************************************************
	#
	# wtplogocontest.php
	#
	# Author: 		Lawrence Mandel
	# Date:			2006-01-31
	#
	# Description: The WTP logo contest page.
	#
	#
	#****************************************************************************
	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "WTP Logo Contest";
	$pageKeywords	= "WTP, Web Tools Platform, Logo, Contest, Eclipse, Submission, Vote, Comment, Entry";
	$pageAuthor		= "Lawrence Mandel";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# $Nav->addNavSeparator("My Page Links", 	"downloads.php");
	# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

	# End: page-specific settings
	#
		
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML

	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<p>
			Like most projects, the Eclipse Web Tools Platform (WTP) is in need of a logo to allow for branding on the WTP website, in
			articles, in presentations, and other WTP opportunities. WTP is calling for your <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=122593">logo submissions</a>
			and <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=122593">feedback on current submissions</a>.
		</p>
		<div class="homeitem3col">
			<h3>Final Logo Selection</h3>
			The WTP PMC has narrowed the logo selection to the WTP gears logo by Artur Filipiak
			and wrench on world submitted by Sukma Nugraha. To help the selection process, here
			are the logos as they will be used on the WTP home page and in presentations. Comments
			are still accepted <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=122593">on
			the bug report</a>.
			<ul>
				<li>
					WTP gears:<br />
					<a href="community/logocontest/wtplogogearssample.php">WTP Home Page 1</a><br />
					<a href="community/logocontest/wtplogogearssample2.php">WTP Home Page 2</a><br />
					<a href="community/logocontest/wtplogogearssample.ppt">WTP Presentation 1</a><br />
					<a href="community/logocontest/wtplogogearssample2.ppt">WTP Presentation 2</a><br />
				</li>
				<li>
					WTP wrench on world:<br />
					<a href="community/logocontest/wtplogowrenchsample.php">WTP Home Page 1</a><br />
					<a href="community/logocontest/wtplogowrenchsample2.php">WTP Home Page 2</a><br />
					<a href="community/logocontest/wtplogowrenchsample.ppt">WTP Presentation 1</a><br />
					<a href="community/logocontest/wtplogowrenchsample2.ppt">WTP Presentation 2</a><br />
				</li>
			</ul>
		</div>
		<p>
			Logos can be submitted by creating an attachment to <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=122593">bug 122593</a>.
		</p>
		<p>
			Your feedback about these logos is appreciated. Please comment on <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=122593">bug 122593</a>.
		</p>
		<p>
			<b>The submission deadline is Feb. 14, 2006.</b>
		</p>
		<p>
			The WTP logo contest is open to everyone. The WTP PMC will make the final logo 
			decision.
		</p>
		<h2>A few details about submissions</h2>
		<p>
			To make it easier for others to comment on your submission, please include one logo per
			attachment. When designing a logo, some things to keep in mind are novelty of design and representation of
			WTP. Also, please remember that the selected logo will be used in print and in various sizes.
			And, as always when contributing to Eclipse, ensure you have the appropriate rights to
			your submissions. Do NOT copy or modify images created by others without first obtaining
			rights to those images. If you do have the rights to modify an existing image, please include
			a reference to the original image and a declaration of the rights to copy the image in your
			submission.
		</p>
		<h1>Logo Submissions</h1>
		<p>
			This list was created on Feb. 13, 2006. Please see <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=122593">bug 122593</a> for the current submissions.<br />
			Some of the submissions contain large files and may take time to load.
		</p>
		<!-- SUBMISSIONS -->
		<hr size="1" />
		<a name="32449"></a><h3>1. Globe Logos (submitted by Scott Oja)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32449&action=view" /></p>
		<hr size="1" />
		<a name="32741"></a><h3>2. Globe Logos - Updated (submitted by Scott Oja)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32741&action=view" /></p>
		<hr size="1" />
		<a name="32647"></a><h3>3. Potential Conceptual Icons (submitted by David Williams)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32647&action=view" /></p>
		<hr size="1" />
		<a name="32790"></a><h3>4. Spider-A (submitted by Naci Dai)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32790&action=view" /></p>
		<hr size="1" />
		<a name="32791"></a><h3>5. Spider-B Wireframe (submitted by Naci Dai)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32791&action=view" /></p>
		<hr size="1" />
		<a name="32792"></a><h3>6. Spider-C Globe (submitted by Naci Dai)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32792&action=view" /></p>
		<hr size="1" />
		<a name="32818"></a><h3>7. Spider-D (submitted by Naci Dai)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32818&action=view" /></p>
		<hr size="1" />
		<a name="32819"></a><h3>8. Spider-E and Spider-E1 (submitted by Naci Dai)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32819&action=view" width="100%"/></p>
		<a name="32820"></a><hr size="1" />
		<h3>9. Spider-F and Spider-F1 (submitted by Naci Dai)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32820&action=view" width="100%"/></p>
		<hr size="1" />
		<a name="32821"></a><h3>10. Spider-G (submitted by Naci Dai)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=32821&action=view" /></p>
		<hr size="1" />
		<a name="32170"></a><h3>11. Vector based logo with cartoon spider (inspired by Tortoise SVN turtle) (submitted by Andy Hoffman)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33170&action=view" /></p>
		<hr size="1" />
		<a name="33187"></a><h3>12. A submission coordinating the idea of other submissions (submitted by Michael Elder)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33187&action=view" /></p>
		<hr size="1" />
		<a name="33287"></a><h3>13. Food for thought - to get you thinking! (submitted by Randy Hudson)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33287&action=view" /></p>
		<hr size="1" />
		<a name="33572"></a><h3>14. Wrench on world (submitted by Sukma Nugraha)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33572&action=view" /></p>
		<hr size="1" />
		<a name="33749"></a><h3>15. A new take on an old idea (submitted by David Sanders)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33749&action=view" /></p>
		<hr size="1" />
		<a name="33853"></a><h3>16. The eclipse family (submitted by Rutger)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33853&action=view" /></p>
		<hr size="1" />
		<a name="33862"></a><h3>17. Eclipse orbit (submitted by Rutger)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33862&action=view" /></p>
		<hr size="1" />
		<a name="33865"></a><h3>18. WTP identifier proposal (submitted by Janet Mockler)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33865&action=view" /></p>
		<hr size="1" />
		<a name="33872"></a><h3>19. WTP and Eclipse == bolt and nut (submitted by Hendrik Ebel)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33872&action=view" /></p>
		<hr size="1" />
		<a name="33914"></a><h3>20. Web Tools Platform (submitted by Aaron)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=33914&action=view" /></p>
		<hr size="1" />
		<a name="34002"></a><h3>21. no-wrench-1  (submitted by Eli Spizzichino)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34002&action=view" /></p>
		<hr size="1" />
		<a name="34003"></a><h3>22. no-wrench-2  (submitted by Eli Spizzichino)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34003&action=view" /></p>
		<hr size="1" />
		<a name="34249"></a><h3>23. wtp_image  (submitted by Chris W)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34249&action=view" /></p>
		<hr size="1" />
		<a name="34250"></a><h3>24. WTP Logo1  (submitted by Francisco Becerra)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34250&action=view" /></p>
		<hr size="1" />
		<a name="34251"></a><h3>25. WTP Logo2  (submitted by Francisco Becerra)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34251&action=view" /></p>
		<hr size="1" />
		<a name="34312"></a><h3>26. no-wrench-3  (submitted by Eli Spizzichino)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34312&action=view" width="100%"/></p>
		<hr size="1" />
		<a name="34464"></a><h3>27. WTP Logo - Image only  (submitted by Roger Dudler)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34464&action=view"/></p>
		<hr size="1" />
		<a name="34467"></a><h3>28. WTP Logo Showcase  (submitted by Roger Dudler)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34467&action=view"/></p>
		<hr size="1" />
		<a name="34519"></a><h3>29. More Wrenches  (submitted by Francisco Becerra)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34519&action=view"/></p>
		<hr size="1" />
		<a name="34551"></a><h3>30. WTP Gears  (submitted by Artur Filipiak)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34551&action=view"/></p>
		<hr size="1" />
		<a name="34553"></a><h3>31. WTP Gears in Red  (submitted by Artur Filipiak)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34553&action=view"/></p>
		<hr size="1" />
		<a name="34563"></a><h3>32. Simple Wrench   (submitted by Tri Seprian Damayanto)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34563&action=view"/></p>
		<hr size="1" />
		<a name="34564"></a><h3>33. Eclipse Shape   (submitted by Tri Seprian Damayanto)</h3>
		<p><img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=34564&action=view"/></p>
	</div>

	
EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
