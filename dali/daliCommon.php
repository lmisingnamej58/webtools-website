<?php
 function &read_file($file_name) {
	$fp = fopen($file_name, "r");
	$file_contents = fread($fp, filesize($file_name));
	fclose($fp);
	return $file_contents;
}

# * * * * *   $wtpTopButtons   * * * * * #
$wtpTopButtons = <<<EOHTML
<link rel="stylesheet" type="text/css" href="/webtools/customMain.css" />
<link rel="shortcut icon" href="/webtools/images/favicon.ico">

<div class="logoBanner"><a href="/webtools/"><img src="/webtools/images/wtp_logo_2010.png" /></a></div>
<div id="bigbuttons">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<div class="bigbuttonsAbout">
					<a href="/projects/project_summary.php?projectid=webtools"><div class="bigbuttonsTitleLink"></div></a>
					<span><a href="http://dev.eclipse.org/viewcvs/index.cgi/?root=WebTools_Project">Source Repository</a>, PMC,<br/>Committers and more...</span>
				</div>
			</td>
			<td>
				<div class="bigbuttonsDocumentation">
					<a href="/webtools/documentation/"><div class="bigbuttonsTitleLink"></div></a>
					<span>
						<a href="/webtools/documentation/">Help Docs</a>,
						<a href="https://wiki.eclipse.org/WTP_FAQ"> FAQ</a>,
						<a href="/projects/project-plan.php?projectid=webtools.dali"> Plans,</a><br/>
						<a href="/webtools/development/news/main.php">New and Noteworthy</a>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="bigbuttonsCommunity">
					<a href="/webtools/community/"><div class="bigbuttonsTitleLink"></div></a>
					<span>
						<a href="https://wiki.eclipse.org/Dali">Wiki</a>,
						<a href="/forums/index.php/f/113/">Forum</a>,<br/>
						<a href="https://dev.eclipse.org/mailman/listinfo/dali-dev">Mailing List</a>,
						<a href="/webtools/community/resources/"> Resources</a>,<br/>
						<a href="/webtools/community/education/">Education</a>
					</span>
				</div>
			</td>
			<td>
				<div class="bigbuttonsDownloads">
					<a href="http://download.eclipse.org/webtools/downloads/"><div class="bigbuttonsTitleLink"></div></a>
					<span>
						<a href="http://download.eclipse.org/webtools/downloads/">Builds</a>,
						<a href="http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/indigosr2">Java EE</a> and <a href="http://www.eclipse.org/downloads/packages/eclipse-ide-javascript-web-developers/indigosr2">JavaScript Web</a> packages, 
						<a href="http://download.eclipse.org/releases/indigo/">Indigo Update Site</a>
					</span>
				</div>
			</td>
		</tr>
	</table>
</div>
EOHTML;
# * * * * *   $wtpTopButtons END   * * * * * #
$aboutLinks = <<<EOHTML
<style>#leftcol{display:block !important;}#midcolumn{width:725px !important;}</style>
EOHTML;
?>