<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0"
    exclude-result-prefixes="xalan date"
    xmlns:xalan="http://xml.apache.org/xalan"
    xmlns:date="http://exslt.org/dates-and-times">
	<xsl:include href="news.xsl" />
    <xsl:include href="wtp-common.xsl"/>
    <xsl:variable name="root" select="'a'"/>

	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="/news">
		<xsl:variable name="items">
			<xsl:apply-templates select="." mode="listcreation">
             <xsl:with-param name="all">true</xsl:with-param>
             <xsl:with-param name="alldates">true</xsl:with-param>
            </xsl:apply-templates>
		</xsl:variable>

	<xsl:call-template name="rss20">
		<xsl:with-param name="title">News</xsl:with-param>
		<xsl:with-param name="ttl" select="1440" />
		<!--xsl:with-param name="description" select="" /-->
		<xsl:with-param name="items" select="xalan:nodeset($items)" />
	</xsl:call-template>
    <!-- xsl:apply-templates select="./*"></xsl:apply-templates-->
</xsl:template>

	<xsl:template name="rss20">
		<xsl:param name="title" />
        <xsl:param name="ttl" />
        <xsl:param name="items"/>

		<rss version="2.0">
			<channel>
				<title>Eclipse Web Tools Platform Project: <xsl:value-of select="$title"/></title>
				<link>http://www.eclipse.org/webtools/</link>
				<description>
					This RSS feed contains the latest news from the Eclipse Web Tools Platform (WTP) project. The 
                    Eclipse WTP project contains Web tools and frameworks for the Eclipse platform.
				</description>
				<language>en-us</language>
				<copyright>
					Copyright (c) 2001, 2007 IBM Corporation and others.
                    All rights reserved. This program and the accompanying materials
                    are made available under the terms of the Eclipse Public License v1.0
                    which accompanies this distribution, and is available at
                    http://www.eclipse.org/legal/epl-v10.html
  
                    Contributors:
                    IBM Corporation - initial API and implementation
				</copyright>
				<lastBuildDate>
					<xsl:call-template name="formatdate">
                        <xsl:with-param name="date" select="date:date-time()"/>
                      </xsl:call-template>
				</lastBuildDate>
				<category>Eclipse Web Tools Platform (WTP) Project: <xsl:value-of select="$title"/></category>
                <!-- xsl:if test="$ttl">
				  <ttl><xsl:value-of select="$ttl"/></ttl>
                </xsl:if-->
				<!-- image>
					<title>Eclipse Web Tools Platform (WTP) Project  <xsl:value-of select="$title"/></title>
					<url>
						http://static.wired.com/news/images/netcenterb.gif
					</url>
					<link>http://www.wired.com/</link>
				</image-->
                <xsl:for-each select="$items/item">
				<item>
					<title><xsl:value-of select="@title"/></title>
					<pubDate><xsl:value-of select="date:day-abbreviation(@date)"/>,<xsl:text> </xsl:text><xsl:value-of select="date:day-in-month(@date)"/><xsl:text> </xsl:text><xsl:value-of select="date:month-abbreviation(@date)"/><xsl:text> </xsl:text><xsl:value-of select="date:year(@date)"/><xsl:text> </xsl:text><xsl:value-of select="date:time(@date)"/><xsl:text> </xsl:text>UTC</pubDate>
					<link><xsl:value-of select="@link"/></link>
					<!--guid isPermaLink="true">
						http://www.wired.com/news/mac/0,2125,66460,00.html?tw=rss.GAD
					</guid-->
					<description>
                      <xsl:apply-templates select="*|text()" mode="rss"/>
					</description>
				</item>
                </xsl:for-each>
			</channel>
		</rss>
	</xsl:template>

<xsl:template name="formatdate">
<xsl:param name="date"/>
  <xsl:value-of select="date:day-abbreviation($date)"/>, <xsl:value-of select="date:day-in-month($date)"/><xsl:text> </xsl:text><xsl:value-of select="date:month-abbreviation($date)"/><xsl:text> </xsl:text><xsl:value-of select="date:year($date)"/><xsl:text> </xsl:text><xsl:value-of select="date:time($date)"/>
</xsl:template>

<xsl:template match="text()" mode="rss">
  <xsl:copy>
	<xsl:apply-templates select="text()" mode="rss"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="*|@*" mode="rss">
  <xsl:apply-templates select="text()" mode="rss"/>
</xsl:template>

<xsl:template match="*|@*|text()"/>

</xsl:stylesheet>