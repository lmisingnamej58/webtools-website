<?php

# Set the theme for your project's web pages.
# See the Committer Tools "How Do I" for list of themes
# https://dev.eclipse.org/committers/
# Optional: defaults to system theme
# miasma
$theme = "Nova";
header("Content-type: text/html; charset=utf-8");
$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/wtpnova.css\" />\n");

# Define your project-wide Nav bars here.
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# these are optional
$Nav->setLinkList(null);
$Nav->addCustomNav( "About This Project", "/projects/project_summary.php?projectid=webtools.atf", "_self", 1  );
$Nav->addNavSeparator("WTP Home", "/webtools/");
$Nav->addNavSeparator("Ajax Tools", "/webtools/atf");
$Nav->addNavSeparator("Downloads", "http://download.eclipse.org/webtools/downloads/");
$Nav->addNavSeparator("Documentation", "/webtools/documentation/");
$Nav->addCustomNav("Project Plan", "http://www.eclipse.org/projects/project-plan.php?projectid=webtools.atf", "_self", 2);
$Nav->addNavSeparator("Community", 	"/webtools/community/");
$Nav->addCustomNav("Wiki", "https://wiki.eclipse.org/ATF", "_self", 2);
$Nav->addCustomNav("Newsgroup", "http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools.atf", "_self", 2);
$Nav->addCustomNav("Mailing List", "http://dev.eclipse.org/mailman/listinfo/atf-dev", "_self", 2);
?>
