<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<?xml-stylesheet type="text/xsl" href="http://www.eclipse.org/projects/project-plan.xsl"?>
    <!--
        Format is detailed at
        http://wiki.eclipse.org/Development_Resources/Project_Plan
        
        Use this to test local rendering -->
<!-- <?xml-stylesheet type="text/xsl" href="http://www.eclipse.org/projects/project-plan.xsl"?> -->
<p:plan plan-format="1.0" xmlns:p="http://www.eclipse.org/project/plan"
	xmlns="http://www.w3.org/1999/xhtml" name="JavaScript Development Tools">
	<p:release projectid="webtools.jsdt" version="3.4 (Juno)" />
	<p:introduction>
		<p>The JavaScript Development Tools (JSDT) project is a
			part of the Eclipse WTP top-level Project. It provides a JavaScript
			IDE for Eclipse and JavaScript language support for WTP's Source Editing project.
			This document describes	the features and the API set in the JSDT project
			for the	WTP 3.4 release.</p>
	</p:introduction>
	<p:release_deliverables>
		<p>Source code for deliverables tagged in CVS as version tagged
			<em>R3_4_0</em>, viewable in the WebTools_Project
			<a
				href="http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.jsdt/?root=WebTools_Project">CVS repository</a>
			.
		</p>
	</p:release_deliverables>
	<p:target_environments>
		<p>
			JSDT will support the platforms certified by the Eclipse Platform
			project. For a list of platforms supported , see
			<a
				href="http://www.eclipse.org/projects/project-plan.php?projectid=eclipse#target_environments">Eclipse Target Operating Environments</a>.
		</p>
		<p:internationalization>
			<p>
				Internationalization and Localization will be supported.
			</p>
		</p:internationalization>
	</p:target_environments>
	<p:themes_and_priorities>
		<p:preamble>
			<p>Themes and their priorities communicate the main objectives of
				the project and their importance. These will be prioritized based
				on the community feedback. New themes could be synthesized from the
				requirements submitted by the community.
			</p>
			<p>
				The sections to follow defines the plan items in the JSDT
				project. The plan items are grouped under the respective
				themes where applicable. Each plan item corresponds to a new
				feature, API or some aspects of the project that needs to be
				improved. A plan item has an entry in the Eclipse Bugzilla system
				that has a detailed description of the plan item. Not all plan
				items
				represent the same amount of work; some may be quite large,
				others,
				quite small. Although some plan items are for work that is
				more
				pressing than others, the plan items appear in no particular
				order.
				See the corresponding Bugzilla items for up-to-date status
				information on ongoing work and planned delivery milestones.</p>
		</p:preamble>
		
		<p:theme name="Version Currency">
			<p:description>
				<p>
					JSDT should try to keep up with evolving standards, both for JavaScript and for the web in general.
				</p>
			</p:description>
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;status_whiteboard=currency&amp;product=JSDT&amp;target_milestone=3.4+M1&amp;target_milestone=3.4+M2&amp;target_milestone=3.4+M3&amp;target_milestone=3.4+M4&amp;target_milestone=3.4+M5&amp;target_milestone=3.4+M6&amp;target_milestone=3.4+M7&amp;target_milestone=3.4+RC1&amp;target_milestone=3.4+RC2&amp;target_milestone=3.4+RC3&amp;target_milestone=3.4+RC4&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:committed>
		</p:theme>
		<p:theme name="Ease of Use">
			<p:description>
				<p>
						Features provided by WTP should be simple to use for users with
						widely-varying backgrounds and skill sets.
				</p>
					<ul>
						<li>WTP's User Interface should be consistent and should follow the
							Eclipse User Experience Guidelines. Experienced Eclipse users
							should
							find few, if any, unpleasant surprises or omissions in the
							UI.</li>
						<li>Usability and Accessibility reviews should be done for the
							most common task flows. Cheat Sheets may be provided to assist
							users in performing tasks.</li>
						<li>WTP should provide enhanced user documentation, tutorials,
							white
							papers, demonstrations.</li>
					</ul>
					<p>
						<i>Ease of Use</i>
						plan items are designated with "EaseOfUse" in the Status
						Whiteboard and the "plan" keyword.
					</p>
			</p:description>
			<!--
				keyword "plan" and status white-board includes the string
				"EaseOfUse"
			-->
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;status_whiteboard=EaseOfUse&amp;product=JSDT&amp;target_milestone=3.4+M1&amp;target_milestone=3.4+M2&amp;target_milestone=3.4+M3&amp;target_milestone=3.4+M4&amp;target_milestone=3.4+M5&amp;target_milestone=3.4+M6&amp;target_milestone=3.4+M7&amp;target_milestone=3.4+RC1&amp;target_milestone=3.4+RC2&amp;target_milestone=3.4+RC3&amp;target_milestone=3.4+RC4&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:committed>
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;status_whiteboard=EaseOfUse&amp;product=JSDT&amp;target_milestone=3.4&amp;target_milestone=---&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:proposed>
		</p:theme>
		<p:theme name="Scaling Up">
			<p:description>
					<p>
						Source Editing must be able to deal with development and
						deployment on
						an
						increasingly large and more complex scale. Source Editing
						should spend focused
						effort on performance testing and improvement
						when dealing with
						extremely large projects and workspaces,
						particularly where
						extensibility is offered.
				</p>
					<p>
						<i>Scaling Up</i>
						plan items are designated with the "plan" and "performance"
						keywords together.
					</p>
			</p:description>
			<!-- keywords "plan" and "performance" -->
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;product=JSDT&amp;target_milestone=3.4+M1&amp;target_milestone=3.4+M2&amp;target_milestone=3.4+M3&amp;target_milestone=3.4+M4&amp;target_milestone=3.4+M5&amp;target_milestone=3.4+M6&amp;target_milestone=3.4+M7&amp;target_milestone=3.4+RC1&amp;target_milestone=3.4+RC2&amp;target_milestone=3.4+RC3&amp;target_milestone=3.4+RC4&amp;keywords=plan+performance&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:committed>
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;product=JSDT&amp;target_milestone=---&amp;target_milestone=3.4&amp;target_milestone=4.0&amp;keywords=plan+performance&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:proposed>
		</p:theme>
		<p:theme name="Design for Extensibility">
			<p:description>
				<p>
					The 'P' in WTP stands for Platform, meaning Source Editing can
					be
					used by adopters to
					extend its functionality. This theme is about
					continuing to ensure the
					success of its adopters by promoting new
					APIs and Extension
					points.
					These should be backed with robust JUnit
					tests and good documentation.
				</p>
				<p>
					<i>Design for Extensibility</i>
					plan items are designated with the "plan" and "api" keywords together.
				</p>
			</p:description>
			<!-- keywords "plan" and "api" -->
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;product=JSDT&amp;target_milestone=3.4+M1&amp;target_milestone=3.4+M2&amp;target_milestone=3.4+M3&amp;target_milestone=3.4+M4&amp;target_milestone=3.4+M5&amp;target_milestone=3.4+M6&amp;target_milestone=3.4+M7&amp;target_milestone=3.4+RC1&amp;target_milestone=3.4+RC2&amp;target_milestone=3.4+RC3&amp;target_milestone=3.4+RC4&amp;keywords=plan+api&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:committed>
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;product=JSDT&amp;target_milestone=---&amp;target_milestone=3.4&amp;target_milestone=4.0&amp;keywords=plan+api&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:proposed>
		</p:theme>
		<p:theme name="Quality">
			<p:description>
				<p>
					Source Editing is one of the most heavily used projects within WTP and
					carries a correspondingly weighty bug report list because of it. Items
					under this theme are meant to address the backlog of bug reports not
					concerning performance and is not to include enhancements. It is
					expected that these require a significant enough amount of time to
					merit inclusion within the plan. Items under this theme are only to be
					chosen by Committers.
				</p>
				<p>
					<i>Quality</i>
					plan items are designated with the "plan" keyword and "qplan"
					within the status whiteboard.
				</p>
			</p:description>
			<!-- keywords "plan" and "qplan" in status -->
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;status_whiteboard=qplan&amp;product=JSDT&amp;target_milestone=3.4+M1&amp;target_milestone=3.4+M2&amp;target_milestone=3.4+M3&amp;target_milestone=3.4+M4&amp;target_milestone=3.4+M5&amp;target_milestone=3.4+M6&amp;target_milestone=3.4+M7&amp;target_milestone=3.4+RC1&amp;target_milestone=3.4+RC2&amp;target_milestone=3.4+RC3&amp;target_milestone=3.4+RC4&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:committed>
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;keywords_type=allwords&amp;status_whiteboard_type=substring&amp;status_whiteboard=qplan&amp;product=JSDT&amp;target_milestone=---&amp;target_milestone=3.4&amp;target_milestone=4.0&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:proposed>
		</p:theme>
		<p:theme name="Everything">
			<p:description>
				<p>
					Following are plan items including those not yet
					categorized into a theme.
				</p>
			</p:description>
			<!-- keyword "plan" -->
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;keywords_type=allwords&amp;short_desc=&amp;product=JSDT&amp;target_milestone=3.4+M1&amp;target_milestone=3.4+M2&amp;target_milestone=3.4+M3&amp;target_milestone=3.4+M4&amp;target_milestone=3.4+M5&amp;target_milestone=3.4+M6&amp;target_milestone=3.4+M7&amp;target_milestone=3.4+RC1&amp;target_milestone=3.4+RC2&amp;target_milestone=3.4+RC3&amp;target_milestone=3.4+RC4&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit" />
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;keywords_type=allwords&amp;short_desc=&amp;product=JSDT&amp;target_milestone=3.4&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:proposed>
			<p:deferred
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;keywords_type=allwords&amp;short_desc=&amp;product=JSDT&amp;target_milestone=Future&amp;target_milestone=---&amp;target_milestone=4.0&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:deferred>
		</p:theme>
	</p:themes_and_priorities>
	<p:appendix name="References">
		<p>
			The general WTP plan can be found
			<a
				href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools">
				here
			</a>
			.
		</p>
	</p:appendix>
</p:plan>