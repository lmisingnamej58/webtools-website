<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<!--  Use this to test local rendering in firefox -->
<!--
    <?xml-stylesheet type="text/xsl" href="project-plan-render.xsl"?>
-->
<?xml-stylesheet type="text/xsl" href="http://www.eclipse.org/projects/project-plan.xsl"?>
<p:plan
    plan-format="1.0"
    xmlns:p="http://www.eclipse.org/project/plan"
    xmlns="http://www.w3.org/1999/xhtml"
    name="Java EE Tools">
    <p:release
        projectid="webtools.jeetools"
        version="3.1" />
    <p:introduction>
        <p>The Web Tool Platform (WTP) project provides extensible frameworks and exemplary tools to build Web and Java EE applications. This document describes the features and the API set for the Galileo release.</p>
    </p:introduction>
    <p:release_deliverables>
        <p>
            There will be SDK and non-SDK versions of each of the main deliverables:
            <ul>
                <li>XML IDE (including XSL, XSD, etc)</li>
                <li>JSDT (Javascript only)</li>
                <li>Web Development (non Java)</li>
                <li>JEE Development</li>
                <li>EPP Packaging project: The Eclipse IDE for JEE Developers</li>
            </ul>
        </p>
    </p:release_deliverables>
    <p:release_milestones>
        <p:preamble>
            <p>
                WTP Milestones follow the Eclipse release milestones by an offset of 2 as set by the
                <a href="http://wiki.eclipse.org/Galileo#Milestones_and_Release_Candidates">Galileo release schedule.</a> For details, see our WTP 3.1 ramp down plan, 
<a href="http://wiki.eclipse.org/WTP_3.1_Ramp_down_Plan_for_Galileo">WTP 3.1 Ramp down plan for Galileo.</a>
                
            </p>
        </p:preamble>
        <p:milestone
            date="8/22/2008"
            milestone="M1">
            <p>M1 milestone</p>
        </p:milestone>
        <p:milestone
            date="09/26/2008"
            milestone="M2">
            <p>M2 milestone.</p>
        </p:milestone>
        <p:milestone
            date="11/07/2008"
            milestone="M3">
            <p>M3 milestone</p>
        </p:milestone>
        <p:milestone
            date="12/19/2008"
            milestone="M4">
            <p>M4 milestone</p>
        </p:milestone>
        <p:milestone
            date="2/06/2009"
            milestone="M5">
            <p>M5 milestone</p>
        </p:milestone>
        <p:milestone
            date="3/18/2009"
            milestone="M6">
            <p>M6 milestone.Feature complete, API and UI Freeze</p>
        </p:milestone>
        <p:milestone
            date="5/05/2009"
            milestone="M7">
            <p>M7 milestone. PMC review starts</p>
        </p:milestone>
        <p:milestone
            date="5/19/2009"
            milestone="RC1">
            <p>RC1 milestone.PMC approval required, 1 vote required</p>
        </p:milestone>
        <p:milestone
            date="5/26/2009"
            milestone="RC2">
            <p>RC2 milestone.PMC approval required, 2 votes required</p>
        </p:milestone>
        <p:milestone
            date="6/02/2009"
            milestone="RC3">
            <p>RC3 milestone.Document Freeze. PMC approval required, 3 votes required</p>
        </p:milestone>
        <p:milestone
            date="6/09/2009"
            milestone="RC4">
            <p>RC4 milestone.Only stop-ship defects, PMC approval required, 3 vote required</p>
        </p:milestone>
        <p:milestone
            date="6/16/2009"
            milestone="RC5">
            <p>RC5 milestone.Only stop-ship defects, PMC approval required, 3 vote required</p>
        </p:milestone>
        <p:milestone
            date="6/24/2009"
            milestone="GA">
            <p>GA</p>
        </p:milestone>
        <p:postamble />
    </p:release_milestones>
    <p:target_environments>
        <p>
            WTP will support the platforms certified by the Eclipse Platform project. For a list of platforms supported in WTP 3.1, see
            <a href="http://www.eclipse.org/projects/project-plan.php?projectid=eclipse#target_environments">Eclipse Target Operating Environments</a>
        </p>
        <p>.</p>
        <p:internationalization>
            <p>
                Internationalization and Localization will be supported.
                <ul>
                    <li>
                        Internationalization (I18N)
                        <p>Each project should be able to work in an international environment, including support for operating in different locales and processing/displaying international data (dates, strings, etc.).</p>
                    </li>
                    <li>
                        Localization
                        <p>Each project should provide an environment that supports the localization of the technology (i.e. translation). This includes, but is not limited to, ensuring that strings are externalized for easy translation.</p>
                    </li>
                    <p>
                        Where possible, projects should use an open and transparent process to create, maintain and deliver language packs translated into multiple languages in a timely manner. The primary languages to consider are: English, Simplified Chinese, Traditional Chinese, Japanese, French,
                        German, Spanish.
                    </p>
                </ul>
            </p>
        </p:internationalization>
    </p:target_environments>
    <p:compatibility_with_previous_releases />
    <p:themes_and_priorities>
        <p:preamble>
            <p>See the following sub-project plans for details</p>
            <!-- <p:preamble>
			<p> Themes and their priorities communicate the main objectives of
				the project and their importance. The following themes are derived
				from those defined by the Eclipse Requirement council for the
				Eclipse Galileo release and from the WTP 3.0 release themes. These
				will be prioritized based on the community feedback. New themes
				could be synthesized from the requirements submitted by the
				community.</p>
			<p>The sections to follow defines the plan items in the Dali Java
				Persistence Tools project. The plan items are grouped under the
				respective themes where applicable. Each plan item corresponds to a
				new feature, API or some apsects of the project that needs to be
				improved. A plan item has an entry in the Eclipse Bugzilla system
				that has a detailed description of the plan item. Not all plan items
				represent the same amount of work; some may be quite large, others,
				quite small. Although some plan items are for work that is more
				pressing than others, the plan items appear in no particular order.
				See the corresponding bugzilla items for up-to-date status
				information on ongoing work and planned delivery milestones.</p>
		</p:preamble>
		<p:theme name="Ease of Use">
			<p:description>
				<p>
					Features provided by WTP should be simple to use for users with
					widely-varying backgrounds and skill sets.
					<ul>
						<li>WTP User Interface should be consistent and should follow
							the Eclipse User Experience Guidelines.</li>
						<li>Usability and Accessibility reviews should be done
							for the most common task flows. Cheat Sheets should be provided
							to assist users in performing tasks</li>
						<li>WTP should provide enhanced user documentation, tutorials,
							white papers, demonstrations.</li>
					</ul>
				</p>
			</p:description>
			
		</p:theme>
		<p:preamble>
            -->
            <p>
                Sub-Project Plans:
                <ul>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.common">WTP Common Tools</a>
                    </li>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.dali">Dali Java Persistence Tools</a>
                    </li>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.ejbtools">EJB Tools</a>
                    </li>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.jeetools">Java EE Tools</a>
                    </li>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.jsf">JSF Tools</a>
                    </li>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.servertools">Server Tools</a>
                    </li>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.sourceediting">Source Editing</a>
                    </li>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.webservices">Web Services</a>
                    </li>
                    <li>
                        <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.releng">WTP Releng</a>
                    </li>
                </ul>
            </p>
        </p:preamble>
    </p:themes_and_priorities>
    <p:appendix />
</p:plan>