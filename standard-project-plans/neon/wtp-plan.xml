<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<!-- Use this to test local rendering in firefox -->

<?xml-stylesheet type="text/xsl" href="www.eclipse.org/eclipse/development/project-plan-render.xsl"?>
<!-- <?xml-stylesheet type="text/xsl" href="http://www.eclipse.org/projects/project-plan.xsl"?> -->
<p:plan
    plan-format="1.0"
    xmlns:p="http://www.eclipse.org/project/plan"
    xmlns="http://www.w3.org/1999/xhtml"
    name="Web Tools Platform">
    <p:release
        projectid="webtools"
        version="Neon"/>
    <p:introduction>
        <p>The Web Tool Platform (WTP) project provides extensible
            frameworks and exemplary tools to build Web and Java EE
            applications. This document describes the features and the
            API set for the Mars release.
        </p>
    </p:introduction>
    <p:release_deliverables>
        <p>
            There will be SDK and non-SDK versions of each of the main
            deliverables:
            <ul>
                <li>XML IDE (including XSL, XSD, etc)</li>
                <li>JSDT (JavaScript only)</li>
                <li>Web Development (no Java technologies)</li>
                <li>Java EE Development</li>
                <li>EPP Packaging project: The Eclipse IDE for Java EE Developers</li>
            </ul>
        </p>
    </p:release_deliverables>
    <p:release_milestones>
        <p:preamble>
            <p>
                WTP Milestones follow the Eclipse release milestones by
                an offset of 2 as set by the
                <a href="https://wiki.eclipse.org/Neon#Milestones_and_Release_Candidates">Neon release schedule.</a>
                For details, see the WTP  ramp down plan,
                <a href="https://wiki.eclipse.org/WTP_Ramp_down_Plan_for_Neon">Ramp down plan for WTP Neon.</a>
            </p>
        </p:preamble>
        <p:milestone></p:milestone>
        <p:postamble></p:postamble>
    </p:release_milestones>
    <p:target_environments>
        <p>
            WTP will support the same platforms as the Eclipse
            Platform project. For a list of platforms supported in
            Mars, see
            <a href="http://www.eclipse.org/projects/project-plan.php?projectid=eclipse#target_environments">Eclipse Target Operating Environments</a>.
            That is, WTP is pure Java code, no native code, so should "run anywhere". WTP committers test primarily on Windows, some on Linux, and a little on Macs.
            Bugs reproducible only on other platforms will still be considered valid, but generally will require close adopter involvement to propose
            patches and test fixes.
        </p>
        <p>
            WTP committers use and test on Java 6 and Java 7, but in theory should run on Java 5, as that is the highest version of Java assumed in the bundle's manifest.mf files (in the OSGi BREE heading), and, with few
            exceptions, our pre-reqs. Where there are exceptions, and Java 6 is required, such as for some JDT functions, everything else should continue to work fine
            just with reduced functionality.
            (Note, for committer convenience, some of the unit test bundles do assume Java 6.) Many of the WTP bundles assume only Java 4. The exact requirements can be
            determined by looking at the distributed bundles' BREE levels, but it is pretty much up to adopters to test or support Java 4 or Java 5 installations, if desired.
            If there are bugs only reproducible on Java 4 or Java 5, we will consider them valid, but generally give them a lower priority than other bugs.
        </p>
        <p:internationalization>
            <p>
                Internationalization and Localization will be supported.
                <ul>
                    <li>
                        Internationalization (I18N)
                        <p>Each project should be able to work in an
                            international environment, including support
                            for operating in
                            different locales and
                            processing/displaying international data
                            (dates, strings, etc.).
                        </p>
                    </li>
                    <li>
                        Localization
                        <p>Each project should provide an environment
                            that supports the localization of the
                            technology (i.e.
                            translation). This
                            includes, but is not limited to, ensuring
                            that strings are externalized for easy
                            translation.
                        </p>
                    </li>
                    <p>
                        We will provide "map files" or similar required input to the Babel Project, so they
                        can deliver language packs
                        translated into multiple
                        languages in a timely manner. The primary
                        languages to consider are: English,
                        Simplified
                        Chinese, Traditional Chinese, Japanese, French,
                        German, Spanish.
                    </p>
                </ul>
            </p>
        </p:internationalization>
    </p:target_environments>
    <p:compatibility_with_previous_releases>
        <div>
            <p>
                In general, we in WTP strive to provide the same type of
                <a href="http://www.eclipse.org/projects/project-plan.php?projectid=eclipse#compatibility">compatibility as the Eclipse Platform</a>.</p>
           
                <p><strong>API compatibility.</strong> WTP 3.5 will be compatible with APIs declared in WTP 3.4, WTP 3.3 and WTP 3.2. See also <a href="https://wiki.eclipse.org/WTP_API_Policy">WTP API Policy</a>.</p>
                <p><strong>Workspace compatibility.</strong>
                A workspace being used with WTP 3.4, 3.3 or 3.2 should still open and work with WTP 3.5. In general, though, once a workspace is opened with WTP 3.5, there is no guarantee it will continue to work with older versions (that is, there may be some one-time migration of some workspace meta data that prevents it being usable in older versions.</p>
                <p><strong>Project compatibility.</strong>
                A project being used with WTP 3.2, 3.3 or 3.4 should still be capable of being imported into and work with WTP 3.5. In general, a project being used with WTP 3.N should be able to co-exist with using the project with 3.N-2 ... as long as no new function from 3.N is used. This use case is
                    motivated by adopters supporting large development shops (say, of 20 to 100 developers) who can not all necessarily "move up" to latest version at the same time. They should all be
                    able to normally share
                    the same project, via SCMs and similar, until they all are able to move to common
                    development version or until they use some new function in the latest release (which, of course, would not be present in the previous releases). Note, it is hard to completely guarantee this will always work since there is no "common API" or spec that says
                    how to guarantee it. While we will make every
                    effort to write good code that is "forward friendly" (such as, code that knows to ignore preferences or
                    metadata that is not understood rather than blindly throwing an exception and failing or writing thousands of
                    error messages to the log, we depend heavily on adopters reporting bugs they find in the many possible "co-existence scenarios". We'll consider
                    bugs on this topic as valid and prioritize them along with other bugs. In cases where they can not be 
                    fixed, we will explicitly call out "co-existence" exceptions in our release or migrations documentation. 
                </p>
        </div>
    </p:compatibility_with_previous_releases>
    <p:themes_and_priorities>
        <p:preamble>
            <p> Themes and their priorities communicate the main
                objectives of the project and their importance. The
                section to follow
                defines themes that are common to all
                the sub-projects. Each sub-project defines additional
                themes and plan items
                corresponding to each of the
                themes.
            </p>
        </p:preamble>
    </p:themes_and_priorities>
    <p:appendix name="Sub-Project Plans">
        <p>
            Sub-Project Plans:
            <ul>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.common">WTP Common Tools</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.dali">Dali Java Persistence Tools</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.libra">Enterprise Tools for the OSGi Service Platform</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.jeetools">Java EE Tools</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.jsdt">JavaScript Development Tools</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.jsf">JSF Tools</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.servertools">Server Tools</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.sourceediting">Source Editing</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.webservices">Web Services</a>
                </li>
                <li>
                    <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools.releng">WTP Releng</a>
                </li>
            </ul>
        </p>
    </p:appendix>
</p:plan>