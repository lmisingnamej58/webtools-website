<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

#*****************************************************************************
#
# template.php
#
# Author:               Denis Roy
# Date:                 2005-06-16
#
# Description: Type your page comments here - these are not sent to the browser
#
#
#****************************************************************************

#
# Begin: page-specific settings.  Change these.
$pageTitle              = "WTP Incubator";
$pageKeywords   = "Eclipse WTP webtools XSL XML IDE";
$pageAuthor             = "David Williams";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links",        "downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings
#

# Include this php file to get the right column
# as assigned by the $rightColumn variable in HTML content
include( 'rightcolumn.php' );


# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="midcolumn">
<table>
    <tr>
        <td width="60%">
        <h1>$pageTitle</h1>
        <div class="wtpsubtitle">A home for hatchlings</div>
        </td>
        <td><img
            src="/webtools/images/wtplogosmall.jpg"
            alt="WTP Logo"
            align="middle"
            height="129"
            hspace="50"
            width="207"
            usemap="logomap" /> <map
            id="logomap"
            name="logomap">
            <area
                coords="0,0,207,129"
                href="/webtools/"
                alt="WTP Home" />
        </map></td>
    </tr>
</table>
<h1>Project overview</h1>
<p>The WTP Incubator is a generalized incubating project to foster the creation and development of new components of
WTP subprojects, before they become part of a release.</p>
<h2>Source Editing Components</h2>
<h3>XSL Component</h3>
<p>This component is to support XSL: authoring and editing XSL stylesheets, and running and debugging XSL
transforms. The <a href="http://www.eclipse.org/webtools/incubator/proposals/XSLTools_proposal.html">original
proposal</a> is still made available, but the developers maintain the status, plans, and activities of this work on the <a
    href="https://wiki.eclipse.org/XSLT_Project">Eclipse XSL Wiki Page</a>.</p>
<h3>X Query</h3>
<p><a href="https://wiki.eclipse.org/WTP/XQuery/Proposal">XQuery Development Toolkit(XQDT)</a> is mainly an editor
built on Structured Source Editor(SSE) for easy XQuery handling. It will initially have basic text editor functions such
as syntax colouring and content assistance to create and modify XQuery documents easily. Later it could be added more
rich features such as content formatting, advance preferences, error tips and support to other commercial XML databases
through extension points.</p>
<h3>XML Security Tools</h3>
<p>The XML Security Tools (XST) component supports signing, verifying as well as en- and decrypting of arbitrary XML documents
based on the recommendations of the World Wide Web Consortium. See the <a
href="https://wiki.eclipse.org/XML_Security_Tools_Proposal">proposal</a> and the <a
href="https://wiki.eclipse.org/WTP/XMLSecurityTools">XML Security Tools Wiki Page</a> for more information
about this component.</p>
<h3>Visual XML Editor (VEX)</h3>
<p>The <a href="https://wiki.eclipse.org/WTP/VisualXMLEditor/Proposal">Visual XML Editor</a> is a set of plugins that
provide WYSIWYG editing ability for XML files. This can be used regardless of the XML involved, and uses CSS stylesheets
to provide the formatting for the presentation layer. All that is needed is an stylesheet and an appropriate XML file to
edit. It hides the XML tags from the user, allow them to have a word processor like experience but store the information
in the native XML format.</p>
<h2>JSF Components</h2>
<h3>JSF Facelets Tools Project</h3>
<p>The JSF Facelets Tools project will enable the current features of the JSF Tools Project in a dynamic web project
for Facelets. See the <a href="http://www.eclipse.org/webtools/incubator/proposals/JSF Facelets Tools Proposal.html">JSF
Facelets Tools Project proposal</a> for detailed information on the project. The <a
    href="https://wiki.eclipse.org/JSF_Facelets_Tools_Project">JSF Facelets Tools Project Wiki</a> has the project status
and other developer resources.</p>
</div>
        $rightColumn
EOHTML;


        # Generate the web page
        $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
        ?>

