<?php

# Set the theme for your project's web pages.
# See the Committer Tools "How Do I" for list of themes
# https://dev.eclipse.org/committers/
# Optional: defaults to system theme
# miasma
$theme = "Nova";
header("Content-type: text/html; charset=utf-8");
$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/nova/wtpnova.css\" />\n");

# Define your project-wide Nav bars here.
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# these are optional
$Nav->setLinkList(null);
$Nav->addCustomNav( "About This Project", "/projects/project_summary.php?projectid=webtools.incubator", "_self", 1  );
$Nav->addNavSeparator("WTP Home", "/webtools/");
$Nav->addNavSeparator("WTP Incubator", "/webtools/incubator");
$Nav->addCustomNav("XSL", "#", "_self", 2);
$Nav->addCustomNav("XQDT", "#", "_self", 2);
$Nav->addCustomNav("XST", "#", "_self", 2);
$Nav->addCustomNav("VEX", "#", "_self", 2);
$Nav->addCustomNav("JSF Facelets", "#", "_self", 2);
$Nav->addNavSeparator("Downloads", 		"http://download.eclipse.org/webtools/downloads/");
$Nav->addNavSeparator("Documentation", 		"/webtools/documentation/");
$Nav->addCustomNav("Plan", "http://www.eclipse.org/projects/project-plan.php?projectid=webtools.incubator", "_self", 2);
$Nav->addNavSeparator("Community", 	"/webtools/community/");
$Nav->addCustomNav("Wiki", 		"https://wiki.eclipse.org/Category:Eclipse_Web_Tools_Platform_Project", 		"_self", 2);
$Nav->addCustomNav("Newsgroup", "http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools", "_self", 2);
$Nav->addCustomNav("Mailing List", 	"http://dev.eclipse.org/mailman/listinfo/wtp-incubator-dev", "_self", 2);
?>
