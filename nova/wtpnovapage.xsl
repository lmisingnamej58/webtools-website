<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output omit-xml-declaration="yes" />
    <xsl:template match="/sections">
        <div id="midcolumn">
            <div class="wtpbanner">
                <table>
                    <tr>
                        <td width="60%">
                            <h1>
                                <xsl:value-of select="@title" />
                            </h1>
                            <div class="wtpsubtitle">
                                <xsl:value-of select="@subtitle" />
                            </div>
                        </td>
                        <td>
                            <img src="/webtools/images/wtplogosmall.jpg" alt="WTP Logo" align="middle" height="129"
                                hspace="50" width="207" usemap="logomap" />
                            <map id="logomap" name="logomap">
                                <area coords="0,0,207,129" href="/webtools/" alt="WTP Home" />
                            </map>
                        </td>
                    </tr>
                </table>
            </div>
            <xsl:apply-templates select="section[@class='main']" mode="main" />
        </div>
        <div id="rightcolumn">
            <xsl:apply-templates select="section[@class='infobox']" mode="infobox" />
        </div>
    </xsl:template>
    <xsl:template match="section" mode="main">
        <div class="homeitem">
            <xsl:if test="@anchor">
                <a>
                    <xsl:attribute name="name">
						<xsl:value-of select="@anchor" />
					</xsl:attribute>
                </a>
            </xsl:if>
            <h3>
                <xsl:value-of select="@name" />
            </h3>
            <xsl:apply-templates select="description" mode="body" />
            <ul>
                <xsl:apply-templates />
            </ul>
        </div>
    </xsl:template>
    <xsl:template match="section" mode="infobox">
        <div class="sideitem">
            <h6>
                <xsl:value-of select="@name" />
            </h6>
            <div>
                <xsl:apply-templates select="description" mode="body" />
                <ul>
                    <xsl:apply-templates />
                </ul>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="section">
        <li>
            <xsl:choose>
                <xsl:when test="@name and @link">
                    <a>
                        <xsl:attribute name="href">
							<xsl:value-of select="@link" />
						</xsl:attribute>
                        <xsl:value-of select="@name" />
                    </a>
                    <br />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="@name">
                        <div class="wtpsectiontitle">
                            <xsl:value-of select="@name" />
                        </div>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates mode="body" />
        </li>
    </xsl:template>
    <xsl:template match="item">
        <li>
            <a>
                <xsl:attribute name="href">
							<xsl:value-of select="@link" />
						</xsl:attribute>
                <xsl:value-of select="@title" />
            </a>
            <xsl:if test="*|text()">
                <br />
                <xsl:apply-templates mode="body" />
            </xsl:if>
        </li>
    </xsl:template>
    <xsl:template match="description" mode="body">
        <xsl:apply-templates mode="body" />
    </xsl:template>
    <xsl:template match="*|@*|text()" mode="body">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()" mode="body" />
        </xsl:copy>
    </xsl:template>
    <xsl:template match="*|@*|text()" />
</xsl:stylesheet>
