<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle              = "Source Editing";
$pageKeywords   = "Eclipse WTP webtools XML HTML JSP CSS DTD JavaScript JS XSL stylesheet IDE";
$pageAuthor             ="Ugur Yildirim @ Eteration A.S.";

// Note: SSE is just one part of WTP Source Editing

$html = <<<EOHTML

<div id="midcolumn">
<table>
    <tr>
        <td width="60%">
        <h1>$pageTitle</h1>
        </td>
        <td><img
            src="/webtools/images/wtplogosmall.jpg"
            alt="WTP Logo"
            align="middle"
            height="129"
            hspace="50"
            width="207"
            usemap="logomap" /> <map
            id="logomap"
            name="logomap">
            <area
                coords="0,0,207,129"
                href="/webtools/"
                alt="WTP Home" />
        </map></td>
    </tr>
</table>
<h2>Project overview</h2>
<p>
	The Source Editing project is a sub-project in the Eclipse
	WTP Top-Level Project. Its main goal is to provide source
	editing support for typical "web" languages and the mixing
	of them, with a focus on making the editors extensible and
	easily reusable. It is important to recognize that SSE
	builds on the existing platform text infrastructure.
	Concepts such as content types, partition types, and viewer
	configurations are employed heavily.
</p>
<p>
	This project overview page is continually evolving, so
	please check back regularly for the most up-to-date
	information on the Source Editors project.  In the meantime,
	consult the older <a href="../wst/components/sse/overview.html">SSE Overview</a>
	page, as well as those for the
	<a href="../wst/components/css/overview.html">CSS</a>,
	<a href="../wst/components/dtd/overview.html">DTD</a>,
	<a href="../wst/components/html/overview.html">HTML</a>,
	<a href="https://wiki.eclipse.org/index.php/ATF/JSDT">JavaScript</a>,
	<a href="../jst/components/jsp/overview.html">JSP</a>,
	<a href="../wst/components/xml/overview.html">XML</a>, XSL and
	<a href="../wst/components/xsd/overview.html">XML Schema</a> components.
</p>
</div>

EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
