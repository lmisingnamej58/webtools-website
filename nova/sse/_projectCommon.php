<?php

# Set the theme for your project's web pages.
# See the Committer Tools "How Do I" for list of themes
# https://dev.eclipse.org/committers/
# Optional: defaults to system theme
# miasma
$theme = "Nova";
header("Content-type: text/html; charset=utf-8");
$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/nova/wtpnova.css\" />\n");

# Define your project-wide Nav bars here.
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# these are optional
#
# We have folding menus.  Second level (anything with a ... below) has a _projectCommon.php that overrides this one.
# It is up to you to keep all these files in sync.
$Nav->setLinkList(null);
$Nav->addCustomNav("About This Project", "/projects/project_summary.php?projectid=webtools.sourceediting", "", 1  );
$Nav->addNavSeparator("WTP Home", 	"/webtools/");
$Nav->addNavSeparator("WTP Source Editing", 	"/webtools/sse/");
$Nav->addCustomNav("CSS", "/webtools/wst/components/css/overview.html", "_self", 2);
$Nav->addCustomNav("DTD", "/webtools/wst/components/dtd/overview.html", "_self", 2);
$Nav->addCustomNav("HTML", "/webtools/wst/components/html/overview.html", "_self", 2);
$Nav->addCustomNav("JavaScript", "https://wiki.eclipse.org/index.php/ATF/JSDT", "_self", 2);
$Nav->addCustomNav("JSP", "/webtools/jst/components/jsp/overview.html", "_self", 2);
$Nav->addCustomNav("XML", "/webtools/wst/components/xml/overview.html", "_self", 2);
$Nav->addCustomNav("XSD", "/webtools/wst/components/xsd/overview.html", "_self", 2);
$Nav->addCustomNav("XSL", "/webtools/sse/", "_self", 2);
$Nav->addNavSeparator("Downloads", "http://download.eclipse.org/webtools/downloads/");
$Nav->addNavSeparator("Documentation", "/webtools/documentation/");
$Nav->addCustomNav("Project Plan", "http://www.eclipse.org/projects/project-plan.php?projectid=webtools.sourceediting", "_self", 2);
$Nav->addCustomNav("FAQ", "https://wiki.eclipse.org/WTP_FAQ", "_self", 2);
$Nav->addCustomNav("New and Noteworthy", "http://www.eclipse.org/webtools/development/news/", "_self", 2);
$Nav->addNavSeparator("Community", "/webtools/community/");
$Nav->addCustomNav("Adopters", "/webtools/adopters/", "_self", 2);
$Nav->addCustomNav("Wiki", "https://wiki.eclipse.org/Category:Eclipse_Web_Tools_Platform_Project", "_self", 2);
$Nav->addCustomNav("Newsgroup", "http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools", "_self", 2);
$Nav->addCustomNav("Mailing List", "http://dev.eclipse.org/mhonarc/lists/wtp-dev/", "_self", 2);
$Nav->addCustomNav("Request an Enhancement", "https://bugs.eclipse.org/bugs/enter_bug.cgi?product=WTP%20Source%20Editing&bug_severity=enhancement", "_self", 2);
$Nav->addCustomNav("Report a Bug", "https://bugs.eclipse.org/bugs/enter_bug.cgi?product=WTP%20Source%20Editing", "_self", 2);
$Nav->addCustomNav("Show Open Bugs", "https://bugs.eclipse.org/bugs/report.cgi?x_axis_field=bug_severity&y_axis_field=component&z_axis_field=&query_format=report-table&classification=WebTools&product=WTP+Source+Editing&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&format=table&action=wrap", "_self", 2);
$Nav->addNavSeparator("Development", "/webtools/development/");
$Nav->addCustomNav("Status Meetings", "https://wiki.eclipse.org/WTP_Development_Status_Meetings", "_self", 2);


?>
