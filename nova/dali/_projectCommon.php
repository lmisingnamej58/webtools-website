<?php

# Set the theme for your project's web pages.
# See the Committer Tools "How Do I" for list of themes
# https://dev.eclipse.org/committers/
# Optional: defaults to system theme
# miasma
$theme = "Nova";
header("Content-type: text/html; charset=utf-8");
$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/nova/wtpnova.css\" />\n");

# Define your project-wide Nav bars here.
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# these are optional
$Nav->setLinkList(null);
$Nav->addCustomNav( "About This Project", "/projects/project_summary.php?projectid=webtools.dali", "_self", 1  );
$Nav->addNavSeparator("WTP Home", "/webtools/");
$Nav->addNavSeparator("Dali JPA Tools", "/webtools/dali");
$Nav->addNavSeparator("Downloads", "/webtools/dali/downloads.php");
$Nav->addCustomNav("All Downloads", "/webtools/dali/downloads.php", "_self", 2);
$Nav->addCustomNav("Dali 2.1", "http://download.eclipse.org/webtools/downloads/drops/R2.1/R-2.1-20081218190237/", "_self", 2);
$Nav->addCustomNav("WTP 3.0 + Dali 2.0", "http://download.eclipse.org/webtools/downloads/drops/R3.0/R-3.0-20080616152118/", "_self", 2);
$Nav->addCustomNav("In Ganymede", "/ganymede/", "_self", 2);
$Nav->addNavSeparator("Documentation", "/webtools/dali/docs/dali_user_guide.pdf");
$Nav->addCustomNav("Dali 2.1 User Guide", "/webtools/nova/dali/docs/dali_user_guide.pdf", "_self", 2);
$Nav->addCustomNav("Dali 2.0 User Guide", "/webtools/nova/dali/docs/dali_user_guide_2.0.pdf", "_self", 2);
$Nav->addCustomNav("Dali 1.0 User Guide", "/webtools/nova/dali/docs/dali_user_guide_1.0.pdf", "_self", 2);
$Nav->addCustomNav("Project Plan", "/projects/project-plan.php?projectid=webtools.dali", "_self", 2);
$Nav->addNavSeparator("Community", 	"");
$Nav->addCustomNav("Wiki", "https://wiki.eclipse.org/Dali_Project", "_self", 2);
$Nav->addCustomNav("Newsgroup", "/newsportal/thread.php?group=eclipse.technology.dali", "_self", 2);
$Nav->addCustomNav("Mailing List", "http://dev.eclipse.org/mailman/listinfo/dali-dev", "_self", 2);
?>
