<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

#*****************************************************************************
#
# template.php
#
# Author:               Denis Roy
# Date:                 2005-06-16
#
# Description: Type your page comments here - these are not sent to the browser
#
#
#****************************************************************************

#
# Begin: page-specific settings.  Change these.
$pageTitle              = "WTP Release Engineering";
$pageKeywords   = "Eclipse WTP webtools";
$pageAuthor             ="Ugur Yildirim @ Eteration A.S.";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links",        "downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="midcolumn">
		<p>
		</p><table><tbody><tr><td width="60%"><h1>WTP Release Engineering</h1><div class="wtpsubtitle">releng</div></td><td><img src="/webtools/images/wtplogosmall.jpg" alt="WTP Logo" usemap="logomap" align="middle" height="129" hspace="50" width="207"><map id="logomap" name="logomap"><area coords="0,0,207,129" href="/webtools/" alt="WTP Home"></map></td></tr></tbody></table><h2 class="bar">Project overview</h2><p>This project is responsible for the builds and automated test infrastructure.</p>

		
	
</div>
EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

