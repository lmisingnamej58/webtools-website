<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();
include($App->getProjectCommon());

$pageKeywords	= "";
$pageAuthor		= "Naci Dai";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$html = <<<EOHTML

<link href="/webtools/community/education/style.css" rel="stylesheet" type="text/css">
$wtpTopButtons
<div id="edu_maincontent">
	<div class="edu_banner"><img src="/webtools/community/education/education_img/education.png" /></div>
	<div class="edu_desc">
		<p>The goal of WTP Education is to enable and educate WTP end-users by providing them quality resources for self-learning, increase WTP awareness and usage by obtaining, creating, and coordinating, materials for tutorials, articles, webinars, and other digital mediums.</p>
		<p>In these pages you will find a set of courses designed to teach anything from XML, HTML and standards based Web development to advanced topics such as Java Server Faces and Java Persistence.</p>
		<p>You can contribute to WTP Education in many ways. Please provide your feedback, comments, corrections using <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools" target="_blank">Bugzilla</a> and select Education as the component, they are important to us. Finally, if you would like to add your own courses, please contact the <a href="https://wiki.eclipse.org/Category:WTP_Roles" target="_blank">WTP Education Lead</a>.</p>
	</div>

	<div class="edu_menu">
		<div class="menu_left"><img src="/webtools/community/education/education_img/menu_bg-left.png"/></div>
		<div class="menu_center"><p class="edu_menulink">
			<a href="/webtools/community/education/web/">Web</a>
			<span class="edu_span">|</span>
			<a href="/webtools/community/education/jee/">Java EE</a>
			<span class="edu_span">|</span>
			<a href="/webtools/community/education/jsf/">JSF</a>
			<span class="edu_span">|</span>
			<a href="/webtools/community/education/jpa/">JPA</a>
		</p></div>
		<div class="menu_right"><img src="/webtools/community/education/education_img/menu_bg-right.png"/></div>
	</div>

	<div class="edu_main">

	</div>

</div>

EOHTML;
		$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>