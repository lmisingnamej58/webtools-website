<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();
include($App->getProjectCommon());

$pageKeywords	= "";
$pageAuthor		= "Naci Dai";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$html = <<<EOHTML

<link href="/webtools/community/education/style.css" rel="stylesheet" type="text/css">
$wtpTopButtons
<div style="text-align: justify ! important;" id="edu_maincontent">
<table width="100%">
	<tr>
		<td>
			<img src="t320/oulogo_med.gif" alt="The Open University" width="62" height="102" />
		</td>
		<td>
			<h1>T320 Ebusiness Technologies: Foundations and Practice</h1>
			<h2>Web Service Development Tutorials</h2>
		</td>
	</tr>
</table>

<hr></hr>

<b>Dr Neil Simpkins</b> 
<address>
Department of Communication and Systems, Open University
</address>
Email: <a href="mailto:n.k.simpkins@open.ac.uk">n.k.simpkins@open.ac.uk</a>

<p/>
<hr></hr>
<p/>

<h2>Introduction</h2>

This series of tutorials are adapted from those created as a part of a 
third level (year) course at the 
<a href="http://www.open.ac.uk/">Open University</a>.  The course, 
"<a href="http://www3.open.ac.uk/courses/bin/p12.dll?C01T320">E-business Technologies:
Foundations and Practice"</a>, encompasses a wide
range of E-business aspects and related technology.
<p/>
The course consists of four blocks
and a substantial project.  These tutorials are adapted from just one block.
As an academic course there is a focus on learning and understanding of concepts
and processes as well as acquiring skill in research and analysis.
<p/>
You can ignore all the references to other 'Block 3' components and
other 'blocks' of the course.
In the full course the tutorials are linked together by a course text which describes and 
discusses aspects of 'Service Orientated Architecture' (SOA). 
This brief web page will hopefully provide sufficient background for the
activities to be usable.

Here I will give a very much shortened account to tie the tutorials together.

<p/>
<hr></hr>
<p/>

<h2>Service Orientated Architecture</h2>

The tutorials are based around the concept of 'Service Orientated Architecture'
(SOA). 

<p/>
<img src="t320/fig1.gif" alt="Outline of Service Orientated Architecture" />
<br/>
<i>Figure 1 Outline of Service Orientated Architecture</i>
<p/>

The standard SOA model (Figure 1) introduces three key actors:

<ol>
<li>a web service 'provider' which makes services available and is 
also responsible for publishing a description of the service in one or more service 
directories. 
</li>
<li>a web service 'broker', who maintains a searchable directory of service descriptions. 
</li>
<li>a service 'requestor' or 'client' who wishes to find and use a service.
</li>
</ol>

<p/>
At the OU we have extended this model slightly by dividing the retrieval of a 
full web service description into two stages (Figure 2).  First a brief description of the
web service is retrieved from the directory which includes the service's network
location. Then a full description of the web service is retrieved from the 
actual service provider. 

<p/>
<img src="t320/fig2.gif" alt="Outline of Web Service Operation" />
<br/>
<i>Figure 2 Outline of Web Service Operation</i>
<p/>

These models can be realised as a Web Service by instantiating each of the
actors with a specific software component and adopting specific protocols.

<p/>
<img src="t320/fig3.gif" alt="Web service operation: component and protocol instances" />
<br/>
<i>Figure 3 Web service operation: component and protocol instances</i>
<p/>

We have adopted a set of components (Figure 3) to provide the three 
actors as follows:

<ol>
<li>'provider' is realised by Axis2 server side software
(<a href="http://ws.apache.org/axis2/">http://ws.apache.org/axis2/</a>).
This software is a web application which can be run on Tomcat, for example,
and which provides services to manage and expose web services over HTTP.
It handles SOAP messages (requests and responses) and also provides some 
support for alternative REST based communication with web services.
<p/>
There are guides to installing Tomcat under 'Documentation' on the
<a href="http://tomcat.apache.org">Apache site</a> and also a guide to 
<a href="http://ws.apache.org/axis2/index.html">installing Axis2</a>
on the Axis2 website.
</li>
<li>'broker' is provided by an implementation of 
'Universal Description, Discovery, and Integration' (UDDI).  We
selected 'jUDDI' (<a href="http://ws.apache.org/juddi/">http://ws.apache.org/juddi</a>).
This web application has its own interface for publishing and managing 
web service descriptions.  It also provides a standard API which can be
accessed by Eclipse WTP which provides a nice interface for this.
Note that running jUDDI requires a database to be installed and configured
to store descriptions. SQL scripts are provide with the software to set
up tables etc for a range of databases, including MySQL 
<a href="http://www.mysql.com/">http://www.mysql.com/</a>.
<p/>
A guide to installing jUDDI and connecting to MySQL comes with the
software download.
</li>
<li>'client' is provided by Eclipse and WTP. Eclipse is able to automatically
construct web pages which have suitable input fields to pass data to a service
and to construct an appropriate SOAP request message. It can also handle the
SOAP response from a service and display any information that is returned.
</li>
</ol>

<p/>
There are two potential problems with using the T320 tutorials in a wider context:

<ol>
<li>we have produced an installation wizard on a DVD which sets up Eclipse with 
plugins and Tomcat ready to be used. To follow the tutorials you will need to
install this software yourself by hand.
<p/>
</li>
<li>we have installed the Axis2 and jUDDI software on servers at the OU with accounts
ready for students to use (manage web services and publishing descriptions).
To follow the tutorials this software also has to be installed by hand.
</li>
</ol>

However, it is not overly difficult to set up this software yourself by
following the installation guides that come with the software or which are available
on the web.
<p/>
In the next section
I will outline what we have used in terms of plugin software, which is required
in addition to Eclipse (which requires Java of course), Axis2 and jUDDI with MySQL.


<p/>
<hr></hr>
<p/>
<h2>Eclipse plugins</h2>

The first tutorial outlines the plugins we have used. Here I 
will provide more leads to acquiring the software. We used Eclipse 3.3
but I believe there are no problems using other versions.  We did encounter
problems with running Eclipse on older Java versions (from memory I think
Java 1.4 caused minor problems).

<h3>Web Tools Platform</h3>
The Web Tools Platform (WTP) provides a range of facilities for implementing,
deploying, running and texting services. I don't pretend to be an expert on WTP
but we have used its facilities extensively in what follows. WTP also provides
facilities for handling XML for example, which are covered earlier in the
OU's T320 course.
<p/>
The WTP can be added to Eclipse either as a download which you put into the
correct directories by hand or by using Eclipse's software management tool
(use the 'Find and install' option on the 'Help > Software updates' menu).
<p/>
Personally, I found the update approach to be more reliable but during this
process it could be tricky to identify just the pre-requisite software required for
WTP.  If the process has not improved in recent versions I may produce a guide
to this later (it would be useful to receive feedback on installing WTP generally).
<p/>
For further information on WTP, see: 
<a href="http://www.eclipse.org/webtools/">
http://www.eclipse.org/webtools/</a>
<br/>
For WTP updates see: 
<a href="http://download.eclipse.org/webtools/updates/">
http://download.eclipse.org/webtools/updates/</a>
<br/>
For WTP downloads see:
<a href="http://download.eclipse.org/webtools/downloads/">
http://download.eclipse.org/webtools/downloads/</a>
<p/>

<h3>Axis2 plugin</h3>

The Axis2 service maker is employed later in the tutorials.  This tool simply
wraps up a web service as an Axis archive file (.aar) suitable for uploading to
the Axis2 server software which manages services. An Axis archive is very
similar to a '.war' or '.zip' file in pulling together a number of resources
or files into a single item which is easy to handle.
<p/>
For further information see:
<a href="http://ws.apache.org/axis2/tools/1_4_1/eclipse/servicearchiver-plugin.html">
http://ws.apache.org/axis2/tools/1_4_1/eclipse/servicearchiver-plugin.html</a>.

<h3>soapUI plugin</h3>

The soapUI plugin for Eclipse is used in the tutorials to test a service 
for compliance to the basic WS-I profile 
(<a href="http://www.ws-i.org/">http://www.ws-i.org/</a>)
for interoperability.
The potential a web service has for inter-operating with other services
is of course extremely important.
<p/>
For further information see:
<a href="http://www.soapui.org/eclipse/index.html">
http://www.soapui.org/eclipse/index.html</a>

<p/>
<hr></hr>
<p/>
<h2>Tutorials</h2>

The tutorials are all PDF format documents.  If you need a reader for
PDFs then visit the 
<a href="http://get.adobe.com/uk/reader/">Adobe site</a> and download 
the free Adobe Reader.
<p/>
The tutorials are:
<p/>
<ul>

<li>
<a href="t320/Implementing_a_Simple_Web_Service.pdf">Implementing a toy "Hello there" web service
</a>.  This guide refers to a supporting guide 
"<a href="t320/Configuring_an_Application_Server_in_Eclipse.pdf">
Configuring an Application Server in Eclipse</a>".
<p/>
This tutorial starts out by using Eclipse and a local
Tomcat application server. Based on a very simple piece of Java code
which provides the service's 'business logic' a service is implemented
and then deployed to Tomcat by Eclipse. Eclipse is then used to create
a simple client and to test the service.
</li>

<li>
<a href="t320/Generating_a_client_from_WSDL.pdf">Generating a client from a WSDL</a> document.
<p/>
This tutorial demonstrates how to use Eclipse to generate a client set
of web pages and call a web service based on its Web Services Description
Language (WSDL) document.  It uses some simple demonstration toy web services
hosted at the OU but if these are not available you can employ a service
running locally or use another public site, such as
<a href="http://www.xmethods.net">XMethods</a>.
</li>

<li>
<a href="t320/Editing_a_WSDL.pdf">Editing the 'Hello' web service WSDL</a>.
<p/>
This tutorial examines the "Hello there" WSDL document using the Eclipse
WTP WSDL editor. The graphical view is shown to be very useful in understanding
WSDL and making any alterations, such as adjusting the location of the 
service in the description when a service is deployed to a new location.
</li>

<li>
<a href="t320/Deploying_a_web_service.pdf">Deploying a web service</a>.
<p/>
This tutorial demonstrates how to wrap up and deploy a web service using Eclipse and
the Axis2 archiving plugin. It also illustrates how to deploy the archive
to Axis2 running on a Tomcat server and then test the web service.
</li>

<li>
<a href="t320/Publishing_and_accessing_UDDI.pdf">Publishing and accessing UDDI</a>.
<p/>
This tutorial illustrates how you can access UDDI using Eclipse and both
publish your own service descriptions and search for useful services.
The tutorial refers to a UDDI service hosted at the OU which should usually
be available but if not there are others on the web, such as the
<a href="http://registry.gbif.net/uddi/web">
Global Biodiversity Information Facility UDDI Registry</a>
<p/>
You will not have permissions to publish to the OU UDDI but can publish 
to your own UDDI if you have installed jUDDI.
</li>

<li>
<a href="t320/Axis2_and_REST.pdf">Axis2 and REST</a>
<p/>
This very brief guide demonstrates Representational State Transfer (REST) type access to a 
web service which is hosted on Axis2.  
REST is a term coined by Roy Fielding in his PhD dissertation to describe an architectural 
style of systems networking that does not require additional mechanisms or protocols 
over or above existing Internet protocols. In Fielding's own words (2000, Section 6.1):
<blockquote>
<i>
"The name 'Representational State Transfer' is intended to evoke an image of how a well-designed 
Web application behaves: a network of web pages (a virtual state-machine), where the user 
progresses 
through the application by selecting links (state transitions), resulting in the next page 
(representing the next state of the application) being transferred to the user and rendered 
for their use."
</i>
</blockquote>
<p/>
</li>

<li>
<a href="t320/Testing_a_web_service_for_WS-I_conformance.pdf">Testing a web service for WS-I conformance.</a>
<p/>
In this tutorial I test the conformance of a simple web service against the 
<a href="http://www.ws-i.org/">WS-I</a> basic profile.  The soapUI plugin is used to 
generate a report of the results of testing the service against each of the 
profile's criteria.
</li>

<li>
<a href="t320/Specifying_WSDL_style_and_encoding_using_Eclipse.pdf">Specifying WSDL style and encoding using Eclipse</a>.
<p/>
The importance of WSDL style for aspects such as interoperability are
discussed by Butek, see:
<a href="http://www.ibm.com/developerworks/webservices/library/ws-whichwsdl/">Which style of WSDL should I use?</a>
</li>
</ul>
<p/>
<hr></hr>
<p/>
</div>

EOHTML;
		$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>