package components.security.test;

import java.security.AllPermission;
import java.security.BasicPermission;

import org.apache.cactus.ServletTestCase;

import components.security.ApplicationPermission;
import components.security.InsufficientPermissionsException;
import components.security.PermissionManager;

public class PermissionManagerTest extends ServletTestCase{
	
	public void testPermissionManagerAllow() throws Exception
	{
		//Make sure that the AllPermission gives us permission to our test permission
		session.setAttribute(PermissionManager.TOKEN_KEY, new AllPermission());
		PermissionManager manager = new PermissionManager(new TestPermission("Test"));
		manager.validate(request);
	}
	
	public void testPermissionManagerDisallow() throws Exception
	{
		try
		{
			//Make sure that the ApplicationPermission does not give test permission  
			session.setAttribute(PermissionManager.TOKEN_KEY, new ApplicationPermission("Login"));
			PermissionManager manager = new PermissionManager(new TestPermission("Test"));
			manager.validate(request);	
			fail("Should not have allowed this request");
		} catch (InsufficientPermissionsException e)
		{
		}
	}
	
	private class TestPermission extends BasicPermission
	{

		private static final long serialVersionUID = 1L;

		public TestPermission(String name) {
			super(name);
		}
		
	}
}
