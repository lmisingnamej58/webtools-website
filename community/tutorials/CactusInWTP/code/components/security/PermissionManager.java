package components.security;

import java.security.BasicPermission;
import java.security.Permission;

import javax.servlet.http.HttpServletRequest;

public class PermissionManager {

	public static final String TOKEN_KEY = "token";
	private Permission fPermissionRequired;
	
	public PermissionManager(Permission permissionRequired)
	{
		fPermissionRequired = permissionRequired;
	}
	
	public void validate(HttpServletRequest request)
	throws InsufficientPermissionsException
	{
		Permission permission = (Permission) request.getSession().getAttribute(TOKEN_KEY);
		if (!permission.implies(fPermissionRequired))
		{
			throw new InsufficientPermissionsException("You do not have permission for that operation");
		}
	}
	
	public class ApplicationPermission extends BasicPermission {

		private static final long serialVersionUID = 1L;
		
		public ApplicationPermission(String name) {
			super(name);
		}

	}
}
