<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../wtp.xsl"?>
<html>
<head>
  <meta name="root" content="../../../../" />
  <title>WTP Tutorials - Building a Simple EJB Application</title>
</head>

<body>
<h1>WTP Tutorials - Building a Simple EJB Application</h1>
<p>
<b>By Naci Dai</b><BR/>
Eteration A.S.<BR/>
February 23, 2005<BR/>
<BR/>
</p>
<p>In this tutorial you will create a simple EJB component and a client web application. This tutorial will introduce you to Session beans and the Web Tools Platform project's support for XDoclet and server tools.
<BR/>

</p>
<h2>Prerequisites For The Tutorial</h2>
<ol>
	<li>Web Tools Platform (WTP) project<BR/>The WTP project can be downloaded from <a href="http://download.eclipse.org/webtools/downloads/">http://download.eclipse.org/webtools/downloads/</a><BR/><BR/>
	</li>
	<li>XDoclet 1.2.3<BR/>XDoclet is available from <a href="http://xdoclet.sourceforge.net" target="_top">http://xdoclet.sourceforge.net</a><BR/><BR/></li>
	<li>JBoss 3.2.3<BR/>JBoss is available from <a href="http://www.jboss.org" target="_top">www.jboss.org</a><BR/><BR/></li>
	<li>JDK 1.4.2<BR/>Sun's JDK is available from <a href="http://java.sun.com/j2se/1.4.2/download.html" target="_top">http://java.sun.com/j2se/1.4.2/download.html</a><BR/><BR/>
	</li>

</ol>

<h2>Installing The JBoss and XDoclet Runtimes In Eclipse</h2>
<p>In order to run the EJB and Web modules from Eclipse (which will allow you to test and debug the modules) Eclipse has to be told where to find JBoss, XDoclet and where to find the JDK.  JBoss requires a JDK so it can compile servlets and JSPs.</p>
<ol>
	<li>Select Window -&gt; Preferences<B></B>.<BR/>
	<img border="0" src="images/buildejb01.gif" width="550" height="340"/><BR/><BR/></li>
	<li>Select Java -&gt; Installed JREs from the menu on the left.<BR/>
	<img border="0" src="images/buildejb02.gif" width="574" height="513"/><BR/><BR/></li>
	<li>Select Add... The Add JRE dialog will open. Select Browse for the JRE home directory and choose the location where you installed the JDK 1.4.2. Name the runtime Sun JDK 1.4.2 and click OK.<BR/>
	<img border="0" src="images/buildejb03.gif" width="485" height="441"/><BR/><BR/></li>
	<li>The Sun JDK 1.4.2 now shows on the list of installed runtimes. Click OK.<BR/>
	<img border="0" src="images/buildejb03a.gif" width="574" height="513"/><BR/><BR/></li>
	<li>Select Window -&gt; Preferences.<BR/><BR/></li>
	<li>Select Server -&gt; Installed Runtimes from the menu on the left.<BR/>
	<img border="0" src="images/buildejb04.gif" /><BR/><BR/></li>
	<li>Click on Add... Select Generic Server Runtime -&gt; JBoss v3.2.3 and click Next. Click Browse and select the location where you installed JBoss. and click Finish.<BR/>
	<img border="0" src="images/buildejb06.gif" /><BR/><BR/></li>
	<li>Select XDoclet. Make sure the builder item is selected.  Click on the Browse... button and choose the directory where you have installed XDoclet.  Make sure that you choose the correct version. Click Apply.<BR/>
	<img border="0" src="images/buildejb07.gif" /><BR/><BR/></li>
	<li>Select XDoclet-&gt;ejbdoclet This is where you can modify the ejbdoclet generation options.  You will be able to select specific application servers and their versions. XDoclet will generate deployment descriptors for these application servers only.  Make sure nothing is selected.  Click Apply.<BR/>
	<img border="0" src="images/buildejb08.gif" /><BR/><BR/></li>
	<li>Click OK to close the preferences dialog. JDK, XDoclet and JBoss are now configured in Eclipse.</li>
	
</ol><h2>Creating EJB and Web Modules</h2>
<p>You will now create flexible projects with EJB and Web modules.  In the project with an EJB module you will create a simple Session EJB, and in the project with the web module you will create a client web application to this EJB component.</p>
<ol>
	<li>Select File -&gt; New -&gt; Other. Select EJB -&gt; J2EE EJB Module and click Next.<BR/>
	<img border="0" src="images/buildejb09.gif"/><BR/><BR/></li>
	<li>Click New... for the project, name the project ZooBeansProject and choose the server you have defined and click OK. Enter ZooBeans for the module name. Click advanced and deselect Add module to an EAR project. Select Create an EJB Client JAR, make sure the EJB version 2.0 is selected and click Finish to accept the defaults. <BR/>
	<img border="0" src="images/buildejb10.gif" /><BR/><BR/></li>
	<li>You will be prompted to switch to the J2EE perspective. Click Yes.<BR/><BR/></li>
	<li>The Project Explorer is populated with different types of Web and EJB
	applications. Your newly created project is located under EJB
	Projects. Expand your EJB project. The Deployment Descriptor
	contains information about your EJB project such as the EJBs that
	your project contains. Expanding the ZooBeans folder will reveal
	the ejbModule folder. This is where you will store all of your Java
	files including any EJBs and supporting classes you create. ZooBeansClient project holds the common classes that will be packed into the EJB client jar.  We will need this project for the EJB module and the web module.<BR/>
	<img border="0" src="images/buildejb10a.gif" /><BR/><BR/></li>
</ol>

<BR/>
<h2>Creating a Session EJB</h2>
<p>Here we will create the simplest of all EJBs; A stateless session bean.</p>
<ol>
	<li>Right click on the ejbModule folder and select New -&gt; Other...-&gt; EJB -&gt;EnterpriseJava Bean. Choose SessionBean in the first tab, in the next tab. Set the package to com.zoo. Next enter the class name; class name must end with a "Bean" so name it the bean TigerBean, Click Next. Click Finish. The TigerBean class will now appear in the ejbModule folder and XDoclet builder will generate Tiger, TigerBean, TigerHome, TigerUtil and other related classes for you. TigerBean is an XDoclet annotated Session Bean and other classes are derived classes. Open TigerBean in the Java editor.<BR/>
	<table><tr><td><img border="0" src="images/buildejb11.gif" /></td><td><img border="0" src="images/buildejb11a.gif" /></td></tr></table><BR/><img border="0" src="images/buildejb11b.gif" /><BR/></li>
	<li>XDoclet is an extended Javadoc Doclet engine. It's a generic Java tool that lets you create custom Javadoc @tags and based on those @tags generate source code or other files (such as xml-ish deployment descriptors) using a template engine it provides. XDoclet supports a set of common standard tasks such as web.xml or ejb-jar.xml generation. XDoclet uses special JavaDoc @tags to define
     settings for each component. For example putting a <span style='font-size:10.0pt;font-family:Courier;
     color:#3F5FBF;background:white;
     '>@ejb.bean</span><span style='font-size:10.0pt;
     font-family:Courier;color:windowtext;
     background:white;'> </span><span style='font-size:10.0pt;
     font-family:Courier;color:#3F5FBF;
     background:white;'>name=&quot;Tiger&quot;</span><span
     style='font-size:10.0pt;font-family:Courier;
     color:windowtext;background:white;'> </span><span
     style='font-size:10.0pt;font-family:Courier;
     color:#3F5FBF;background:white;'>jndi</span><span
     style='font-size:10.0pt;font-family:Courier;
     color:#7F7F9F;background:white;'>-</span><span
     style='font-size:10.0pt;font-family:Courier;
     color:#3F5FBF;background:white;'>name=&quot;Tiger&quot;</span><span
     style='font-size:10.0pt;font-family:Courier;
     color:windowtext;background:white;'> </span><span
     style='font-size:10.0pt;font-family:Courier;
     color:#3F5FBF;background:white;'>type=&quot;Stateless&quot;</span><span
     style='font-size:10.0pt;font-family:Courier;
     color:windowtext;background:white;'> </span><i>in TigerBean.java</i>
    <BR/>
    <BR/>
    <pre id="code">
/**
 *
 * <!-- begin-user-doc -->
 * A generated session bean
 * <!-- end-user-doc -->
 * *
 * <!-- begin-xdoclet-definition --> 
 * @ejb.bean name="Tiger"	
 *           description="A session bean named Tiger"
 *           display-name="Tiger"
 *           jndi-name="Tiger"
 *           type="Stateless" 
 *           transaction-type="Container"
 * 
 * <!-- end-xdoclet-definition --> 
 * @generated
 */

public abstract class TigerBean implements javax.ejb.SessionBean {

	/** 
	 *
	 * <!-- begin-xdoclet-definition --> 
	 * @ejb.interface-method view-type="remote"
	 * <!-- end-xdoclet-definition --> 
	 * @generated
	 *
	 * //TODO: Must provide implementation for bean method stub
	 */
	public String foo(String param) {
		return null;
	}
}    
    </pre>
    
	</li>
	<li>TigerBean is the only class you will edit.  All others are automatically generated and XDoclet will regenerate them each time you make a change to the TigerBean class. WTP provides code-assist for XDoclet tags.  To get help hit crtl-space anywhere inside a xdoclet Javadoc. Edit the TigerBean class and change the foo method to look like this:<BR/>
	<pre id="code">
	/** 
	 *
	 * <!-- begin-xdoclet-definition --> 
	 * @ejb.interface-method view-type="remote"
	 * <!-- end-xdoclet-definition --> 
	 *
	 */
	public String roar() {
		return "Roar!";
	}    
    </pre>    
    </li>

	<li>XDoclet builder will start working again and update your classes.  
	At the end your projects will look like the following: The ZooBeansProject 
	that hosts the EJB mmodule will have two classes TigerBean.java and 
	TigerSession.java.  These are server side classes.  ZooBeansClient project will have
	the public interfaces such as the Tiger.java and TigerHome.java.  These are the classes
	that will be needed by all clients.<BR/><BR/>
	<img border="0" src="images/buildejb12.gif" /><BR/><BR/>
	<img border="0" src="images/buildejb12a.gif" /><BR/><BR/></li>
</ol>
<h2>Publishing the EJB to the server</h2>
<p> Here we will publish the newly created ZooBeans to a server.</p>
<ol>
	<li> Select the server view from the workbench menu Window&gt;Show View&gt;Other&gt;Server&gt;Servers. Right click on the server view window select New -&gt; Server... Select All&gt;Generic Server Support and then Click Next. Keep the setting for JBoss 3.2.3 and click Next.<BR/>
	<img border="0" src="images/buildejb13.gif" />
	<BR/>Choose the ZooBeans project from the Available Projects list and click Add and then Click Finish.<BR/><BR/><BR/>
	</li>
	<li>You have setup your server to run your EJB module.  Click on the server right click.  Choose Start.<BR/>
	<img border="0" src="images/buildejb14.gif" /><BR/>
	</li>
	<li>You should now observe the console output.  JBoss will print a message similar to the following indicating that the EJB Tiger has been deployed successfully.<BR/>
	<pre id="code">
12:46:38,899 INFO  [MainDeployer] Starting deployment of package: ZooBeans.jar
12:46:39,249 INFO  [EjbModule] Deploying Tiger
12:46:39,409 INFO  [StatelessSessionInstancePool] Started jboss.j2ee:jndiName=Tiger,plugin=pool,service=EJB
12:46:39,409 INFO  [StatelessSessionContainer] Started jboss.j2ee:jndiName=Tiger,service=EJB
12:46:39,409 INFO  [EjbModule] Started jboss.j2ee:module=ZooBeans.jar,service=EjbModule
12:46:39,409 INFO  [EJBDeployer] Deployed: ZooBeans.jar
	
	</pre><BR/><BR/>
	</li>
</ol>

<h2>Testing the Tiger! Building a client web application</h2>
<p>
Your EJB has now been defined with your EJB Project and it has been deployed. It is ready to be used. You can now build a test client to make the Tiger EJB Roar! To make this create a Flexible Project with a Web Module named ZooWebProject.  Use the same server (JBoss) for this project too.  You can follow the step described in the tutorials <a href="../SchoolSchedule/BuildingAScheduleWebApp.html">Building a School Schedule Web Application</a> or <a href="../BuildJ2EEWebApp/BuildJ2EEWebApp.html">Building and Running a Web Application</a> if you are not familiar with the process.</p>
<ol>
	<li>When you have created the ZooWebProject.  You will end up with a project that looks like the following:<BR/>
	<img border="0" src="images/buildejb16.gif" /><BR/><BR/>
	This Web module will be client to the EJB.  Therefore it needs to know about the EJB module we have created.
	To do this we will set the project build path and use the Flexible projects to dynamically include the EJB client jar 
	with our web module. 
	</li>
</ol>
<h2>Using Flexible projects to link the web module with the an EJB Client module</h2>
<p>We will need to access the  EJB interface types such as Tiger and TigerHome in client applications.  These classes are in the ZooBeansClient project, and are packaged in an EJB client jar (ZooBeansClient.jar). 
    We need to add the client classes to the build path and inlcude the client.jar in the web module
    when it is deployed to a server.</p>
<ol>
	<li>In the Project or Package Explorer, right click on the ZooBeansWeb project and Choose Properties... In the Java buildpath, add the ZooBeansClient project to the project references.  
	This will help allows us to compile against the latest ejb client classes in this web project.<BR/>
	<img border="0" src="images/buildejb15a.gif" /><BR/><BR/>
	</li>
	<li>Now we have to add a dependent module to the ZooWeb web module. We will use the flexible project support to do this.
	Currently, WTP does not have a user interface to add dependent modules, so we will edit a file named ".wtpmodules", which can be found at the root of each
	flexible project.  However, this is a hidden file so too see it, one must use the "Resource Perspective".  To go to the resource perspective
	go Window&gt;Open Perspective&gt;Other and choose resource.  Open the .wtpmodules file and edit the contents to add the following:
	<br/>
    <pre id="code">
	
    &lt;dependent-module deploy-path="/WEB-INF/lib" 
      handle="module:/resource/ZooBeansClient/ZooBeansClient"&gt;
        &lt;dependency-type&gt;uses&lt;/dependency-type&gt;
    &lt;/dependent-module&gt;
	</pre>
	<BR/>
	<BR/>
	Here we have asked the flexible project support to include the ejb client jar
	inside the WEB-INF/lib folder of the web module during runtime deployment. Flexible
	project support will do the tasks of building a new jar and including it with our web
	module each time the ejb changes.
	<BR/>
	<BR/>
	
	<img border="0" src="images/buildejb15b.gif" /><BR/><BR/>
	</li>
    
	<li>
	Now Create a file named test.jsp
	<BR/>
	Open test.jsp in the JSP source page editor.<BR/><BR/></li>
	<li>Add  the following lines to test.jsp
    <pre id="code">
&lt;html&gt;
&lt;head&gt;
&lt;title&gt;Insert title here&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;%
   com.zoo.Tiger tiger = null;
   try{
	   com.zoo.TigerHome home = com.zoo.TigerUtil.getHome();
	   tiger = home.create();
	   
   }catch(Exception exception)
   {
	   
   }
   

%&gt;
&lt;b&gt;&lt;%= tiger.roar() %&gt;&lt;/b&gt;
&lt;/body&gt;
&lt;/html&gt;
    </pre>
    </li>
	<li>Save the test file.</li>
</ol>
<h2>Running And Testing The Web Application</h2>
<p>Your web application is now complete. Now it is time to take it for a spin.</p>
<ol>
	<li>Right click on test.jsp and select Run As -&gt; Run on Server...<BR/><BR/></li>
	<li>A Web browser window will open with the application. Beware of the tiger!.<BR/>
	<img border="0" src="images/buildejb17.gif" /><BR/><BR/>
	</li>
	<li>Experiment adding more methods and ejbs.</li>
</ol>
<h2>Summary</h2>
<p>In this tutorial you learned how to configure Eclipse to work with XDoclet and create a J2EE EJB project that has a Session Bean. You created a client Web application. This application, while simple, provides a good introduction to EJB development and some of the Web development tools available in the Eclipse Web Tools Platform project.</p>
<p></p>
<p>
</p>
</body>
</html>
