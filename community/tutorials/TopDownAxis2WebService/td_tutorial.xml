<!--
 /*******************************************************************************
 * Copyright (c) 26.04.2007 WSO2 Inc. Others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   WSO2 Inc - Initial version of the Tutorial
 *   20070426 182718 sandakith@wso2.com Lahiru Sandakith, Initial Tutorial Implementation
 *   20070629 194962 sandakith@wso2.com Lahiru Sandakith, Update the BU Tutorial to sync with WTP 2.0
 *******************************************************************************/ 
-->
<html>
 <head>
  <META
   http-equiv="Content-Type"
   content="text/html; charset=UTF-8">
  </META>
  <meta
   http-equiv="Content-Type"
   content="text/html; charset=iso-8859-1">
  </meta>
  <link
   rel="stylesheet"
   href="./default_style.css"
   type="text/css">
  </link>
  <link
   rel="stylesheet"
   href="stylesheets/wtp.css"
   type="text/css">
  </link>
  <title>Eclipse WTP Tutorials - Creating Top Down Web Service via Apache Axis2</title>
 </head>
 <body>
  <table
   width="100%"
   cellspacing="5"
   cellpadding="2"
   border="0">
   <tbody>
    <tr>
     <td
      width="60%"
      align="left">
      <font
       class="indextop"
       size="5">
       <b>Eclipse WTP Tutorials - Creating Top Down Web Service via Apache Axis2</b>
      </font>
     </td>
     <td width="40%">
      <img
       width="207"
       hspace="50"
       height="129"
       align="middle"
       src="images/wtplogosmall.jpg" />
     </td>
     <br />
    </tr>
   </tbody>
  </table>
  <table
   width="100%"
   cellspacing="5"
   cellpadding="2"
   border="0">
   <col width="16" />
   <col width="*" />
   <tbody>
    <tr>
     <td
      valign="top"
      align="right">
      &#160;
     </td>
     <td valign="top">
      <p>
       <b>
        <a href="mailto:sandakith@wso2.com">By Lahiru Sandakith</a>
       </b>
       <br />
       <a href="http://www.wso2.org">WSO2 Inc.</a>
       <br />
       June 29, 2007
       <br />
       <br />
      </p>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      bgcolor="#0080c0"
      align="left"
      colspan="2">
      <b>
       <font
        face="Arial,Helvetica"
        color="#ffffff">
        Introduction
       </font>
      </b>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      align="right">
      &#160;
     </td>
     <td valign="top">
      <p>
       <br />
       This tutorial is meant to demonstrate the use of the newly introduced
       <b>Axis2</b>
       Web Services tools in the Web Tools Platform Project using the
       <b>WTP 2.0</b>
       drivers. Also this tutorial shows how to create a simple top-down Web service from a WSDL file and test that with the WSE (Web Service Explorer). The WSDL file in this scenario calculates the area of an rectangle and its the same WSDL that used in the Axis web services tutorials.
      </p>
      <br />
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      bgcolor="#0080c0"
      align="left"
      colspan="2">
      <b>
       <font
        face="Arial,Helvetica"
        color="#ffffff">
        Set Up
       </font>
      </b>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      align="right">
      &#160;
     </td>
     <td valign="top">
      Before creating the Web service, there are two prerequisites:
      <ol>
       <li>
        <a href="http://www.eclipse.org/webtools/releases/2.0/">Install Latest Eclipse WTP 2.0 integration driver</a>
       </li>
       <li>
        <a href="http://www.eclipse.org/webtools/jst/components/ws/1.5/tutorials/InstallTomcat/InstallTomcat.html">Configure Apache Tomcat inside Eclipse WTP</a>
       </li>
      </ol>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      bgcolor="#0080c0"
      align="left"
      colspan="2">
      <b>
       <font
        face="Arial,Helvetica"
        color="#ffffff">
        Creating a top down Java bean Web service and Web service client using Axis2 WTP Tools
       </font>
      </b>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      align="right">
      &#160;
     </td>
     <td valign="top">
      <p>
       This tutorial need a Axis2 runtime. You can download the latest axis2 binary distribution from
       <a href="http://ws.apache.org/axis2/download.cgi">here</a>
       .
       <br />
       <b>Note :</b>
       Currently Axis2 version 1.2 is the supported version for the Web Services Scenarios.
      </p>
      <ol>
       <li>Download the latest Axis2 runtime from the above link and extract it.</li>
       <br />
       <li>
        Now we point Eclipse WTP to downloaded Axis2 Runtime. Open
        <b>Window -&gt; Preferences -&gt; Web Services -&gt; Axis2 Emitter</b>
       </li>
       <br />
       <img
        border="0"
        src="images/1_n.png" />
       <br />
       <p>Select the Axis2 Runtime tab and point to the correct Axis2 runtime location. Alternatively at the Axis2 Preference tab you can set the default setting that will come up on the Web Services Creation wizards. For the moment we will accept the default settings.</p>
       <li>
        Click OK.
        <br />
        <br />
       </li>
       <li>
        Next we need to create a project with the support of Axis2 features. Open
        <b>File -&gt; New -&gt; Other... -&gt; Web -&gt; Dynamic Web Project</b>
       </li>
       <br />
       <img
        border="0"
        src="images/a1.png" />
       <br />
       <p>Click next</p>
       <li>Select the name Axis2WSTest as the Dynamic web project name(You can specify any name you prefer), and select the configured tomcat runtime as the target runtime</li>
       <br />
       <img
        border="0"
        src="images/a2.png" />
       <br />
       <p>Click next</p>
       <li>Select the Axis2 Project Facet</li>
       <br />
       <img
        border="0"
        src="images/a3_n.png" />
       <br />
       <p>Click next</p>
       <li>Select the Axis2 Project Facet</li>
       <br />
       <img
        border="0"
        src="images/a4.png" />
       <br />
       <p>Click Finish</p>
       <li>This will create a dynamic web project in the workbench</li>
       <br />
       <img
        border="0"
        height="100%"
        width="100%"
        src="images/b1.png" />
       <br />
       <br />
       <br />
       <li>
        Create a folder called WSDL on the Axis2WSTest project. Import the file
        <a href="code/AreaService.wsdl">AreaService.wsdl</a>
        into Axis2WSTest/wsdl.
       </li>
       <br />
       <img
        border="0"
        height="100%"
        width="100%"
        src="images/g1.png" />
       <br />
       <p>Build the Project, if its not auto build.</p>
       <li>Now we are going to invoke the web service creation wizard with respect to the newly imported WSDL. By selecting AreaService.wsdl source the Open File -&gt; New -&gt; Other... -&gt; Web Services -&gt; Web Service</li>
       <br />
       <img
        border="0"
        src="images/g3.png" />
       <br />
       <p>Click next</p>
       <li>
        Because of the wizard was invoked related to a correct WSDL the correct web services scenario will be picked automatically with the correct service definition. Also the configuration must be automatically set to the correct server, web service runtime and the service project. Make sure that
        you have selected the scale until to the start service. Also make sure you have the exact same items for the Server, Web Service Runtime and Service Project. Else make appropriate changes to select them as displayed below.
       </li>
       <br />
       <img
        border="0"
        src="images/g4.png" />
       <br />
       <p>Click next</p>
       <li>This page is the skeleton JAVA bean configuration page. if you have a custom preferences you can edit the configuration as needed, for the moment default will do.</li>
       <br />
       <img
        border="0"
        src="images/g5_n.png" />
       <br />
       <p>Click next</p>
       <li>This page is the start server page. Click on the start server. This will start the server runtime.</li>
       <br />
       <img
        border="0"
        src="images/g6.png" />
       <br />
       <p>Click next</p>
       <li>This page is the web services publication page, accept the defaults.</li>
       <br />
       <img
        border="0"
        src="images/g7.png" />
       <br />
       <p>Click Finish. You will see the newly created skeleton page gets pop up in the Editor. If you need you can edit the skeleton to include your area service logic. But for the moment We will accept the defaults settings.</p>
       <br />
       <li>
        Here after the skeleton generation the user need to fill the nessesary parts of the service skeleton with the bussiness logic. In this example the CalculatedRectArea method TODO plus the exception must be replace with appropriate logic. For that we need some changes in the build system to
        auto reflect the change in the deployed service. One thing is we need to make sure that Build Automatically is enabled in the Project Menu. Then we need to change the output location of the project to the location where the service is deployed in the local web service runtime attached to the
        project. As in this example to the folder WebContent -> axi2-web -> WEB-INF -> services -> Axis2TestWs directory.
       </li>
       <br />
       <img
        border="0"
        src="images/sk_1.png" />
       <br />
       <p></p>
       <br />
       <li>
        So now we have done the nessesary thing to auto deploy our TD created web service. Now we go ahead and implement the skeleton. Replave the TODO plus the exception with the following logic foe calculating the Area.
        <pre>org.tempuri.areaservice.Area area = new org.tempuri.areaservice.Area(); area.setArea( parameters.getParameters().getHeight() * parameters.getParameters().getWidth(); return area;</pre>
       </li>
       <br />
       <img
        border="0"
        src="images/sk_2.png" />
       <br />
       <p>Tigger a auto build after saving. You will notice in the console that the service getting re deployed in the server.</p>
       <br />
       <li>Now we can run the Dynamic Web Project on the server to see our top down created web service running on axis2 server. For that Select the Axis2WSTest dynamic web project and select Run -&gt; Run As -&gt; Run on Server</li>
       <br />
       <img
        border="0"
        src="images/d1.png" />
       <br />
       <p>Click Next.</p>
       <li>Make sure you have the Axis2WSTest dynamic web project on the Configured project.</li>
       <br />
       <img
        border="0"
        src="images/d2.png" />
       <br />
       <p>Click Next.</p>
       <li>This page is the web services publication page, accept the defaults.</li>
       <br />
       <img
        border="0"
        src="images/d3.png" />
       <br />
       <p>Click Finish.</p>
       <li>By doing the above test the Axis2 server webapp will be automatically deployed on the configured servlet container and will display the axis2 home page.</li>
       <br />
       <img
        border="0"
        height="100%"
        width="100%"
        src="images/d5.png" />
       <br />
       <br />
       <li>By selecting services can view the available services. The newly created AreaService will be available there.</li>
       <br />
       <img
        border="0"
        height="100%"
        width="100%"
        src="images/g8.png" />
       <br />
       <br />
       <li>If you need to test the newly creating AreaService with the WSE. at the first page of the skeleton scenario you can increase the slider to the test lever so that the created service can be tested against the Web Services Explorer.</li>
       <br />
       <img
        border="0"
        src="images/wse_1.png" />
       <br />
       <br />
       <li>So after the normal pagers it will add another additional page to select the test facility. Select the Web Service Explorer</li>
       <br />
       <img
        border="0"
        src="images/wse_2.png" />
       <br />
       <br />
       <li>Now For testing of the newly created AreaService with the WSE, at the end of the scenario it will launch the Web Services Explorer with the nessesary configuration loaded so that user can invoke the service easily.</li>
       <br />
       <img
        border="0"
        height="100%"
        width="100%"
        src="images/wse_3.png" />
       <br />
       <br />
       <li>
        Please refer the
        <a href="http://www.eclipse.org/webtools/jst/components/ws/1.0/tutorials/WebServiceExplorer/WebServiceExplorer.html">Using Web Service Explorer to test a Web service</a>
        for more details.
        <br />
        <br />
        <br />
        <br />
       </li>
      </ol>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      bgcolor="#0080c0"
      align="left"
      colspan="2">
      <b>
       <font
        face="Arial,Helvetica"
        color="#ffffff">
        Summary
       </font>
      </b>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      align="right">
      &#160;
     </td>
     <td valign="top">
      <p>
       The Web Service wizard is orchestrating the end-to-end generation, assembly, deployment, installation and execution of the Web service, Web service client. In this scenario, we completed the top-down web service creation scenario using WTP web services tools. Most of the time the wizard will
       pick reasonable defaults according to the basic high-level choices. After completing this scenario, the WSDL for the AreaService Web service can be found in Clicking on the available services converter service link Now that your Web service is running, there are a few interesting things you
       can do with this WSDL file. Examples:
       <ul>
        <li>You can choose Web Services -&gt; Test with Web Services Explorer to test the service.</li>
        <li>You can choose Web Services -&gt; Publish WSDL file to publish the service to a public UDDI registry.</li>
        <li>You choose New -&gt; Other... -&gt; Web Services -&gt; Web Service Client to generate a Web service client.</li>
       </ul>
      </p>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      bgcolor="#0080c0"
      align="left"
      colspan="2">
      <b>
       <font
        face="Arial,Helvetica"
        color="#ffffff">
        Resources
       </font>
      </b>
     </td>
    </tr>
    <tr>
     <td
      valign="top"
      align="right">
      &#160;
     </td>
     <td valign="top">
      <p>
       You can also refer the Axis web services creation wizards.
       <ul>
        <li>
         <a href="http://www.eclipse.org/webtools/jst/components/ws/1.0/tutorials/WebServiceExplorer/WebServiceExplorer.html">Using Web Service Explorer to test a Web service</a>
        </li>
        <li>
         <a href="http://www.eclipse.org/webtools/jst/components/ws/1.5/tutorials/BottomUpWebService/BottomUpWebService.html">Creating Bottom Up Web Service via Axis</a>
        </li>
        <li>
         <a href="http://www.eclipse.org/webtools/jst/components/ws/1.5/tutorials/TopDownWebService/TopDownWebService.html">Creating Top Down Web Service via Axis</a>
        </li>
        <li>
         <a href="http://www.eclipse.org/webtools/jst/components/ws/1.5/tutorials/WebServiceClient/WebServiceClient.html">Consuming Web service using Web Service Client via Axis</a>
        </li>
       </ul>
      </p>
     </td>
    </tr>
   </tbody>
  </table>
 </body>
</html>
