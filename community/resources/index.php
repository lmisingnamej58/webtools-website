<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageKeywords	= "";
$pageAuthor		= "Naci Dai";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

# Generate the web page
// Load the XML source
$xml = DOMDocument::load('resources.xml');

//Set the page title
$pageTitle = "Eclipse Web Tools Community Resources";

// Load the XSL source
$xsl = DOMDocument::load('resources.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$maincontent = $proc->transformToXML($xml);
//$maincontent = htmlentities($maincontent);
$html = <<<EOHTML
 <html>
  <head>
	<link media="screen" href="resources.css" type="text/css" rel="stylesheet"/>
	<script type="text/javascript" src="resources.js"></script>
  </head>
  <body onLoad="format_filter('all')">
  $wtpTopButtons
	<div id="maincontent">
			$maincontent
	</div>
  </body>
 </html>


EOHTML;
		$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
		?>
