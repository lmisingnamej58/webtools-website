<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>

<xsl:output method='xml' indent="yes" />

<xsl:template match='presentations'>

<html>
<head><title>Eclipse Web Tools Platform Project Presentation Archive</title></head>
<body>

<h1>wtp presentation archive</h1>

<h2>Introduction</h2>

<p>
This page lists presentations on WTP and related topics.
Feel free to reuse this material in your own presentations. 
</p>
<p>
If you have a presentation that you'd like to share with the WTP community,
please contribute it by creating a <a href='https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools'>Bugzilla</a> bug against WTP. Use
<ul>
<li><b>Product:</b> Web Tools</li>
<li><b>Component:</b> website</li>
<li><b>Severity:</b> enhancement</li>
</ul>
Include the title, speaker, venue, and date in the bug comments.
Attach your presentation to the bug report. 
</p>

<h2>Presentations</h2>

<xsl:apply-templates select='presentation'/>

</body>
</html>

</xsl:template>

<xsl:template match='presentation'>

<xsl:variable name="file" select="normalize-space(file)"/>

<h3>
<a name='{$file}' href='{../@base}{$file}'><em><xsl:value-of select='title'/></em></a>

<xsl:apply-templates select='translation'/>

<xsl:apply-templates select='presenter'/>

<xsl:text>, </xsl:text>
<xsl:value-of select='venue'/>

<xsl:text>, </xsl:text>
<xsl:value-of select='date'/>

<xsl:text>. </xsl:text>
</h3>

</xsl:template>

<xsl:template match='translation'>
<xsl:variable name="file" select="normalize-space(.)"/>
<xsl:text> [</xsl:text>
<a href='{../../@base}{$file}'><xsl:value-of select='@language'/></a>
<xsl:text>]</xsl:text>
</xsl:template>

<xsl:template match='presenter'>
<xsl:text>, </xsl:text>
<xsl:value-of select='.'/>
</xsl:template>

</xsl:stylesheet>
