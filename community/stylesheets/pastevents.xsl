<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns:xalan="http://xml.apache.org/xalan"
	xmlns:events="http://www.eclipse.org/webtools/events"
	xmlns:date="http://exslt.org/dates-and-times"
    exclude-result-prefixes="date events xalan">

	<xsl:include href="wtp-common.xsl" />
	<xsl:include href="events.xsl"/>

	<xsl:template match="/">
		<xsl:param name="title" select="'wtp community'"/>
		<xsl:param name="subtitle" select="'past activities'"/>
		<xsl:param name="root" select="'../..'"/>
  		<xsl:call-template name="page">
    		<xsl:with-param name="element" select="events:events"/>
    		<xsl:with-param name="title" select="$title"/>
    		<xsl:with-param name="subtitle" select="$subtitle"/>
    		<xsl:with-param name="root" select="$root"/>
  		</xsl:call-template>
	</xsl:template>

	<xsl:template match="events:events" mode="communityresourcespage">
		<xsl:param name="title" select="'wtp community resources'"/>
		<xsl:param name="subtitle" select="''"/>
		<xsl:param name="root" select="'../..'"/>
  		<xsl:call-template name="page">
    		<xsl:with-param name="element" select="."/>
    		<xsl:with-param name="title" select="$title"/>
    		<xsl:with-param name="subtitle" select="$subtitle"/>
    		<xsl:with-param name="root" select="$root"/>
  		</xsl:call-template>
	</xsl:template>

	<xsl:template name="aboutPage">
		<xsl:param name="resources"/>
		<xsl:call-template name="heading">
			<xsl:with-param name="title" select="'Activities and resources from around the WTP community...'"/>
			<xsl:with-param name="alink" select="'top'"/>
		</xsl:call-template>
		<table cellspacing="5" cellpadding="2" border="0">
			<tr>
				<td with="16">&#160;&#160;&#160;&#160;</td>
				<td>
					<p>
			The page is about finding more information about WTP from around the WTP community. 
        	The community has provided many <xsl:apply-templates select="xalan:nodeset($resources)" mode="body"/> 
			that are dedicated to WTP and can help you to make the most of the WTP platform and tools.
					</p>
					<p>
			Current WTP activity information can be found on the <a href="community.html">community page</a>.
					</p>
        			<p>
			We'd like these pages to be useful to you - so please open a 
			<a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools&amp;component=website">bug report</a> 
			for any corrections, additions, pointers, or comments!
        			</p>
					<p>
        	For software licensing, website terms of use, and legal FAQs, please see our 
			<a href="http://eclipse.org/legal/main.html" target="_top">legal stuff</a> page.
					</p>
				</td>
				<td width="16">&#160;</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template name="endpage">
		<xsl:param name="root"/>
		<p></p>
        <p>Please see our <a href="http://www.eclipse.org/legal/privacy.html">privacy 
        policy</a> and website <a href="http://www.eclipse.org/legal/termsofuse.html">terms 
        of use</a>. For problems with the eclipse.org site, please contact the 
        <a href="mailto:webmaster@eclipse.org">webmaster</a> or read the 
        <a href="{$root}/../webmaster/index.html">webmaster FAQ</a> for answers to common questions!</p>
	</xsl:template>
	
	<xsl:template name="heading">
		<xsl:param name="alink"/>
		<xsl:param name="title"/>
		<xsl:param name="toplink"/>
		<table bgcolor="#0080c0" border="0" cellpadding="2" cellspacing="0" width="100%">
			<tbody>
				<tr>
					<td width="90%">
						<b>
							<font color="#ffffff" face="Arial,Helvetica">
								<a name="{$alink}"><xsl:value-of select="$title"/></a>
							</font>
						</b>
					</td>
					<td width="10%" align="right">
						<xsl:if test="$toplink != ''">
							<a href="{$toplink}">top</a>
						</xsl:if>
					</td>
				</tr>
			</tbody>
		</table>
	</xsl:template>

	<xsl:template match="events:events">
		<xsl:param name="root"/>
		
		<xsl:variable name="resources"><xsl:for-each select="document('../communityresources.xml')/resources/resource"><xsl:sort select="@shortTitle"/><xsl:choose><xsl:when test="position() = last()">and <a href="communityresources.html#{@shortTitle}"><xsl:value-of select="@shortTitle"/></a></xsl:when><xsl:otherwise><a href="communityresources.html#{@shortTitle}"><xsl:value-of select="@shortTitle"/></a>, </xsl:otherwise></xsl:choose></xsl:for-each></xsl:variable>
		<xsl:call-template name="aboutPage">
			<xsl:with-param name="resources" select="$resources"/>
		</xsl:call-template>

		<!-- Create page sections. -->
		<xsl:call-template name="heading">
			<xsl:with-param name="title" select="'Past WTP Activities'"/>
			<xsl:with-param name="alink" select="''"/>
			<xsl:with-param name="toplink" select="'#top'"/>
		</xsl:call-template><!-- date:format-date(date:date(),'yyyy-MM-dd') -->
		<xsl:variable name="date" select="date:date()"/>
		<xsl:for-each select="events:event[date:year(@enddate) &lt; date:year($date) or (date:year(@enddate) = date:year($date) and date:day-in-year(@enddate) &lt;= date:day-in-year($date))]">
			<xsl:sort select="@startdate" order="descending"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		<xsl:call-template name="endpage">
			<xsl:with-param name="root" select="$root"/>
		</xsl:call-template>
	</xsl:template>

</xsl:stylesheet>
