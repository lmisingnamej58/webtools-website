<?php  				
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	

$App = new App();	
$Nav = new Nav();	
$Menu = new Menu();		

include($App->getProjectCommon());

	#*****************************************************************************
	#
	# Copyright (c) 2010, 2016 IBM Corporation and others.
 	# All rights reserved. This program and the accompanying materials
 	# are made available under the terms of the Eclipse Public License v1.0
 	# which accompanies this distribution, and is available at
 	# http://www.eclipse.org/legal/epl-v10.html
 	# Contributors:
	#     IBM Corporation - initial implementation
	#
	# Author: 		Ant Team
	# Date:			August 30, 2010
	#
	# Description: This is the new main page for the JSDT Team website
	#
	#****************************************************************************
	
	$pageTitle 		= "JavaScript Development Tools UI";
	$pageKeywords	= "javascript, JavaScript, JSDT, platform, debugging, debugger, jsdt, breakpoints, about, content, epl, compiler, compile, parse, parser";
	$pageAuthor		= "JSDT Team";
	
	include("../_sideCommon.php");
	include("_pluginCommon.php");
	
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<div class="homeitem3col">
			<h3>Project Overview</h3>
			<p>
				
			</p>
		</div>
		<div class="homeitem3col">
			<h3>JSDT Web Overview</h3>
			<p>
				The JSDT Web integrates JavaScript support within web applications written with <em>classic</em> languages, such as HTML or JSP.
				The project sources are located into the <em>webtools.sourceediting</em> repository, under the <code>org.eclipse.wst.jsdt.web.*</code> namespace. 
			</p>
		</div>
		<div class="homeitem3col">
			<h3>JSDT Web Bundles</h3>
			<p>
				The JSDT Web is broken down in bundles as follows: 
				<ol>
					<li><strong>org.eclipse.wst.jsdt.web.core</strong> - core bundle for JSDT support within HTML.</li>
					<li><strong>org.eclipse.wst.jsdt.web.core.tests</strong> - tests for the Core.</li>
					<li><strong>org.eclipse.wst.jsdt.web.support.jsp</strong> - Support for JavaScript inside JSP, depends on Core and UI.</li>
					<li><strong>org.eclipse.wst.jsdt.web.ui</strong> - Integration of JSDT Web into Eclipse UI.</li>
					<li><strong>org.eclipse.wst.jsdt.web.ui.tests</strong> - tests for the UI.</li>
				</ol>
			</p>
		</div>
		<div class="homeitem3col">
			<h3>Up To The Minute</h3>
			<p>
				JSDT is a project changing rapidly, but not as fast as latest trends in web technologies.
				Please, check also the <a href="https://wiki.eclipse.org/JSDT">JSDT Wiki</a> to get the most recent updates.
			</p>
		</div>
	</div>
	<div id="rightcolumn">
		$commonside
		$commonplugin
	</div>
</div>

EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
