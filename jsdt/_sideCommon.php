<?php

	#*****************************************************************************
	#
	# Copyright (c) 2010 IBM Corporation and others.
 	# All rights reserved. This program and the accompanying materials
 	# are made available under the terms of the Eclipse Public License v1.0
 	# which accompanies this distribution, and is available at
 	# http://www.eclipse.org/legal/epl-v10.html
 	# Contributors:
	#     IBM Corporation - initial implementation
	#
	# Author: 		JSDT Team
	# Date:			August 30, 2010
	#
	# Description: This is a contributed item that provides a common side bar for links etc.
	#
	#****************************************************************************
    
    $commonside = <<<EOHTML
	<div class="sideitem">
		<h6>Quick Links</h6>
        <ul>
            <li><a href="https://wiki.eclipse.org/JSDTestScenarios">Test Scenarios</a></li>
            <li><a href="/webtools/jsdt/who.php">Who We Are</a></li>
            <li><a href="https://wiki.eclipse.org/JSDT">Wiki</a></li>
            <li><a href="/webtools/jsdt/about.php">About This Content</a></li>
        </ul>
	</div> 
EOHTML
?>