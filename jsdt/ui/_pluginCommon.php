<?php

	#*****************************************************************************
	#
	# Copyright (c) 2010 IBM Corporation and others.
 	# All rights reserved. This program and the accompanying materials
 	# are made available under the terms of the Eclipse Public License v1.0
 	# which accompanies this distribution, and is available at
 	# http://www.eclipse.org/legal/epl-v10.html
 	# Contributors:
	#     IBM Corporation - initial implementation
	#
	# Author: 		JSDT Team
	# Date:			August 30, 2010
	#
	# Description: This is a contributed item that provides a common side bar for links etc.
	#
	#
	#****************************************************************************
    
    $commonplugin = <<<EOHTML
    <div class="sideitem">
		<h6>Build Notes</h6>
		
	</div>
	<div class="sideitem">
		<h6>Bundles</h6>
		
	</div>
EOHTML
?>

