<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

#*****************************************************************************
#
# template.php
#
# Author: 		15022010
# Date:			2010-02-10
#
# Description: Type your page comments here - these are not sent to the browser
#
#
#****************************************************************************

#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Web Tools Platform (WTP) Project";
$pageKeywords	= "web,J2EE";
$pageAuthor		="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
// Import the functions that render news.
// require_once ($root . '/webtools/news.php');
// Import common WTP functions.
require_once ($root . '/webtools/common.php');
require_once ($root . '/webtools/newstohtml.php');

// Generate the HTML content for the news based on the news.xml file
// that appears in the root directory.
$newsTitle = "Project News";
//$wtpnews = news_to_html($root . "/webtools/news.xml", "$newsTitle", "/webtools/news/", "/webtools/news.php", true, "long", "5");
$wtpnews = news_to_html($root . "/webtools/news.xml", "", "", "", true, "long", "3");

// Get the project descriptions
$wtpoverview = read_file($root . '/webtools/project-info/description.html');
$commonoverview = read_file($root . '/webtools/project-info/common-description.html');
$ejboverview = read_file($root . '/webtools/project-info/ejb-description.html');
$jeeoverview = read_file($root . '/webtools/project-info/jee-description.html');
$jsdtoverview = read_file($root . '/webtools/project-info/jsdt-description.html');
$rdboverview = read_file($root . '/webtools/project-info/rdb-description.html');
$serveroverview = read_file($root . '/webtools/project-info/server-description.html');
$sseoverview = read_file($root . '/webtools/project-info/sse-description.html');
$wsoverview = read_file($root . '/webtools/project-info/ws-description.html');
$dalioverview = read_file($root . '/webtools/project-info/dali-description.html');
$jsfoverview = read_file($root . '/webtools/project-info/jsf-description.html');
$incoverview = read_file($root . '/webtools/project-info/inc-description.html');
$relengoverview = read_file($root . '/webtools/project-info/releng-description.html');
$libraoverview = read_file($root . '/webtools/project-info/libra-description.html');
# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links", 	"downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

$wtpTopButtons

<div id="midcolumn" >

	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
		<tr>
			<td width="100"></td>
			<td><div style="text-align:center;"><a href="/webtools/community/"><img src="/webtools/images/main_users.png" /></a></div></td>
			<td><div style="text-align:center;"><a href="/webtools/adopters/"><img src="/webtools/images/main_adopters.png" /></a></div></td>
			<td><div style="text-align:center;"><a href="/webtools/development/"><img src="/webtools/images/main_committers.png" /></a></div></td>
			<td width="100"></td>
		</tr>
	</table>

	<div class="homeitem">
		<h3>Subprojects</h3>
		<ul>
			<li>
				<b>Common Components</b>&nbsp;<a href="common/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$commonoverview
				<br /> <a href="common/">more about common components &raquo;</a>
			</li>
			<li>
				<b>Dali JPA Tools</b>&nbsp;<a href="dali/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$dalioverview
				<br /> <a href="dali/">more about dali &raquo;</a>
			</li>
			<li>
				<b>EJB Tools</b>&nbsp;<a href="ejb/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$ejboverview
				<br /> <a href="ejb/">more about EJB Tools&raquo;</a>
			</li>
			<li>
				<b>Enterprise Tools for the OSGi Service Platform (Libra)</b>&nbsp;<a href="libra/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$libraoverview
				<br /> <a href="libra/">more about Libra&raquo;</a>
			</li>
			<li>
				<b>Java EE Tools</b>&nbsp;<a href="jee/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$jeeoverview
				<br /> <a href="jee/">more about Java EE Tools&raquo;</a>
			</li>
			<li>
				<b>JavaScript Development Tools (JSDT)</b>&nbsp;<a href="jsdt"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$jsdtoverview
				<br /> <a href="jsdt">more about JavaScript Development Tools&raquo;</a>
			</li>
			<li>
				<b>JavaServer Faces Tools (JSF)</b>&nbsp;<a href="jsf/main.php"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$jsfoverview
				<br /> <a href="jsf/main.php">more about jsf &raquo;</a>
			</li>
			<li>
				<b>Release Engineering (releng)</b>&nbsp;<a href="releng/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$relengoverview
				<br /> <a href="releng/">more about Releng&raquo;</a>
			</li>
			<li>
				<b>Server Tools</b>&nbsp;<a href="server/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$serveroverview
				<br /> <a href="server/">more about Server Tools&raquo;</a>
			</li>
			<li>
				<b>Source Editing</b>&nbsp;<a href="sse/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$sseoverview
				<br /> <a href="sse/">more about source editing&raquo;</a>
			</li>
			<li>
				<b>Web Services Tools</b>&nbsp;<a href="ws/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$wsoverview
				<br /> <a href="ws/">more about Web services tools&raquo;</a>
			</li>
			<li>
				<b>WTP Incubator</b>&nbsp;<a href="ws/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
				<br />
				$incoverview
				<br /> <a href="incubator/">more about WTP Incubator&raquo;</a>
   			</li>
    	</ul>
	</div>
</div>

<div id="rightcolumn">
	<div class="sideitem">
		<h6>$newsTitle
			<a href="/webtools/news.php"><img src="https://eclipse.org/images/more.gif" title="More..." alt="[More]" /></a>
			<a href="/webtools/news/"><img src="https://eclipse.org/images/rss.gif" align="right" title="RSS Feed" alt="[RSS]" /></a>
		</h6>
		$wtpnews
	</div>
</div>
EOHTML;

# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
