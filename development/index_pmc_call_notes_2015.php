<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon()); # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>
<h2>Call Info</h2>

<p>Toll free (in US and Canada): 888-426-6840 <br />
Access code: 18184877# <br />
Caller pay alternative number: 215-861-6239<br />
<a
	href="https://www.teleconference.att.com/servlet/glbAccess?process=1&accessCode=6460142&accessNumber=2158616239">Full
list of global access numbers</a><br />
<br />
<p><a href="http://www.timeanddate.com/worldclock/custom.html?cities=224,207,1440,107&amp;hour=11&amp;min=0&amp;sec=0&amp;p1=207">Call
Time: 1500 UTC</a></p>

<h2>Meeting agendas and minutes</h2>
<ul compact="true">

<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-12-08">December 8, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-11-10">November 10, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-10-27">October 27, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-10-13">October 13, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-09-29">September 29, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-09-15">September 15, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-09-01">September 1, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-08-18">August 18, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-07-21">July 21, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-06-16">June 16, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-05-26">May 26, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-05-12">May 12, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-04-28">April 28, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-03-31">March 31, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-03-17">March 17, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-03-03">March 3, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-02-17">February 17, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-02-03">February 3, 2015</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2015-01-06">January 6, 2015</a></li>

<li><a href="index_pmc_call_notes_2014.php">2014</a></li>
<li><a href="index_pmc_call_notes_2013.php">2013</a></li>
<li><a href="index_pmc_call_notes_2012.php">2012</a></li>
<li><a href="index_pmc_call_notes_2011.php">2011</a></li>
<li><a href="index_pmc_call_notes_2010.php">2010</a></li>
<li><a href="index_pmc_call_notes_2009.php">2009</a></li>
<li><a href="index_pmc_call_notes_2008.php">2008</a></li>
<li><a href="index_pmc_call_notes_2007.php">2007</a></li>
<li><a href="index_pmc_call_notes_2006.php">2006</a></li>
<li><a href="index_pmc_call_notes_2005.php">2005</a></li>
<li><a href="index_pmc_call_notes_2004.php">2004</a></li>
</ul>
<hr />
<!-- <p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
-->
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadHTML($xmlString);
// Load the XSL source
//$xsl = DOMDocument::load($root . '/webtools/wtpphoenix.xsl');
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');
// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

//echo "xml:";
//echo $xml->saveXML();
$maincontent = $proc->transformToXml($xml);
//echo "maincontent:";
//echo $maincontent;
$html = <<<EOHTML
<div id="maincontent">
$wtpTopButtons
$maincontent
</div>
EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
