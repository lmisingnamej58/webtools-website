<?xml version="1.0" encoding="UTF-8"?>

<!--

	Generates Help Wanted page from Milestone plans.

	ChangeLog:
	
	2005-02-13: Arthur Ryman, ryman@ca.ibm.com
	- cloned from milestone-overview.xsl
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../stylesheets/milestone-common.xsl" />
	<xsl:variable name="plans" select="document(/report/milestone/plan/@location)/plan" />
	<xsl:variable name="rel" select="'../../../'" />
	<xsl:variable name="title" select="'eclipse WTP help wanted'" />
	<xsl:template match="report">
		<html>
			<head>
				<title>
					<xsl:value-of select="$title" />
				</title>
				<link href="../stylesheets/images/default_style.css" type="text/css" rel="stylesheet" />
			</head>
			<body>

				<xsl:call-template name="printHeader">
					<xsl:with-param name="rel" select="$rel" />
					<xsl:with-param name="title" select="$title" />
					<xsl:with-param name="description" select="'eclipse web tools platform project help wanted'" />
				</xsl:call-template>

				<table width="100%">
					<tr>
						<td height="20" bgcolor="#0080c0" colspan="2">
							<b>
								<font color="#ffffff">Help Wanted</font>
							</b>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							Like all Open Source projects, WTP depends on an active community of contributors to accomplish its goals.
							This page lists work items where help is needed. 
							If you'd like to contribute, please review this page to find a task that matches your interests and skills,
							then send a note to <a href="mailto:wtp-dev@eclipse.org">wtp-dev@eclipse.org</a> volunteering your assistance.
							The responsible component lead will contact you to coordinate your work.
						</td>
					</tr>
					
				</table>
				
				<br />
				
				<table width="100%">
					<tr>
						<td>
							<img>
								<xsl:attribute name="src">
									<xsl:value-of select="concat($rel, 'development/milestone_plans/stylesheets/images/new.gif')" />
								</xsl:attribute>
							</img>
							You can also see the list of help wanted bugs <a href="https://bugs.eclipse.org/bugs/report.cgi?x_axis_field=priority&amp;y_axis_field=component&amp;z_axis_field=&amp;query_format=report-table&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=Web+Tools&amp;long_desc_type=allwordssubstr&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=helpwanted&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;emailtype1=substring&amp;email1=&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=&amp;chfieldto=Now&amp;chfieldvalue=&amp;format=table&amp;action=wrap&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">by priority</a> and
							<a href="https://bugs.eclipse.org/bugs/report.cgi?x_axis_field=bug_severity&amp;y_axis_field=component&amp;z_axis_field=&amp;query_format=report-table&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=Web+Tools&amp;long_desc_type=allwordssubstr&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=helpwanted&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;emailtype1=substring&amp;email1=&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=&amp;chfieldto=Now&amp;chfieldvalue=&amp;format=table&amp;action=wrap&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">by severity</a>
						</td>
					</tr>
				</table>

				<br />
				
				<table width="100%">
					<tr align="left">
						<th>Yes, I'll Help!</th>
						<th>Milestone</th>
						<th>Priority</th>
						<th>Subproject</th>
						<th>Component</th>
						<th>Category</th>
						<th>Description</th>
					</tr>
					<xsl:for-each select="document(/report/milestone[substring(@name, 5) >= substring(/report/@current-milestone, 5)]/plan/@location)/plan/component/milestone/category/item[@helpWanted='true']">
						<xsl:sort data-type="number" select="ancestor::milestone/@name" />
						<xsl:sort select="translate(substring(@priority,1,1),'hml','123')" />
						<xsl:sort select="ancestor::component/@subproject" />
						<xsl:sort select="ancestor::component/@name" />
						<xsl:sort select="ancestor::category/@name" />

		<xsl:variable name="milestoneName" select="ancestor::component/milestone[1]/@name" />
		<xsl:variable name="component" select="ancestor::component" />
		<xsl:variable name="url" select="concat($rel, $component/@subproject, '/components/', $component/@name, '/', $milestoneName, '/', 'milestone_plan.html')" />

					<xsl:variable name="subject">
						<xsl:text>[Help Wanted] </xsl:text>
						<xsl:value-of select="ancestor::milestone/@name" />
						<xsl:text> </xsl:text>
						<xsl:value-of select="ancestor::component/@subproject" />
						<xsl:text>.</xsl:text>
						<xsl:value-of select="ancestor::component/@name" />
						<xsl:text>: </xsl:text>
						<xsl:value-of select="ancestor::category/@name" />
					</xsl:variable>
					<xsl:variable name="url-encoded-subject">
						<xsl:call-template name="url-encode">
							<xsl:with-param name="s" select="$subject" />
						</xsl:call-template>
					</xsl:variable>
					
					<xsl:variable name="body">
						<xsl:text>I'd like to help with the item: </xsl:text>
						<xsl:value-of select="description" />
					</xsl:variable>
					<xsl:variable name="url-encoded-body">
						<xsl:call-template name="url-encode">
							<xsl:with-param name="s" select="$body" />
						</xsl:call-template>
					</xsl:variable>
					
					<!-- TODO: would be nice if this could read the developer element for contact info -->
					<xsl:variable name="mailto-url">
						<xsl:text>mailto:wtp-dev@eclipse.org?subject=</xsl:text>
						<xsl:value-of select="$url-encoded-subject" />
						<xsl:text>&amp;body=</xsl:text>
						<xsl:value-of select="$url-encoded-body" />
					</xsl:variable>
					
					<tr>
						<td><a href="{$mailto-url}">Send</a></td>
						<td><xsl:value-of select="ancestor::milestone/@name" /></td>
						<td><xsl:value-of select="@priority" /></td>
						<td><xsl:value-of select="ancestor::component/@subproject" /></td>
						
						<td>
							<a href="{$url}">
								<xsl:value-of select="$component/@name" />
							</a>
						</td>
						
						<td><xsl:value-of select="../@name" /></td>
						
						<td>
							<xsl:call-template name="drawStatus">
								<xsl:with-param name="rel" select="$rel" />
								<xsl:with-param name="value" select="@status" />
								<xsl:with-param name="defaultImage" select="'default.gif'" />
							</xsl:call-template>
							<xsl:text>&#160;&#160;</xsl:text>
							<xsl:value-of select="description" />
  				   			<xsl:value-of select="'  '" />                
							<xsl:call-template name="drawHelpWanted">
								<xsl:with-param name="rel" select="$rel" />
							</xsl:call-template>

						</td>
					</tr>
					</xsl:for-each>
				</table>
				
				<br />
				<br />
				<xsl:call-template name="printLegend">
					<xsl:with-param name="rel" select="$rel" />
				</xsl:call-template>
				<br />
				<br />
				<br />
				<p>
					Please see our
					<a href="http://eclipse.org/legal/privacy.html">privacy policy</a>
					and website
					<a href="http://eclipse.org/legal/termsofuse.html">terms of use</a>
					. For problems with the eclipse.org site, please contact the
					<a href="mailto:webmaster@eclipse.org">webmaster</a>
					or read the
					<a href="http://eclipse.org/webmaster/index.html">webmaster FAQ</a>
					for answers to common questions!
				</p>
			</body>
		</html>
	</xsl:template>
	
	<!-- URL encodes a string by replacing unsafe characters with %dd where dd is the hex for the character. -->
	<xsl:template name="url-encode">
		<xsl:param name="s" />
		<xsl:if test="string-length($s) &gt; 0">
			<xsl:variable name="c" select="substring($s, 1, 1)" />
			<xsl:choose>
				<xsl:when test="$c = ' '">
					<xsl:text>%20</xsl:text>
				</xsl:when>
				<xsl:when test="$c = '%'">
					<xsl:text>%25</xsl:text>
				</xsl:when>
				<xsl:when test="$c = '&amp;'">
					<xsl:text>%26</xsl:text>
				</xsl:when>
				<xsl:when test="$c = '?'">
					<xsl:text>%3F</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$c" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="url-encode">
				<xsl:with-param name="s" select="substring($s, 2)" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>