<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../stylesheets/milestone-common.xsl" />
	<xsl:variable name="datesDocument" select="/report/milestoneDates/@location" />
	<xsl:variable name="targetMilestone" select="/report/milestone/@name" />
	<xsl:variable name="milestoneDates" select="document($datesDocument)//milestone[@name = $targetMilestone]" />
	<xsl:variable name="weeks" select="$milestoneDates//week" />
	<xsl:variable name="plans" select="document(/report/plan/@location)/plan" />
	<xsl:variable name="developers" select="/report/developer" />
	<xsl:variable name="rel" select="'../../../'" />

	<xsl:template match="report">
		<html>
			<head>
				<title>Milestone Plan - Calendar View</title>
				<link href="../stylesheets/images/default_style.css" type="text/css" rel="stylesheet" />
			</head>
			<body>

				<xsl:call-template name="printHeader">
					<xsl:with-param name="rel" select="$rel"></xsl:with-param>
					<xsl:with-param name="title" select="/report/title" />
					<xsl:with-param name="description" select="/report/desrcription" />
				</xsl:call-template>

				<table border="1">
					<xsl:attribute name="width">
						<xsl:value-of select="concat(count(/report/milestone) * 100, '%')" />
					</xsl:attribute>
					<tbody>
						<tr>
							<td></td>
							<td width="20%"></td>
							<xsl:for-each select="$milestoneDates">
								<td>
									<xsl:attribute name="colspan">
										<xsl:value-of select="count(week) + 2" />
									</xsl:attribute>
									<b>
										milestone
										<xsl:value-of select="@name" />
									</b>
								</td>
							</xsl:for-each>
						</tr>
						<tr>
							<td></td>
							<td width="200"></td>
							<xsl:for-each select="$weeks">
								<td width="100">
									<b>
										<xsl:value-of select="@date" />
									</b>
								</td>
							</xsl:for-each>
						</tr>
						<xsl:call-template name="generateRowsForDevelopers" />
					</tbody>
				</table>
				<xsl:call-template name="printLegend">
					<xsl:with-param name="rel" select="$rel" />
				</xsl:call-template>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="generateRowsForDevelopers">
		<xsl:for-each select="$developers">
			<xsl:sort select="@name" />
			<xsl:variable name="theName" select="@name"></xsl:variable>
			<xsl:variable name="items" select="$plans//item[developer/@name = $theName][ancestor::milestone/@name = $targetMilestone]" />
			<tr>
				<td height="30" bgcolor="#0080c0">
					<xsl:attribute name="colspan">
						<xsl:value-of select="count($weeks) + 2" />
					</xsl:attribute>
					<b>
						<font color="#ffffff">
							<xsl:value-of select="@name" />
							(
							<xsl:value-of select="sum($items//@pds)" />
							)
						</font>
					</b>
				</td>
			</tr>
			<xsl:choose>
				<xsl:when test="count($items) > 0">
					<xsl:apply-templates select="$items">
						<xsl:with-param name="developerName" select="$theName"></xsl:with-param>
						<xsl:sort select="ancestor::milestone/@name" />
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<td></td>
						<td>
							<xsl:attribute name="colspan">
								<xsl:value-of select="count($weeks) + 2" />
							</xsl:attribute>
							no items
						</td>
					</tr>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="item">
		<xsl:param name="developerName"></xsl:param>
		<tr>
			<td />
			<xsl:variable name="theItem" select="."></xsl:variable>
			<td height="100">
				<ul>
					<xsl:call-template name="drawStatus">
						<xsl:with-param name="rel" select="$rel"></xsl:with-param>
						<xsl:with-param name="value" select="$theItem/@status" />
					</xsl:call-template>
					<xsl:value-of select="' '" />
					[<xsl:value-of select="ancestor::component/@name" />]
					<xsl:copy-of select="description" />
					(<xsl:value-of select="sum($theItem//@pds)" />)
				</ul>
			</td>

			<xsl:for-each select="$milestoneDates">
				<xsl:variable name="milestoneName" select="@name" />
				<xsl:choose>
					<xsl:when test="$theItem/ancestor::milestone/@name = $milestoneName">
						<xsl:for-each select="week">
							<td>
								<xsl:variable name="p" select="position()" />
								<xsl:variable name="steps" select="$theItem/step[not(developer)] | $theItem/step[developer/@name = $developerName]" />
								<xsl:variable name="newSteps" select="$steps[@start = $p]" />
								<xsl:variable name="oldSteps" select="$steps[@length][@start &lt; $p][@start + @length &gt; $p]" />
									<xsl:for-each select="$newSteps">
										<p>
											<xsl:if test="@start = $p">
												<font size="1">
													<xsl:call-template name="setFontColorForStep">
														<xsl:with-param name="step" select="."></xsl:with-param>
													</xsl:call-template>
													<xsl:call-template name="drawStatus">
														<xsl:with-param name="rel" select="$rel" />
														<xsl:with-param name="value" select="@status" />
														<xsl:with-param name="defaultImage" select="'default.gif'" />
													</xsl:call-template>
													<xsl:choose>
														<xsl:when test="description">                                                                            
															<xsl:copy-of select="description" />
															<xsl:value-of select="concat(' (', @pds, ')')" />
															<xsl:if test="@length > 1">
																<font color="#999999">
																	<xsl:value-of select="concat(' [1/', @length, ' complete]')" />
																</font>
															</xsl:if>
														</xsl:when>
														<xsl:when test="@length > 1">
															<font color="#999999" size="1">
																<xsl:value-of select="concat(' [', ($p - @start + 1), '/', @length, ' complete]')" />
															</font>
														</xsl:when>
														<xsl:otherwise>                                                            
															<font scolor="#999999" size="1">[complete]</font>
														</xsl:otherwise>
													</xsl:choose>
												</font>
											</xsl:if>
										</p>
									</xsl:for-each>
									<xsl:for-each select="$oldSteps">
										<p>
											<font size="1">
												<xsl:call-template name="setFontColorForStep">
													<xsl:with-param name="step" select="."></xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="drawStatus">
													<xsl:with-param name="rel" select="$rel" />
													<xsl:with-param name="value" select="@status" />
													<xsl:with-param name="defaultImage" select="'default.gif'" />
												</xsl:call-template>
												<xsl:if test="description">
													<xsl:value-of select="substring(description, 0, 15)" />
													...
												</xsl:if>
												<xsl:choose>
													<xsl:when test="@start + @length -1 = $p">
														<font color="#999999" size="1">[complete]</font>
													</xsl:when>
													<xsl:otherwise>
														<font color="#999999" size="1">
   														  <xsl:value-of select="concat(' [', ($p - @start + 1), '/', @length, ' complete]')" />
														</font>
													</xsl:otherwise>
												</xsl:choose>
											</font>
										</p>
									</xsl:for-each>
									<xsl:if test="count($newSteps | $oldSteps) = 0">
										<font>.</font>
									</xsl:if>
							</td>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="week">
							<td>
								<center>.</center>
							</td>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:template>


	<xsl:template name="setFontColorForStep">
		<xsl:param name="step"></xsl:param>
		<xsl:variable name="priority" select="$step/ancestor::item/@priority" />
		<xsl:choose>
			<xsl:when test="$priority = 'high'">
				<xsl:attribute name="color">red</xsl:attribute>
			</xsl:when>
			<xsl:when test="$priority = 'medium'">
				<xsl:attribute name="color">blue</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="color">black</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>