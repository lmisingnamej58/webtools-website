<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>

<h2>Meeting agendas and minutes</h2>
<ul>
<li>December 28, 2010 (no meeting, Holiday)</li>
<li>December 21, 2010 (no meeting, Holiday)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-12-14">December 14, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-12-07">December 07, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-11-30">November 30, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-11-23">November 23, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-11-16">November 16, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-11-09">November 09, 2010</a></li>
<li>November 2, 2010 -- no meeting</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-10-26">October 26, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-10-19">October 19, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-10-12">October 12, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-10-05">October 05, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-09-28">September 28, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-09-21">September 21, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-09-14">September 14, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-09-07">September 07, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-08-31">August 31, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-08-17">August 17, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-08-10">August 10, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-08-03">August 03, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-07-27">July 27, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-07-20">July 20, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-07-13">July 13, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-07-06">July 06, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-06-29">June 29, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-06-22">June 22, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-06-15">June 15, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-06-08">June 08, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-06-01">June 01, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-05-25">May 25, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-05-18">May 18, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-05-11">May 11, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-05-04">May 04, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-04-27">April 27, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-04-20">April 20, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-04-13">April 13, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-04-06">April 06, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-03-30">March 30, 2010</a></li>
<li>March 23, 2010 -- no meeting (EclipesCon)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-03-16">March 16, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-03-09">March 9, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-03-02">March 2, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-02-23">February 23, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-02-16">February 16, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-02-09">February 09, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-02-02">February 02, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-01-26">January 26, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-01-19">January 19, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-01-12">January 12, 2010</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2010-01-05">January 05, 2010</a></li>
</ul>
<hr />
<p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadXML($xmlString);
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$maincontent = $proc->transformToXML($xml);


$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
	$maincontent
	</div>
</div>
EOHTML;
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
	?>
