<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon()); # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>
<h2>Call Info</h2>

<p>Toll free (in US and Canada): 888-426-6840 <br />
Access code: 5019600# <br />
Caller pay alternative number: 215-861-6239<br />
<a
	href="https://www.teleconference.att.com/servlet/glbAccess?process=1&accessCode=6460142&accessNumber=2158616239">Full
list of global access numbers</a><br />
<br />
<p><a href="http://www.timeanddate.com/worldclock/custom.html?cities=224,207,1440,107&amp;hour=11&amp;min=0&amp;sec=0&amp;p1=207">Call
Time: 1500 UTC</a></p>

<h2>Meeting agendas and minutes</h2>
<ul compact="true">
<li>December 27, 2010 (no meeting, Holiday)</li>
<li>December 20, 2010 (no meeting, Holiday)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-12-13">December 13, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-12-06">December 6, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-11-29">November 29, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-11-15">November 15, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-11-08">November 8, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-11-01">November 1, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-10-25">October 25, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-10-18">October 18, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-10-11">October 11, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-10-04">October 4, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-09-27">September 27, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-09-20">September 20, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-09-13">September 13, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-09-06">September 6, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-08-30">August 30, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-08-23">August 23, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-08-16">August 16, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-08-02">August 02, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-07-26">July 26, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-06-21">June 21, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-06-07">June 07, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-05-31">May 31, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-05-24">May 24, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-05-17">May 17, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-05-10">May 10, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-05-03">May 03, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-04-26">April 26, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-04-19">April 19, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-04-12">April 12, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-04-05">April 05, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-03-29">March 29, 2011</a></li>
<li>March 22, 2011 -- no meeting (EclipseCon)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-03-15">March 15, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-03-08">March 08, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-03-01">March 01, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-02-22">February 22, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-02-15">February 15, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-02-08">February 08, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-02-01">February 01, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-01-25">January 25, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-01-18">January 18, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-01-11">January 11, 2011</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2011-01-04">January 04, 2011</a></li>

<li><a href="index_pmc_call_notes_2010.php">2010</a></li>
<li><a href="index_pmc_call_notes_2009.php">2009</a></li>
<li><a href="index_pmc_call_notes_2008.php">2008</a></li>
<li><a href="index_pmc_call_notes_2007.php">2007</a></li>
<li><a href="index_pmc_call_notes_2006.php">2006</a></li>
<li><a href="index_pmc_call_notes_2005.php">2005</a></li>
<li><a href="index_pmc_call_notes_2004.php">2004</a></li>
</ul>
<hr />
<!-- <p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
-->
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadHTML($xmlString);
// Load the XSL source
//$xsl = DOMDocument::load($root . '/webtools/wtpphoenix.xsl');
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');
// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

//echo "xml:";
//echo $xml->saveXML();
$maincontent = $proc->transformToXml($xml);
//echo "maincontent:";
//echo $maincontent;
$html = <<<EOHTML
<div id="maincontent">
$wtpTopButtons
$maincontent
</div>
EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
