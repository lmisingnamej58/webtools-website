<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>

<h2>Meeting agendas and minutes</h2>
<ul>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-12-18">December 18, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-12-11">December 11, 2007</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-12-04">December 04, 2007</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-11-27">November 27, 2007</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-11-20">November 20, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-11-13">November 13, 2007</a></li>
    <li>November 6, 2007 -- No meeting. EclipseWorld.</li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-10-30">October 30, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-10-23">October 23, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-10-16">October 16, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-10-09">October 9, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-10-02">October 2, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-09-25">September 25, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-09-18">September 18, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-09-11">September 11, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-09-04">September 04, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-08-28">August 28, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-08-21">August 21, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-08-14">August 14, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-08-07">August 7, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-07-31">July 31, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-07-24">July 24, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-07-17">July 17, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-07-10">July 10, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-06-26">June 26, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-06-19">June 19, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-06-12">June 12, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-06-05">June 5, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-05-29">May 29, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-05-22">May 22, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-05-15">May 15, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-05-08">May 8, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-05-01">May 1, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-04-24">April 24, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-04-17">April 17, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-04-10">April 10, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-04-03">April 3, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-03-27">March 27, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-03-20">March 20, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-03-13">March 13, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-03-08">March 8, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-02-27">February 27, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-02-20">February 20, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-02-13">February 13, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-02-06">February 6, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-01-30">January 30, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-01-23">January 23, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-01-16">January 16, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-01-09">January 09, 2007</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2007-01-02">January 02, 2007</a></li>
</ul>
<hr />
<p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadXML($xmlString);
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$maincontent = $proc->transformToXML($xml);


$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
	$maincontent
	</div>
</div>
EOHTML;
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
	?>
