<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../wtp.xsl"?>
<html>
<head>
<title>j2ee Standard Tools Project</title>
</head>
<body>


<h1>jst</h1>

<h2>Project Vision</h2>
<p>
The project vision is to extend eclipse platform with support 
for building multi-tier J2EE applications. The project will grow the community 
of eclipse users, and grow the community of developers that create Web applications 
based on the J2EE platform. In this way, we will help make eclipse the industry 
standard tool integration platform for development based on Open Standards 
and Technologies. The project must preserve the eclipse value proposition 
by providing integration, ease-of-use, function, and "coolness". Web artifacts 
must be first class citizens with respect to the capabilities that eclipse 
users expect. Servers must be first class execution environments, including 
both Open and Commercial implementations, therefore encourage support of 
eclipse by server vendors. 
</p>

<h3><a href="components.html">JST Components</a></h3>
<p>
The proposed components and component leads.
</p>

<h2>Project Context</h2>
<p>
The scope of the J2EE Standard Tools subproject is the support 
of J2EE programming. This includes the support of APIs covered by the <A CLASS="external" HREF="http://java.sun.com/j2ee" target="_blank">J2EE1.4 
specifications</A> (e.g. JSP, Servlets, EJBs, JCA, JMS, JNDI, JDBC, Java 
Web Services, JAX* and related <A CLASS="external" HREF="http://jcp.org/" target="_blank">JSRs</A>). 
Support for <A CLASS="external" HREF="http://jcp.org/" target="_blank">JCP 
specifications</A> commonly used in Web Applications, but not included in 
J2EE1.4 is to be studied on a case by case basis (ex: JSF,JDO).
</p>
<p> Support of frameworks not covered by the JCP (ex: Struts, Hibernate, 
XMLC) is outside the scope of this project, such projects could find a 
home in the <A CLASS="external" HREF="http://www.eclipse.org/technology" target="_top">Eclipse 
Technology</A> project. 
</p>
<p> JST will have annotation support <A CLASS="external" HREF="http://www.jcp.org/en/jsr/detail?id=175" target="_blank">(JSR 
175-Metadata)</A>, on top of those provided in the JDT, e.g. for code 
assist, where applicable. Annotation support will include <A CLASS="external" HREF="http://www.jcp.org/en/jsr/detail?id=181" target="_blank">JSR 
181-Metadata for Web Services</A>, and in the future will include support 
for other standardized metadata such as EJBs. In the transitional period 
until there are JSRs for J2EE annotations, JST will have some support 
for widely accepted open technologies such as <A CLASS="external" HREF="http://xdoclet.sourceforge.net/xdoclet/index.html" target="_blank">XDoclet</A>. 
</p>

<H4>Some Architectural Principles</H4>
<UL>
	<LI>Seamlessly extend the Eclipse user experience
	</LI>
	<LI>Accommodate common J2EE application project directory layouts
	</LI>
	<UL>
		<LI>Avoid need for migration
		</LI>
	</UL>
	<LI>Apply capabilities orthogonally to source artifacts:
	</LI>
	<UL>
		<LI>Syntax Coloring
		</LI>
		<LI>Code Assist
		</LI>
		<LI>Quick Fixes
		</LI>
		<LI>Refactoring
		</LI>
		<LI>Etc.
		</LI>
	</UL>
	<LI>Extend the Run command to J2EE artifacts, e.g. JSP
	</LI>
	<LI>Enable hybrid source editing:
	</LI>
	<UL>
		<LI>Java in JSP
		</LI>
		<LI>JavaScript in JSP
		</LI>
		<LI>EJB-QL in Java
		</LI>
		<LI>Etc.
		</LI>
	</UL>
	<LI>Enable extension of editors with value-add
	</LI>
	<UL>
		<LI>Graphical views
		</LI>
		<LI>Form based views
		</LI>
	</UL>
	<LI>Provide modular design of tools for enabling separate tool deliveries for differents classes of developers
	</LI>
	<UL>
		<LI>Enable separate deliveries of servlet/JSP and EJB tools
		</LI>
		<LI>Etc
		</LI>
	</UL>
</UL>

<H4>Some use cases</H4>
<UL>
	<LI>Develop a J2EE Web Application
	</LI>
	<UL>
		<LI>Develop JSP pages
		</LI>
		<LI>Develop tag libraries
		</LI>
		<LI>Develop Servlets and Servlet Filters
		</LI>
		<LI>Package the contents of war files
		</LI>
		<LI>Edit deployment descriptors
		</LI>
		<LI>Run, Test, Debug Web Applications
		</LI>
	</UL>
	<LI>Develop an EJB Application
	</LI>
	<UL>
		<LI>Add an EJB
		</LI>
		<LI>Add methods/fields/relationships/mappings to an EJB
		</LI>
		<LI>Edit the deployment descriptors
		</LI>
		<LI>Package the contents of an EJB-JAR
		</LI>
		<UL>
			<LI>Generate necessary classes
			</LI>
			<LI>Run, Test, Debug EJBs
			</LI>
		</UL>
	</UL>
	<LI>Develop an Enterprise Application
	</LI>
	<UL>
		<LI>Assemble an enterprise application
		</LI>
			<UL>
				<LI>Add, remove Web Applications, EJBs
				</LI>
				<LI>Add, remove client applications
				</LI>
			</UL>
		<LI>Package the contents of an ear
		</LI>
		<LI>Edit the deployment descriptors
		</LI>
		<LI>Run, Test, Debug an Enterprise Application
		</LI>
	</UL>
	<LI>Import existing Web applications, EJBs, and ears into a project
	</LI>
	<UL>
		<LI>Enhance the "PetStore" blue-print application
		</LI>
	</UL>
	<LI>Develop Java Web Services
	</LI>
	<UL>
		<LI>Develop a Web Service based on Session Beans
		</LI>
		<LI>Develop a Web Service based on JavaBeans
		</LI>
		<LI>Create Web Service Clients
		</LI>
	</UL>
	<LI>Provide Exemplar Server Tools for open-technology server runtimes 
	  such as <A CLASS="external" HREF="http://jakarta.apache.org/tomcat/index.html" target="_blank">Apache 
	  Tomcat</A>, <A CLASS="external" HREF="http://jonas.objectweb.org/" target="_blank">JOnAS</A>, 
	  <A CLASS="external" HREF="http://www.jboss.org/" target="_blank">JBoss</A>, 
	  <A CLASS="external" HREF="http://java.sun.com/j2ee/1.4/download-sdk.html" target="_blank">J2EE 
	  1.4 SDK</A> and others.
	 </LI>
</UL>

<h2>Goals for the J2EE Core Model and Tools</h2>
These structures will provide common facilities required by most J2EE tool builders, as well as exemplary tools for J2EE developers. The project will leverage the Web Core Model of the WST, to build J2EE specific models (for project, editors, artifacts, components, servers) providing extensible framework and APIs. Our goals are:
<UL>
<LI>Divide and Conquer: Lots of components, so divide the work using the classical principle of hierarchy
</LI>
<LI>Layer Build Dependencies: J2EE Standards builds on the Web Standards, the J2EE Extensions build on the J2EE
</LI>
<LI>The structure of software reflects the structure of the organization that built it: We are building a platform and need to validate our API.
</LI>
</UL>



<h2>J2EE Core Model</h2>
<p>
This is a set of frameworks, and models, which provide the necessary abstractions and APIs to work and develop with J2EE Tools, and to build IDEs and tools to support these activities.  JCM is not just an adaptation of the J2EE specifications, but it will focus on the additional requirements of developers and tool builders. JCM is naturally related to the models and tools defined in
<a CLASS="wikipage" href="../wst/main.html">the Web Standard Tools subproject</a>, and will address at a minimum the following areas:
</p>
<h4>J2EE Project Model</h4>
<p>The J2EE Project model extends Web Project Model to support the development of J2EE applications in a variety of ways; projects divided across multiple related projects, or modules factored into one project that can host multiple artifacts. This includes support for:
</p>

<UL>
<LI>A wide variety of project layouts
</LI>
<UL>
<LI>J2EE Development divided across multiple related projects
</LI>
<UL>
<LI>Structural - EAR, EJBs, WARs
</LI>
<LI>Functional – (e.g. CRM, ERP...)
</LI>
</UL>
<LI>J2EE Development factored into one project with multiple J2EE artifacts. 
</LI>
<UL>
<LI>Integrated views
</LI>
<LI>Sophisticated module management
</LI>
<LI>Project management tools
</LI>
</UL>
</UL>
<LI>Management of build dependencies and activities
</LI>
<LI>Server class-loader models
</LI>
<UL>
<LI>Resolution of names and types across multiple projects
</LI>
</UL>
</UL>

<h4>J2EE Editor Model</h4>
<p>The Editor Model will offer developers a set of APIs to access J2EE models, participate in build and refactoring activities, and allow them to easily create various text, and graphical editors for J2EE.
</p>

These editors will:
<UL>
<LI>Extend the eclipse user experience
</LI>
<LI>Accommodate existing common project directory layouts
</LI>
<LI>Be modular and extensible
</LI>
<UL>
<LI>Graphical views
</LI>
</UL>
<LI>Allows hybrid source editing
</LI>
<UL>
<LI>Java, HTML, XML in JSP, SQL in Java or JSP, etc.
</LI>
</UL>
<LI>Will provide access to JCM models
</LI>
<LI>Will be participate in refactoring and builds
</LI>
<LI>Provide content based selection
</LI>
<LI>Apply editors capabilities orthogonally to source artifacts such as JSPs
</LI>
<UL>
<LI>Syntax Coloring
</LI>
<LI>Code Assist
</LI>
<LI>Quick Fixes
</LI>
<LI>Refactoring
</LI>
<LI>Etc.
</LI>
</UL>
</UL>
<H4>J2EE Artifacts and Components</H4>
<p>These models will represent J2EE standard modules, files, deployment descriptors and various other artifacts, associated natures, builders, validators and EMF models. They will at minimum include models for:
</p>
<UL>
<LI>JSP
</LI>
<LI>Servlet
</LI>
<LI>ServletFilter
</LI>
<LI>EJBs
</LI>
<LI>JCAs
</LI>
<LI>Java Web Services 
</LI>
<LI>Modules, 
</LI>
<UL>
<LI>Deployment descriptors, web.xml, ejb-jar.xml, application.xml, etc. 
</LI>
<LI>Web Modules(war),  
</LI>
<LI>EJB Modules (ejb jars), 
</LI>
<LI>Applications (ear) , 
</LI>
<LI>Resource Archives (rar).
</LI>
</UL>
</UL>
<H4>J2EE Server Models</H4>
<p>J2EE Application Servers are multi-tier distributed component-based products, typically implemented in Java, that reside in the middle-tier of a server centric architecture. They provide middleware services for security and state maintenance, along with data access and persistence. This model generally includes a Client Tier, a Middle Tier, and an EIS Tier. The Client Tier can be one or more applications or browsers. The J2EE Platform is in the Middle Tier and consists of a Web Server and an EJB Server (These servers are also called &quot;containers&quot;). There can be additional sub-tiers in the Middle Tier. The Enterprise Information System (EIS) tier includes the existing applications, files, and databases.<I><BR /> <BR />Although based on the same standard, there are significant variations on how available servers support J2EE components, the way they are administered, started, stopped and how J2EE modules are managed, and how they support development time activities such as debugging and hot deployment and automatic refreshes.</I><BR /> <BR />The server model must at a minimum define abstract facilities and configurations to:
</p>
<UL>
<LI>Support any J2EE Compliant Servers
</LI>
<UL>
<LI>LCD is J2EE, Versions, Avoid Vendor Locking
</LI>
</UL>
<LI>Implement eclipse launching APIs
</LI>
<UL>
<LI>In process/project, local, remote runtimes 
</LI>
</UL>
<LI>Manage environmental and JVM parameters, classpath configurations
</LI>
<LI>Package, deploy, remove, import, export J2EE modules 
</LI>
<LI>Provide an extensible framework for server administration 
</LI>
<LI>Provide a meta-model for easy adaptation of new server types
</LI>
<UL>
<LI>extensions for exceptions (Tomcat)
</LI>
</UL>
<LI>Support for related JSRs
</LI>
<UL>
<LI>JSR 45:  Debugging Support
</LI>
<LI>JSR 77: J2EE Management
</LI>
<LI>JSR 88: J2EE Application Deployment
</LI>
</UL>
</UL>
<h2>J2EE Standard Tools</h2>
<p>
J2EE Standard Tools will provide extensible plug-ins for the development of J2EE Applications and support development, testing and debugging with a wide range of J2EE servers. Exemplar tools will support the development of Web Applications (.war), Enterprise JavaBeans, Java Web Services, and support development, testing and debugging with standard compliant J2EE application servers ('specific additional extensions may be required').
</p>
<p>
The interaction of JCM and the exemplar tools based on JCM will provide a healthy environment where requirements and APIs are based on actual needs.  Tools will verify the APIs.  This is a critical feature of the project in that JCM without tools, or tools without JCM, will not achieve our goal to provide a platform for high quality web development based on J2EE. 
</p>
<I>All JST tools will be based on JCM.</I>
<p>The exemplar tools for J2EE will include:</p>
<UL>
<LI>Server Tooling
</LI>
<UL>
<LI>More than starting a VM 
</LI>
<LI>Complex environmental setup and classloader models
</LI>
<LI>In process, local and remote development scenarios
</LI>
<LI>Must support all J2EE compliant servers
</LI>
<UL>
<LI>Exceptions managed with extensions
</LI>
</UL>
<LI>Launchers - Run, Debug, Profile, Stop and Restart...
</LI>
<LI>Configuration wizards and editors, starting, debugging, profiling, stopping and restarting applications servers
</LI>
</UL>
<LI>Tools for J2EE Modules
</LI>
<UL>
<LI>WAR, EJB-JAR, RAR, EAR
</LI>
<UL>
<LI>Web Apps, EJB, Resource Archives, Enterprise applications
</LI>
</UL>
<LI>Create modules
</LI>
<UL>
<LI>Wizards - Imports
</LI>
</UL>
<LI>Build modules
</LI>
<LI>Validate modules
</LI>
<LI>Edit modules
</LI>
<UL>
<LI>Deployment descriptors
</LI>
<LI>Package contents
</LI>
</UL>
<LI>Aggregate modules (ears)
</LI>
<LI>Server deployment support
</LI>
</UL>
<LI>Editors for J2EE Components
</LI>
<UL>
<LI>JSP
</LI>
<UL>
<LI>Standard editor capabilities (coloring, code assist, refactoring, formatting)
</LI>
<LI>JSR-45 Debugging
</LI>
<LI>Content based selection
</LI>
<LI>Hybrid editing
</LI>
<LI>Build Participation
</LI>
</UL>
<LI>EJB Tools
</LI>
<UL>
<LI>Annotation based editing and code generation
</LI>
<LI>Wizards
</LI>
<LI>Validation
</LI>
</UL>
          <LI>Java Web Services and support for <A CLASS="external" HREF="http://www.jcp.org/en/jsr/detail?id=109" target="_blank">JSR-109 
            Implementing Enterprise Web Services</A> </LI>
<UL>
<LI>Axis Adapter
</LI>
</UL>
</UL>
<LI>Tools for Navigation
</LI>
<UL>
<LI>J2EE Perspective(s)
</LI>
<LI>Module Views
</LI>
<LI>J2EE Structures
</LI>
<LI>Servers
</LI>
</UL>
<LI>Tools to Improve the J2EE Development Process
</LI>
<UL>
<LI>Testing Tools
</LI>
<UL>
<LI>Unit and Integration Testing
</LI>
</UL>
<LI>Performance Tools
</LI>
<UL>
<LI>Profiling </LI>
<LI>Load and Stress Testing </LI>
</UL>
</UL>
</UL>


</body>
</html>