The resource-env-ref element contains a declaration of an application client's reference to an administered object associated with a resource in the application client's environment.  It consists of an optional
description, the resource environment reference name, and an indication of the resource environment reference type expected by the application client code.

Used in: application-client

Example:

<resource-env-ref>
    <resource-env-ref-name>jms/StockQueue</resource-env-ref-name>
    <resource-env-ref-type>javax.jms.Queue</resource-env-ref-type>
</resource-env-ref>

