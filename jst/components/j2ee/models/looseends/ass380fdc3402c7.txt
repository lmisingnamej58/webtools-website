The ejb-class element contains the fully-qualified name of the enterprise bean's class.  Example: <ejb-class>com.wombat.empl.EmployeeServiceBean</ejb-class>
