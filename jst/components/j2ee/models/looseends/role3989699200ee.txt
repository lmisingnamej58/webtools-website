The value of the type element describes the Java type of the attributes value.
For static values (those determined at translation time) the type is always java.lang.String.
