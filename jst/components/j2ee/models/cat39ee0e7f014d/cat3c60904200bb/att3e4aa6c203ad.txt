The element managedconnectionfactory-class specifies
        the fully qualified name of the Java class that
        implements the
        javax.resource.spi.ManagedConnectionFactory interface.
        This Java class is provided as part of resource
        adapter's implementation of connector architecture
        specified contracts. The implementation of this
        class is required to be a JavaBean.

        Example:
        <managedconnectionfactory-class>
            com.wombat.ManagedConnectionFactoryImpl
        </managedconnectionfactory-class>
