The connection-impl-classType specifies the fully
        qualified name of the Connection class that
        implements resource adapter specific Connection
        interface.  It is used by the connection-impl-class
        elements.

        Example:

            <connection-impl-class>com.wombat.ConnectionImpl
            </connection-impl-class>
