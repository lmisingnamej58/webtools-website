The jaxrpc-mapping-file element contains the name of a file that
        describes the JAX-RPC mapping between the Java interaces used by
        the application and the WSDL description in the wsdl-file.  The
        file name is a relative path within the module file.
