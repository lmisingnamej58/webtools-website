The subscription-durability element specifies whether a JMS topic subscription is intended to be durable or nondurable.

The subscription-durability element must be one of the two following:  Durable, NonDurable

