Defines a name for a role that is unique within an ejb-relation. Different relationships can use the same name for a role.

