@since J2EE1.4
The version specifies the version of the
    EJB specification that the instance document must
    comply with. This information enables deployment tools
    to validate a particular EJB Deployment
    Descriptor with respect to a specific version of the EJB
    schema.
