The destination-type element specifies the type of the JMS destination. The type is specified by the Java interface expected to be implemented by the destination.

The destination-type element must be one of the two following: javax.jms.Queue, javax.jms.Topic
