The session-beanType declares an session bean. The
declaration consists of:

    - an optional description
    - an optional display name
    - an optional icon element that contains a small and a large
      icon file name
    - a name assigned to the enterprise bean
      in the deployment description
    - the names of the session bean's remote home and
      remote interfaces, if any
    - the names of the session bean's local home and
      local interfaces, if any
    - the name of the session bean's web service endpoint
      interface, if any
    - the session bean's implementation class
    - the session bean's state management type
    - the session bean's transaction management type
    - an optional declaration of the bean's
      environment entries
    - an optional declaration of the bean's EJB references
    - an optional declaration of the bean's local
      EJB references
    - an optional declaration of the bean's web
      service references
    - an optional declaration of the security role
      references
    - an optional declaration of the security identity
      to be used for the execution of the bean's methods
    - an optional declaration of the bean's resource
      manager connection factory references
    - an optional declaration of the bean's resource
      environment references.
    - an optional declaration of the bean's message
      destination references

The elements that are optional are "optional" in the sense
that they are omitted when if lists represented by them are
empty.

Either both the local-home and the local elements or both
the home and the remote elements must be specified for the
session bean.

The service-endpoint element may only be specified if the
bean is a stateless session bean.
