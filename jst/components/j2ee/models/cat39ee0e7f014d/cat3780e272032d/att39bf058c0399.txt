Contains the EJB QL query string that defines a finder or select query. This element is defined within the scope of a query element whose contents specify the finder or the select method that uses the query. The content must be a valid EJB QL query string for the entity bean for which the query is specified. The ejb-ql element must be specified for all queries that are expressible in EJB QL.

@invariant The value must be a valid EJB QL query string for the entity bean or dependent object class for which the query is specified.
@invariant The ejb-ql element must be specified for all queries that are expressible in EJB QL.

Example:
<query>
<query-method>
<method-name>ejbSelectPendingLineitems</method-name>
<method-params/>
</query-method>
<ejb-ql>SELECT ENTITY(l)
FROM LineItems l
WHERE l.shipped is FALSE
</ejb-ql>
</query>
