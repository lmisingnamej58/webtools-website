<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../.." />
		<title>Integrating existing maven project in Eclipse WTP.</title>
	</head>
	
	<body>
		<h1>jst j2ee</h1>
		<h2>Intoduction</h2>
		<p>This article walkthroughs the steps required to integrate an existing maven project structure 
		in the eclipse WTP platform. This integration uses the  flexibile project structure introduced in 
		the WTP M3 release.
		</p>
		<br/>
		<h2>Pre-requisites</h2>
		<p>
		Maven should be installed and  configured, if not it can be downloaded from
		<a href = "http://maven.apache.org/"> http://maven.apache.org/</a>.<br/>This article also assumes 
		that you are  familiar with maven project artifacts and the maven eclipse plugin.
		</p>
		<br/>		
		<h2>A Sample Maven Project : WebSample</h2>
		<p>
		
				Unzip <A href="resource/MavenWebSample.zip" target="_blank">MavenWebSample.zip</A>.
				
		Our MavenWebSample  has  two projects, a web project  and a utility project.
		The web project "csrweb" contains a HelloWorldServlet and a web resource
		index.html.<br/>  The utility project "bedrock" has a class Util which is used by
		HelloWorldServlet. <br/><br/>
		Please build the utility project by going to  command prompt and  typing maven jar:install
		and the  web project by typing maven war:install
		<br/><br/>
		Here is the eclipse enabled project   <A href="resource/WebSample.zip" target="_blank">WebSample.zip</A>.
		
		</p>
		
		<br/>
		
		<h2>Configuring the Web Project</h2>
		
			<ol>
			
				<li>
				<p>
				Add  the  follwing in  the <b>project.xml</b> of the csrweb to set the  bedrock  as the dependent project of the csrweb,
				since  csrweb uses bedrock project, setting this dependency in the eclipse will make sure proper 
				project build order.
				</p>
				
    <pre>
   &lt;dependency&gt;
      &lt;groupId&gt;WebSample&lt;/groupId&gt;
      &lt;artifactId&gt;bedrock&lt;/artifactId&gt;
      &lt;version&gt;1.0&lt;/version&gt;
      &lt;properties&gt;
        &lt;war.bundle&gt;true&lt;/war.bundle&gt;
        <b>&lt;eclipse.dependency>true&lt;/eclipse.dependency&gt;</b>
      &lt;/properties&gt;
    &lt;/dependency&gt;
   </pre>
				
				</li>
				<li>Create a file <b>project.properties</b> under the csrweb folder, the folder where you
				will find project.xml. Now  add  the following in  this file.
<pre>				
maven.eclipse.projectnatures=org.eclipse.jst.j2ee.web.WebNature,org.eclipse.jdt.core.javanature,org.eclipse.wst.common.modulecore.ModuleCoreNature
maven.eclipse.buildcommands=org.eclipse.wst.common.modulecore.DeployableModuleBuilder,org.eclipse.jdt.core.javabuilder,org.eclipse.jst.j2ee.web.LibDirBuilder,org.eclipse.wst.validation.validationbuilder,org.eclipse.jst.j2ee.LibCopyBuilder,org.eclipse.wst.common.modulecore.LocalDependencyResolver
</pre>

			
				
				</li>
				
				<li>
				Now open the command prompt, go to the csrweb folder and execute "maven eclipse".
				You will see  that  two  files are  generated <b> .project</b> and <b> .classpath </b>
				<br/>
				<br/>
				
				<p>
				Open the .project  and  remove the first buildcommand  javabuilder and the first  nature 
				org.eclipse.jdt.core.javanature.<br/>  This is done  to ensure  that only one 
				entry of javabuilder and org.eclipse.jdt.core.javanature exists. <br/>The correct order of builder and natures 
				are  needed  to  ensure proper  functionality.The final .project should look like  this.
				</p>
				<img src="images/csrwebproject.JPG"/>
				
				</li>
				
				<br/>
				<br/>
				<li>
				Create a  <b>.j2ee</b>  file in the  csrweb  project
				
<pre>								
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;j2eesettings version="600"&gt;
&lt;moduleversion>23&lt;/moduleversion&gt;
&lt;webcontent>src/webapp&lt;/webcontent&gt;
&lt;context-root>csrweb&lt;/context-root&gt;
&lt;/j2eesettings&gt;
</pre>
			The  moduleversion denotes the servlet version, for e.g. enter 22 for Servlet version 2.2, 23 for 2.3 and 24 for 2.4,<br/>
			this should match the version specified in the project.xml, in the dependency section of the servletapi.
				
				</li>
				
				<br/>
				<br/>
				
				<li>
				Now create a  <b>.wtpmodule</b>  file in the csrweb project and put the following content.
				The xsd of  the .wtpmodule is defined elsewere in the WTP.
				
				
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;project-modules id="moduleCoreId"&gt;
    &lt;wb-module deploy-name="csrweb.war"&gt;
        &lt;wb-module module-type-id="jst.web"/&gt;
        &lt;wb-resource deploy-path="/" source-path="/csrweb/src/webapp"/&gt;
        &lt;wb-resource deploy-path="/WEB-INF/classes" source-path="/csrweb/src/main/java"/&gt;

                
        &lt;dependent-module deploy-path="WEB-INF/lib" handle="module:/resource/csrweb/Library.jar"&gt;
            &lt;dependency-type&gt;uses&lt;/dependency-type&gt;
        &lt;/dependent-module&gt;
                
        &lt;dependent-module deploy-path="/WEB-INF/lib" handle="module:/resource/bedrock/bedrock.jar" &gt;
            &lt;dependency-type>uses&lt;/dependency-type&gt;
        &lt;/dependent-module&gt;
    &lt;/wb-module&gt;
    &lt;wb-module deploy-name="Library.jar"&gt;
        &lt;wb-module module-type-id="jst.utility"/&gt;
        &lt;wb-resource deploy-path="/" source-path="/csrweb/LibraryContents"/&gt;
    &lt;/wb-module&gt;
&lt;/project-modules&gt;

</pre>				

	<p>
	For quick overview the  line  <b>&lt;wb-resource deploy-path="/" source-path="/csrweb/src/webapp"/&gt;  </b>implies that
	all the contents of source-path  will be placed <br/>in the  csrweb/.deployables folder after  the build. <br/>
	Similarly the  line  <b>&lt;wb-resource deploy-path="/WEB-INF/classes" source-path="/csrweb/src/main/java"/&gt;</b> <br/>implies that
	after the  build  of the java files under  the source-path, all the .class will be in  the  csrweb/.deployables/WEB-INF/classes
	</p>
				</li>
				
		<br/>
		<li>
			Note that you will have to create a  folder META-INF and MANIFEST.MF  manually and 
			add the necessary jars as per your requirement, eclipse WTP <br/>as of now doesn't generate 
			this file. You will also need to put this file as the wb-resource  in the .wtpmodule file as
			described above.<br/>
			
			The csrweb has the MANIFEST.MF in the folder csrweb/src/webapp/META-INF
		</li>		
			
		</ol>	
				
		
		
				
		<h2>Configuring the Util project</h2>	
		
		<p>	
			<ol>	
				<li>
Create  <b>project.properties </b>  in the bedrock folder  

<pre>
maven.eclipse.projectnatures=org.eclipse.wst.common.modulecore.ModuleCoreNature
maven.eclipse.buildcommands=org.eclipse.wst.common.modulecore.DeployableModuleBuilder,org.eclipse.jdt.core.javabuilder,org.eclipse.wst.common.modulecore.LocalDependencyResolver
</pre>
				</li>
				
				<li>
				Now open the command prompt, go to the bedrock folder and execute "maven eclipse".
				You will see  that  two  files are  generated <b> .project</b> and <b> .classpath </b>	
				<br/>
				<br/>
				
				<p>
				Open the .project  and  remove the first buildcommand  javabuilder.This is done  to ensure  that only one 
				entry of javabuilder exists. <br/>The correct order of builder and natures 
				are  needed  to  ensure proper  functionality.The final .project should look like  this.
				</p>
				<img src="images/utilityproject.JPG"/>							
				</li>
				
				<br/>
				<br/>
				<li>
				Now create a  <b>.wtpmodule</b>  file in the bedrock project and put the following content.
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;project-modules id="moduleCoreId"&gt;
    &lt;wb-module deploy-name="bedrock.jar"&gt;
        &lt;wb-module module-type-id="jst.utility"/&gt;
        &lt;wb-resource deploy-path="/" source-path="/bedrock/src/java"/&gt;
    &lt;/wb-module&gt;

&lt;/project-modules&gt;

</pre>				
			
				</li>
			</ol>	
		</p>


<h2>Bringing the project in the eclipse</h2>

<p>
<ol>
   <li>
   <p>
   From the  Window->Preferences  set the classpath variable MAVEN_REPO  to point to the 
   repository of  the maven.
   </p>
   <img src="images/DefiningMavenClassPath.JPG"/>	
   </li>
   <br/>
   <br/>
   
   <li>
   <p>
   Import the projects using File->Import->Existing Project into workspace.
   </p>
   <img src="images/ImportProjectInEclipse.JPG"/>

   </li>
   <br/>
   <br/>
   <li>
   <p>
   Go to the csrweb, project Properties->Java Build Path->Source and add select Add Folder, point  to the csrweb/LibraryContents<br/>
   </p>


   </li>
   
      
</ol>

</p>

<h2>Running the Servlet in the Tomcat Server</h2>
	<ol>
		<li>
		Open the J2EE perspective.
		</li>
		<br/>
		<br/>		
		<li>
		<p>
		From the  Window->Preferences, setup the Tomcat Server runtime
		</p>
		<img src="images/SetTomcatRuntime.JPG"/>
		</li>
		
		<br/>
		<br/>
		
		<li>
		Set up the server from  the Servers view in the J2EE perspective
		</li>
		
		<br/>
		<br/>
		
		<li>
		<p>
		Set up the server in the web project
		</p>
		<img src="images/SettingServerInWebProject.JPG"/>
		</li>	
			
		<br/>
		<br/>
		
		<li>
		Build the projects.
		</li>
		<br/>
		<br/>
		<li>
		Run the web project csrweb by right clicking and choosing Run As->Run on Server
		</li>
				
	</ol>


	</body>
</html>		