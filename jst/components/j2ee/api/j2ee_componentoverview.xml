<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../../webtools/wtp.xsl"?>
<html>
	<head>
		<meta
			name="root"
			content="../../../../.." />
		<title>overview</title>
	</head>

	<body>
		<h1>Java EE project</h1>
		<h2>Functional Overview</h2>
		<p>
			This section provides a functional overview of the Java EE subproject in WTP.

			<h4>Java EE Project Creation</h4>
			Project creation wizards, operations and the data models that drive the
			operations are used to create Java EE projects. The project creation wizards
			can be launched using the File > New > Project menu action. The Java EE
			projects include Enterprise Application, Web, Application Client,
			Enterprise Java Bean and Connector projects. These Java EE projects can be
			created for Java EE specification level 1.2, 1.3, 1.4 Java EE 5 and 6 and targeting to a
			Java EE specification compatible application server. A Java EE runtime target
			(target server) should be predefined for project creation. The runtime
			targets are a mechanism to set the JRE and Server classpath on a Java EE
			project for compile time. The Java EE module projects Web, Application
			Client, Enterprise JavaBean and Connector projects can be created as
			standalone or can be nested under a new or existing Enterprise
			Application project. The api that are available to create the Java EE
			projects are discussed in the Api Overview section.

			<h4>Java EE Archive Import</h4>
			Import wizards and operations and data models that drive the operations
			are used to import Java EE archives (ear, war,jar.rar) into the workbench.
			The import wizards can launched using the File > Import menu action. The
			Java EE archives can be imported into new or existing Java EE projects. For new
			projects, a runtime target (target server) must be predefined. When
			importing into an existing project, the user has the option to overwrite
			the contents of the project with that of the archive and also delete the
			project and create and new one. The Enterprise Application wizard has
			more options to import modules nested in a EAR file. The api that are
			available to import Java EE archives are discussed in the Api Overview
			section.

			<h4>Java EE Module Export</h4>
			Export wizards , operations and data models that drive the operations are
			used to export Java EE projects into deployable archives (ear, war, jar,
			rar). The export wizards can be launched using File > Export menu action.
			The export wizards have the option to include Java source also using
			export of the Java EE projects. The export of an Enterprise Application
			project also exports all the associated modules into the ear that can be
			deployed to a Java EE application server.

			<h4>Java EE Project Explorer contributions</h4>
			The Project Explorer provides a single, consistent view that allows users
			to explore rich levels of content. Physical and logical content is
			integrated seamlessly to allow users to browse Eclipse resources, Java
			packages and classes, navigable models of configuration information,
			Databases, Database Servers and Web Services among other enhanced
			content. Java EE deployment descriptor models are contributed for fast 
			visual inspection of a modules content, and several menu actions are available
			for related activities (Run on server, open editor etc...)
			
			<h4>Java EE Server Deployment</h4>
			Built on WTP's' common project's "Deployment Assembly" framework, Each Java EE
			project has the flexibility to assemble source from various locations, and map
			folders, libraries, and classpath entries into a given deployment directive
			
			<h4>Java EE Validation</h4>
			Underlying in memory models are used to validate relevant artifacts, the various
			validator's are built on WTP's validation framework. Utilizing JDT(Java) api's and 
			deployment descriptor model's - Specification rules are checked to catch problems
			at development time, and to ensure deployment to various runtimes goes smoothly.
		</p>


		<h2>Subproject and plugin dependencies</h2>
		<p>
			The Java EE project is comprised of
			<i>jst.common</i>
			,
			<i>jst.j2ee</i>
			,
			<i>jst.servlet</i>
			,
			<i>jst.ejb</i>
			, and
			<i>jst.web</i>
			subprojects. The following diagram visualizes the dependencies among
			these components, and the relevant WST subcomponent dependencies.
		</p>
		<table
			cellspacing="10"
			cellpadding="10">
			<tr>
				<td>
					<p>
						<img src="diagrams/j2ee_subcomponent_dependencies.jpg" />
					</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>
						<i>
							Figure 1: Component dependencies relevant to the Java EE
							project
						</i>
					</p>
				</td>
			</tr>
		</table>
		<p>
			Within the Java EE project, the
			<i>org.eclipse.jst.j2ee.core</i>
			plugin defines the EMF metamodels that are used to work with Java EE
			artifact deployment descriptors. Each Java EE plugin builds on top of these.
			Each logical type of artifact has one plugin each for non-UI and UI
			functionality. The non-UI component generally contains Operations and
			Edit Models among other things. The UI component generally contains
			Wizards, Wizard Pages, and possibly Editor or Editor Pages. Two plugins
			are outside of this pattern:
			<i>org.eclipse.jst.j2ee.migration.ui</i>
			and
			<i>org.eclipse.jst.j2ee.navigator.ui</i>
			which provide functionality that crosses all module types. Future
			iterations on the design could result in breaking these into separate,
			module-type specific contributions.
		</p>
		<table
			cellspacing="10"
			cellpadding="10">
			<tr>
				<td>
					<p>
						<img src="diagrams/j2ee_plugin_dependencies.jpg" />
					</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>
						<i>
							Figure 2: Plugin dependencies within the Java EE project
						</i>
					</p>
				</td>
			</tr>
		</table>
	</body>
</html>
