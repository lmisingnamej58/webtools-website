<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../../../../" />
		<title>Creating Bottom Up Web Service</title>
	</head>
	<body>
		<h1>Creating Bottom Up Web Service</h1>
		<p>
			<b>By Kathy Chan</b>
			<br />
			December 22, 2005
			<br />
			<br />
		</p>

		<h2>Introduction</h2>
		<br></br>
		<p>
			This document is one of a series of tutorials to
			demonstrate the use of the Web Services tools in the Web
			Tools Platform Project using the <b>WTP 1.0 drivers</b>.
		</p>
		<p>
			Refer to <a href="../../../1.5/tutorials/index.html">WTP 1.5 Web services tutorials</a>
			for a new set of Web services tools tutorials for the WTP 1.5 driver.
		</p>
		<p>
			This tutorial shows how to create a simple Web service and
			Web service client from a Java class. The Java class in this
			scenario converts between the Celsius and Farenheit
			temperature scales.
		</p>
		<br></br>
		
		<h2>Set Up</h2>
		<br></br>
		<p>
			Before creating the Web service, there are two
			prerequisites:
		</p>
		<ol>
			<li>
				<a href="../InstallTomcat/InstallTomcat.html">Install Apache Tomcat</a>
			</li>

			<li>
				<a href="../CreateWebProject/CreateWebProject.html">Create a dynamic Web project</a>
				called
				<tt>ConverterProj</tt>
				.
			</li>
		</ol>


		<h2>
			Creating a bottom up Java bean Web service and Web service
			client
		</h2>
		<br></br>
		<ol>
			<li>
				Import the
				<a
					href="srcfiles/wtp/Converter.java">
					wtp/Converter.java
				</a>
				class into ConverterProj/src (be sure to preserve the
				package).
			</li>
			<li>
				Select the
				<b>Converter.java</b>
				file.
			</li>
			<li>
				Open <b>File -&gt; New -&gt; Other... -&gt; Web Services
				-&gt; Web Service</b>.
			</li>
			<li>
				Click <b>Next</b>.
			</li>
			<li>
				Select
				<b>Generate a proxy</b>
				.
			</li>
			<li>
				Select
				<b>Test the Web service</b>
				.
			</li>
			<li>
				Select
				<b>Monitor the Web service</b>
				.
			</li>
			<li>
				Select
				<b>Overwrite files without warning</b>
				.
			</li>
			<li>
				Result:
			</li>

			<br></br>
			<img border="0"
				src="images/bu1.jpg" />
			<br/>
			
			<li>
				Click
				<b>Next</b>
				. Result:
			</li>

			<br></br>
			<img border="0"
				src="images/bu2.jpg" />
            <br/>

			<li>
				Click
				<b>Next</b>
				to get to the
				<b>Service Deployment Configuration Page</b>
				. Result:
			</li>

			<br></br>
			<img border="0"
				src="images/bu3.jpg" />
		    <br/>
		    
			<li>
				If you want to choose a server different from the one
				defaulted by the wizard, click the
				<b>Edit</b>
				button to:
				<a href="../SelectServer/SelectServer.html">
					select a server
				</a>
				.
			</li>
			<li>
				Click
				<b>Finish</b>
				.
			</li>
			<li>
				It will take about one minute for the wizard to assemble
				the Web service and Web service client Web projects,
				start Apache Tomcat, and deploy the projects to Tomcat.
				Once finished, the generated Sample JSP Web application
				will appear in the browser view, maximized here for
				clarity:
			</li>

			<br></br>
			<img border="0"
				src="images/bu4.jpg" />
			<br/>
			
			<li>
				Under
				<b>Methods</b>
				, click on
				<b>celsiusToFarenheit(float)</b>
				.
			</li>
			<li>
				Under
				<b>Inputs</b>
				, enter
				<tt>37</tt>
				into the
				<b>celsius</b>
				entry field.
			</li>
			<li>
				Click on
				<b>Invoke</b>. 
				In the <b>Result</b> view, you should get a response of
				<b>98.6</b>.
			</li>

			<br></br>
			<img border="0"
				src="images/bu5.jpg" />
			<br/>
			
			<li>
				<p>
					Since you selected the "Monitor Web service" checkbox, 
					a TCP/IP port is automatically created on the
					Tomcat server. When you invoke the Web service,
					the <b>TCP/IP Monitor</b> view comes up automatically showing
					the SOAP message request and response. Double-click
					on the <b>TCP/IP Monitor</b> view and select XML view in the drop-down
					combo for both request and response. Result:
				</p>
			</li>

			<br></br>
			<img border="0"
				src="images/bu6.jpg" />
 			<br/>
 			
		</ol>
		<h2>Explanation</h2>
		<p>
			The Web Service wizard is orchestrating the end-to-end
			generation, assembly, deployment, installation and execution
			of the Web service, Web service client, and sample JSPs. In
			this scenario, we clicked Finish on page one. In effect, this
			is allowing the wizard to pick reasonable defaults according
			to the basic high-level choices made on the first page. If
			you repeat this scenario, but use the Next button to work
			through the wizard, you will learn more about the kinds of
			choices that are available and the kinds of defaults being
			assumed.
		</p>
		<p>
			After completing this scenario, the WSDL for the Converter
			Web service can be found in
			<tt>ConverterProj/WebContent/wsdl/Converter.wsdl</tt>.
		</p>
		<p>
			Now that your Web service is running, there are a few
			interesting things you can do with this WSDL file. Examples:
		</p>
		<ol>
			<li>
				You can double-click on the WSDL to open the WSDL
				graphical editor.
			</li>

			<li>
				You can right-click on the WSDL and choose
				<b>
					Web Services -&gt; Test with Web Services Explorer
				</b>
				to test the service.
			</li>

			<li>
				You can right-click on the WSDL and choose
				<b>Web Services -&gt; Publish WSDL file</b>
				to publish the service to a public UDDI registry.
			</li>

			<li>
				You can click on the WSDL and choose
				<b>
					New -&gt; Other... -&gt; Web Services -&gt;
					Web Service Client
				</b>
				to generate a Web service client. Note, however, that we
				have already witnessed the internal and visual features
				of the Web Service Client wizard since, in addition to
				being its own wizard, it is quite literally embedded
				within the larger Web Service wizard.
			</li>
		</ol>

	</body>
</html>
