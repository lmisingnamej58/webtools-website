<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../../../" />
		<title>Using Web Service Explorer to test a Web service</title>
	</head>
	<body>
		<h1>Using Web Service Explorer to test a Web service</h1>
		<p>
			<b>By Kathy Chan</b>
			<br />
			April 29, 2005
			<br />
			<br />
		</p>

		<h2>Introduction</h2>
		<br></br>
		<p>
			This document is a one of a series of tutorials to
			demonstrate the use of the Web Services tools in the Web
			Tools Platform Project using the <b>M4 driver</b>.
		</p>
		<p>
			This tutorial shows how to use the Web Service Explorer to
			test a Web service via native WSDL and SOAP. It demonstrates
			how to use the Web Services Explorer to invoke operations on
			a Web service named "
			<a
				href="http://www.xmethods.net/ve2/ViewListing.po;jsessionid=TZdF2D6E_KB9KKIY5_DG4FJw(QHyMHiRM)?key=uuid:477CEED8-1EDD-89FA-1070-6C2DBE1685F8">
				Temperature - Weather
			</a>
			" as available from
			<a href="http://www.xmethods.net/">XMethods</a>
			on the internet.
		</p>
		<p>
			The only prerequisite is that you be connected to the
			internet. If you are connected via a firewall, you can use
			the Internet preferences page (choose
			<b>Preferences -> Internet</b>
			from the main menu). Other than that, you can begin with a
			completely fresh workspace and no installed servers.
		</p>

		<br />
		
		<h2>Using the Web Services Explorer to test a Web service</h2>
		<br></br>
		<ol>
			<li>Launch the Eclipse WTP workbench.</li>

			<li>
				From the main menu bar, select
				<b>Run -> Launch the Web Services Explorer</b>
				. This will take a few moments the first time while
				Eclipse's internal Tomcat server starts up and loads the
				Web Services Explorer application.
			</li>

			<li>
				After the Web Browser view opens, maximize it. Result:
			</li>
			<br></br>
			<img border="0"
				SRC="../../M3/tutorials/resources/WebServiceExplorer/imageU71.JPG" />
			<li>
				In the Web Service Explorer's toolbar (top-right), click
				on the
				<b>WSDL Page</b>
				icon,
			</li>
			<br></br>
			<img border="0" SRC="../../M3/tutorials/resources/WebServiceExplorer/wsdl.gif" />
			<br></br>
			<img border="0"
				SRC="../../M3/tutorials/resources/WebServiceExplorer/imageEJ9.JPG" />

			<li>
				Click on the
				<img border="0"
					SRC="../../M3/tutorials/resources/WebServiceExplorer/wsdl_main.gif" />
				<b>WSDL Main</b>
				icon. Result:
			</li>
			<br />
			<img border="0"
				SRC="../../M3/tutorials/resources/WebServiceExplorer/imageIAI.JPG" />

			<li>
				In the
				<b>WSDL URL</b>
				field, enter the URL
				<tt>
					http://www.xmethods.net/sd/2001/TemperatureService.wsdl
				</tt>
				, then click on
				<b>Go</b>
				. Result:
			</li>
			<br />
			<img border="0"
				SRC="../../M3/tutorials/resources/WebServiceExplorer/imageO78.JPG" />

			<li>
				Under
				<b>Operations</b>
				in the right hand pane, click on
				<b>getTemp</b>
				. Result:
			</li>
			<br></br>
			<img border="0"
				SRC="../../M3/tutorials/resources/WebServiceExplorer/image99L.JPG" />

			<li>
				In the
				<b>zipcode string</b>
				entry field, enter
				<tt>90210</tt>
				, then click on
				<b>Go</b>
				. In the
				<b>Status</b>
				pane, you should see an answer like this:
			</li>

			<br />
			<b>return (float): 57.0</b>
			<p>
				You may need to move the horizontal bar separating the
				<b>Actions</b>
				and
				<b>Status</b>
				panes upwards a bit to get a better view. Result:
			</p>
			<br></br>
			<img border="0"
				SRC="../../M3/tutorials/resources/WebServiceExplorer/imageLKF.JPG" />

		</ol>
		<h2>Explanation</h2>
		<br />
		<p>
			The Web Services Explorer is a JSP Web application hosted on
			the Apache Tomcat servlet engine contained within Eclipse.
			It is integrated into Eclipse on two levels: visually by
			virtue of it running in the embedded browser, and logically
			by virtue of it running as a thread in the Eclipse JRE.
			Though not demonstrated in this scenario, this latter type
			of integration allows the Web Services Explorer to access
			resources in the workspace, write resources into the
			workspace, and launch various Web services wizards.
		</p>
		<p>
			The Web Services Explorer provides three key services to the
			user:
		</p>
		<ol>
			<li>
				Comprehensive support for discovering Web services in
				WS-Inspection 1.0 documents, and in UDDI v2 or v3
				registries using the UDDI v2 protocol.
			</li>

			<li>
				Comprehensive support for publishing Web services to
				UDDI v2 or v3 registries using the UDDI v2 protocol.
			</li>

			<li>
				Comprehensive support for browsing and invoking Web
				services natively via their WSDL and XSD.
			</li>
		</ol>
		<p>
			<img border="0" SRC="../../M3/tutorials/resources/WebServiceExplorer/note.gif"
				alt="Note: " />
			<br />
			A key point of the scenario above is that no code was
			generated and no servers were required in order to invoke
			operations on the WSDL.
		</p>

	</body>
</html>
