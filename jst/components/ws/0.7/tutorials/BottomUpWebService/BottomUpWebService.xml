<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../../../../" />
		<title>Bottom Up Web Service Supporting Flexible Projects</title>
	</head>
	<body>
		<h1>Bottom Up Web Service Supporting Flexible Projects</h1>
		<p>
			<b>By Kathy Chan</b>
			<br />
			Aug 11, 2005
			<br />
			<br />
		</p>

		<h2>Introduction</h2>
		<br></br>
		<p>
			This document is one of a series of tutorials to
			demonstrate the use of the Web Services tools in the Web
			Tools Platform Project using the <b>WTP 0.7 drivers</b>.
		</p>
		<p>
			This tutorial shows how to create a simple Web service and
			Web service client from a Java class. The Java class in this
			scenario converts between the Celsius and Farenheit
			temperature scales.
		</p>
		<br></br>
		
		<h2>Set Up</h2>
		<br></br>
		<p>
			Before creating the Web service, there are three
			prerequisites:
			<ol>
				<li>
					<a href="../InstallTomcat/InstallTomcat.html">
						Install Apache Tomcat
					</a>
				</li>
				<li>
				Set the <b>J2EE Project Module Preference</b> to
				allow multiple modules per project.  Open 
				<b>Window -&gt; Preferences... -&gt; J2EE Project Modules Preference
				</b> and check the <b>Allow Multiple modules per project</b> option.
				</li>
				<li>Create a Web project</li>
				<ol>
					<li>
						Open
						<b>
							File -&gt; New -&gt; Other... -&gt; Web -&gt; Dynamic Web Project
						</b>
						to create a new flexible project and Web module.
					</li>
					<li>Click <b>Next</b>.</li>
					<li>Add a new project by clicking <b>New...</b> beside the project name field.</li>
					<li>
						Enter
						<tt>ConverterProject</tt>
						into the
						<b>Name</b>
						field.
					</li>
					
					<li>
						Ensure that the 
						<b>Target server</b>
						is set to 
						<b>Apache Tomcat v5.0</b>
						.  If you have not previously configured
						a server runtime use the <b>New...</b>
						button beside the <b>Target server</b> field to
						do so.  Result:
					</li>
					
					<br></br>
					<img border="0"
						SRC="images/ConverterProject.JPG" />
					<br></br>
					
					<li>Click <b>Finish</b> to go back to the <b>Dynamic Web Project</b> wizard.</li>
					
					<li>
						Enter
						<tt>converter</tt>
						into the
						<b>Module Name</b>
						field.  Result:
					</li>
					
					<br/>
					<img border="0"
						SRC="images/converter.JPG" />
					<br/>
					
					<li>
						Click
						<b>Finish</b>
						.
					</li>
					<li>
					Say <b>Yes</b> to open the J2EE perspective and you will
					see the <tt>ConverterProject</tt> you created listed
					under <b>Dynamic Web Projects</b> in the <b>Project Explorer</b>
					view. Expand the <tt>ConverterProject</tt> project to
					see its contents.
					</li>
				</ol>
			</ol>
		</p>

		<h2>
			Creating a bottom up Java bean Web service and Web service
			client
		</h2>
		<br></br>
		<ol>
			<li>
				Import the
				<a
					href="srcfiles/wtp/Converter.java">
					wtp/Converter.java
				</a>
				class into Converter/JavaSource (be sure to preserve the
				package).
			</li>
			<li>
				Select the
				<b>Converter.java</b>
				file.
			</li>
			<li>
				Open <b>File -&gt; New -&gt; Other... -&gt; Web Services
				-&gt; Web Service</b>.
			</li>
			<li>
				Click <b>Next</b>.
			</li>
			<li>
				Select
				<b>Generate a proxy</b>
				.
			</li>
			<li>
				Select
				<b>Test the Web service</b>
				.
			</li>
			<li>
				Select
				<b>Monitor the Web service</b>
				.
			</li>
			<li>
				Select
				<b>Overwrite files without warning</b>
				.
			</li>
			<li>
				Result:
			</li>

			<br></br>
			<img border="0"
				src="images/bu1.JPG" />
			<br/>
			
			<li>
				Click
				<b>Next</b>
				. Result:
			</li>

			<br></br>
			<img border="0"
				src="images/bu2.JPG" />
            <br/>

			<li>
				Click
				<b>Next</b>
				to get to the
				<b>Service Deployment Configuration Page</b>
				. Result:
			</li>

			<br></br>
			<img border="0"
				src="images/bu3.JPG" />
		    <br/>
		    
			<li>
				If you want to choose a server different from the one
				defaulted by the wizard, click the
				<b>Edit</b>
				button to:
				<a href="../SelectServer/SelectServer.html">
					select a server
				</a>
				.
			</li>
			<li>
				Click
				<b>Finish</b>
				.
			</li>
			<li>
				It will take about one minute for the wizard to assemble
				the Web service and Web service client Web projects,
				start Apache Tomcat, and deploy the projects to Tomcat.
				Once finished, the generated Sample JSP Web application
				will appear in the browser view, maximized here for
				clarity:
			</li>

			<br></br>
			<img border="0"
				src="images/bu4.JPG" />
			<br/>
			
			<li>
				Under
				<b>Methods</b>
				, click on
				<b>celsiusToFarenheit(float)</b>
				.
			</li>
			<li>
				Under
				<b>Inputs</b>
				, enter
				<tt>37</tt>
				into the
				<b>celsius</b>
				entry field.
			</li>
			<li>
				Click on
				<b>Invoke</b>. 
				In the <b>Result</b> view, you should get a response of
				<b>98.6</b>.
			</li>

			<br></br>
			<img border="0"
				src="images/bu5.JPG" />
			<br/>
			
			<li>
				<p>
					Since you selected the "Monitor Web service" checkbox, 
					a TCP/IP port is automatically created on the
					Tomcat server. When you invoke the Web service,
					the <b>TCP/IP Monitor</b> view comes up automatically showing
					the SOAP message request and response. Double-click
					on the <b>TCP/IP Monitor</b> view and select XML view in the drop-down
					combo for both request and response. Result:
				</p>
			</li>

			<br></br>
			<img border="0"
				src="images/bu6.JPG" />
 			<br/>
 			
		</ol>
		<h2>Explanation</h2>
		<p>
			The Web Service wizard is orchestrating the end-to-end
			generation, assembly, deployment, installation and execution
			of the Web service, Web service client, and sample JSPs. In
			this scenario, we clicked Finish on page one. In effect, this
			is allowing the wizard to pick reasonable defaults according
			to the basic high-level choices made on the first page. If
			you repeat this scenario, but use the Next button to work
			through the wizard, you will learn more about the kinds of
			choices that are available and the kinds of defaults being
			assumed.
		</p>
		<p>
			After completing this scenario, the WSDL for the Converter
			Web service can be found in
			<tt>Converter/WebContent/wsdl/Converter.wsdl</tt>.
		</p>
		<p>
			Now that your Web service is running, there are a few
			interesting things you can do with this WSDL file. Examples:
		</p>
		<ol>
			<li>
				You can double-click on the WSDL to open the WSDL
				graphical editor.
			</li>

			<li>
				You can right-click on the WSDL and choose
				<b>
					Web Services -&gt; Test with Web Services Explorer
				</b>
				to test the service.
			</li>

			<li>
				You can right-click on the WSDL and choose
				<b>Web Services -&gt; Publish WSDL file</b>
				to publish the service to a public UDDI registry.
			</li>

			<li>
				You can click on the WSDL and choose
				<b>
					New -&gt; Other... -&gt; Web Services -&gt;
					Web Service Client
				</b>
				to generate a Web service client. Note, however, that we
				have already witnessed the internal and visual features
				of the Web Service Client wizard since, in addition to
				being its own wizard, it is quite literally embedded
				within the larger Web Service wizard.
			</li>
		</ol>

	</body>
</html>
