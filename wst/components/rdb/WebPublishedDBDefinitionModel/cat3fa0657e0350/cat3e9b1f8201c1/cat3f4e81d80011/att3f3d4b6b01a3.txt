From 5WD-02-Foundation-2002-12
4.27 SQL-invoked routines

An SQL-invoked routine has a routine authorization identifier, which is (directly or indirectly) the authorization
identifier of the owner of the schema that contains the SQL-invoked routine at the time that the SQL-invoked
routine is created.
