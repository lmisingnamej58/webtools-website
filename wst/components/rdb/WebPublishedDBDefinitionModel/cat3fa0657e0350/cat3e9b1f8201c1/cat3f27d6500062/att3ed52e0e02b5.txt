Section 4.17.2
A check constraint is satisfied if and only if the specified <search condition> is not False for any row of a table.
