<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Source Validation in SSE editors</title>
<link type="text/css" rel="stylesheet" href="theme/tutorial-style.css" />
</head>

<body>

<h1>Source Validation in the Structured Source Editor</h1>
<h2>(aka as-you-type validation)</h2>

<b>by Phil Avery, IBM</b>
<br />
2005-11-02

<br />
<br />
<b>Table Of Contents</b>
<ol>
	<li><a href="#whatis">What is it?</a></li>
	<li><a href="#using">End User Usage</a></li>
	<li><a href="#extending">Extending the Source Validation Framework</a></li>
	<li><a href="#foovalidator">FooValidator example</a></li>
	<li><a href="#vsbatch">Source Validation vs. "batch" Workspace Validation</a></li>
	<li><a href="#future">(possible) future enhancements</a></li>
</ol>

<a name="whatis"></a>
<h3>&nbsp;What is it?</h3>

<p>Source validation is a way to communicate errors, warnings and other
information to the user "as-you-type" in the Structured Source Editor
(SSE).
<p>The SSE component allows clients to contribute their own custom
validation to this feature via extension point. All that's required is
an implementation of IValidator.</p>

<a name="using"></a>
<h3>&nbsp;End User Usage</h3>

<p>Create a new file.</p>
<p>To enable source validation, make sure you've checked the preference:
Window &gt; Preferences... &gt; General &gt; Text Editors
&gt; Structured Text Editors &gt; Report problems as you type</p>
<img src="images/source-validation/as-you-type-pref.png" />

<p>Now type in the file. Depending on how your preferences are
configured, this is done by drawing various colored "squiggles" where a
suspected error appears in the editor.</p>
<img src="images/source-validation/squiggle-error.png" />

<p>These errors show up in the overview ruler as well.  
Clicking on these errors will move the cursor to that position in the editor. </p>
<img src="images/source-validation/overview-error.png" />

<p>If you hover the mouse over one of these squiggles or marks in the
overview ruler, you'll get hover help with detailed info about the
error/warning.</p>
<img src="images/source-validation/hover-error.png" />

<a name="extending"></a>
<h3>&nbsp;Extending the Source Validation Framework</h3>

<p>Clients can contribute their own custom validation to the source
editor if they have an IValidator. All that's required is to specify the
IValidator implementation, and for which content type/partitions it can
operate.
<p>Here's an example of how HTML Validator contributes to source
validation via the extension point:</p>
<table align="center" bgcolor="#f0f0f0">
	<tr>
		<td><pre>
&lt;extension
           point=&quot;org.eclipse.wst.sse.ui.sourcevalidation&quot;&gt;
        &lt;validator
              scope=&quot;total&quot;
              class=&quot;org.eclipse.wst.validation.html.HTMLValidator&quot;
              id=&quot;org.eclipse.wst.validation.htmlsourcevalidator&quot;&gt;
           &lt;contentTypeIdentifier
                 id=&quot;org.eclipse.wst.html.core.htmlsource&quot;&gt;
              &lt;partitionType
                    id=&quot;org.eclipse.wst.html.HTML_DEFAULT&quot;&gt;
              &lt;/partitionType&gt;
           &lt;/contentTypeIdentifier&gt;
           &lt;contentTypeIdentifier
                 id=&quot;org.eclipse.jst.jsp.core.jspsource&quot;&gt;
              &lt;partitionType
                    id=&quot;org.eclipse.wst.html.HTML_DEFAULT&quot;&gt;
              &lt;/partitionType&gt;
           &lt;/contentTypeIdentifier&gt;
        &lt;/validator&gt;
&lt;/extension&gt;
</pre></td>
	</tr>
</table>

<p>So this example says "run the HTML Validator (a total scope
validator) on files of HTML or JSP content type, on partitions of
HTML_DEFAULT." Total scope means the validator analyzes the entire file
every time it's invoked. This is used so the source validation framework
can better optimize calls to this validator.</p>
<ul>
	<li><b>extension</b>
	<ul>
		<li><b>point</b> - org.eclipse.wst.sse.ui.extensions.sourcevalidation
		</li>
	</ul>
	</li>

	<li><b>validator</b>
	<ul>
		<li><b>scope</b> - [total | partial] describes what area of the
		document is validated each time validate is called on the IValidator
		(for performance purposes)</li>
		<li><b>class</b> - the implementation class of IValidator</li>
		<li><b>id</b> - the unique id for the validator being contributed</li>
	</ul>
	</li>

	<li><b>contentTypeIdentifier</b>
	<ul>
		<li><b>id</b> - the id of an IContentType on which this IValidator can
		operate. This string is contributed via the <code>org.eclipse.core.runtime.ContentTypes</code>
		extension point.</li>
	</ul>
	</li>
	<li><b>partition</b>
	<ul>
		<li><b>id</b> - the id of a PartitionType <code>org.eclipse.jface.text.ITypedRegion#getType()</code>
		on which this as-you-type IValidator can operate. <br />
		These partition type definitions may be found in: <code>org.eclipse.wst.sse.core.text.IStructuredPartitions</code>
		<code>org.eclipse.wst.xml.core.text.IXMLPartitions</code> <code>org.eclipse.wst.html.core.text.IHTMLPartitions</code>
		<code>org.eclipse.jst.jsp.core.text.IJSPPartitions</code> <code>org.eclipse.wst.dtd.core.text.IDTDPartitions</code>
		<code>org.eclipse.wst.css.core.text.ICSSPartitions</code></li>
	</ul>
	</li>
</ul>

<p>
	<b>ISourceValidator</b>
</p>

<p>
	Since validating an entire document every time a user types is expensive, we've introduced
	the ISourceValidator interface.
</p>

<p>	
	First <code>connect(IDocument)</code> is called to give the validator a reference to the document being 
	validated.  The corresponding <code>disconnect(IDocument)</code> is called when the Editor is closed,
	the IDocument goes away, and the ISourceValidator won't be called anymore.
</p>

<p>
	While connected to the Document, <code>validate(IRegion, IValidationContext, IReporter)</code>
	will be called whenever the document has a dirty region.  The ISourceValidator can then
	optimize it's validation according to the dirty IRegion from the call.
</p>

<a name="foovalidator"></a>
<h3>&nbsp;The FooValidator example</h3>
<p>
This is an example of a simple validator.  It looks for the first occurrence of the string "foo" 
in the dirty region and underlines it.  
</p>
<p>
First create a pluging project called <b>sourcevalidator.test</b>
Make sure the bundle is a singleton, that is, the MANIFEST.MF file should contain this line:
<br/><b>Bundle-SymbolicName: sourcevalidator.test; singleton:=true</b>
</p>
<!-- ADD THE EXTENSION POINT -->
<p>
add this extension point description to the plugin.xml
</p>
<table align="center" bgcolor="#f0f0f0">
	<tr>
		<td>
		<pre>
	&lt;extension point="org.eclipse.wst.sse.ui.sourcevalidation"&gt;
		&lt;validator
			scope="total"
			class="sourcevalidator.test.FooValidator"
			id="sourcevalidator.test.FooValidator"&gt;
			&lt;contentTypeIdentifier id="org.eclipse.core.runtime.xml"&gt;
				&lt;partitionType id="org.eclipse.wst.xml.XML_DEFAULT"&gt;
				&lt;/partitionType&gt;
			&lt;/contentTypeIdentifier&gt;
		&lt;/validator&gt;
	&lt;/extension&gt;
		</pre>
		</td>
	</tr>
</table>



<!-- FooValidator.java -->
<p>
create the class <b>FooValidator.java</b>
</p>
<table align="center" bgcolor="#f0f0f0">
	<tr>
		<td>
		<pre>
package sourcevalidator.test;

import java.util.Locale;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.wst.sse.ui.internal.reconcile.validator.ISourceValidator;
import org.eclipse.wst.validation.internal.core.Message;
import org.eclipse.wst.validation.internal.core.ValidationException;
import org.eclipse.wst.validation.internal.provisional.core.IMessage;
import org.eclipse.wst.validation.internal.provisional.core.IReporter;
import org.eclipse.wst.validation.internal.provisional.core.IValidationContext;
import org.eclipse.wst.validation.internal.provisional.core.IValidator;

public class FooValidator implements ISourceValidator, IValidator {
	
	protected class LocalizedMessage extends Message {

		private String _message = null;

		public LocalizedMessage(int severity, String messageText) {
			this(severity, messageText, null);
		}

		public LocalizedMessage(int severity, String messageText, IResource targetObject) {
			this(severity, messageText, (Object) targetObject);
		}

		public LocalizedMessage(int severity, String messageText, Object targetObject) {
			super(null, severity, null);
			setLocalizedMessage(messageText);
			setTargetObject(targetObject);
		}

		public void setLocalizedMessage(String message) {
			_message = message;
		}

		public String getLocalizedMessage() {
			return _message;
		}

		public String getText() {
			return getLocalizedMessage();
		}

		public String getText(ClassLoader cl) {
			return getLocalizedMessage();
		}

		public String getText(Locale l) {
			return getLocalizedMessage();
		}

		public String getText(Locale l, ClassLoader cl) {
			return getLocalizedMessage();
		}
	}
	
	IDocument fDocument = null;
	public void connect(IDocument document) {
		fDocument = document;
	}

	public void disconnect(IDocument document) {
		fDocument = null;
	}
	
	/**
	 * implement ISourceValidator
	 */
	public void validate(IRegion dirtyRegion, IValidationContext helper, IReporter reporter) {
		// look for foo
		int start = dirtyRegion.getOffset();
		int length = dirtyRegion.getLength();
		String dirtyText = fDocument.get().substring(start, start + length);
		
		int fooIndex = dirtyText.indexOf("foo");
		if(fooIndex != -1) {
			IMessage m = new LocalizedMessage(IMessage.HIGH_SEVERITY, "found foo");
			m.setOffset(start + fooIndex);
			m.setLength("foo".length());
			try {
				m.setLineNo(fDocument.getLineOfOffset(start + fooIndex) + 1);
			}
			catch (BadLocationException e) {
				m.setLineNo(-1);
			}
			reporter.addMessage(this, m);
		}
	}

	public void cleanup(IReporter reporter) {
		// TODO Auto-generated method stub
		
	}

	public void validate(IValidationContext helper, IReporter reporter) throws ValidationException {
		// TODO Auto-generated method stub
		
	} 
}

		</pre>
		</td>
	</tr>
</table>

<p>
Now just start the workbench up and creat any *.xml file.  Type "foo" and it should be underlined! 
Change it to "fo" and the squiggle should disappear.
</p>

<a name="vsbatch"></a>
<h3>&nbsp;Source Editor Validation vs. Batch Workspace Validation</h3>
<p>Source Validation is often confused with Batch Workspace Validation.
These are two different frameworks. They're invoked and displayed
differently in the workbench (even if they use the same IValidator under
the covers).</p>
<p><b>Source validation</b> can be thought of as "as-you-type"
validation, as errors/warnings show up in the editor as you type.</p>

<p><b>Batch Validation</b> only occurs on save or build, or when
explicitly invoked via the context menu in the Project Explorer. These
errors not only show up in the editor, but also in the Problems View,
and as little overlay icons on resources in the navigator views.</p>
<p>Batch validation adds markers to the resources in your workspace.
this means that errors, warnings, and info messages will appear in the
Problems View</p>
<img src="images/source-validation/problems-view.png" />

<p>Batch validation errors cause an icon to appear in the left hand
ruler</p>
<img src="images/source-validation/left-margin-error.png" />

<p>Batch validation errors appear in some Navigator Views as little
error overlay icons</p>
<img src="images/source-validation/navigator-view.png" />
<p>If the same IValidator is used for source and batch validation, if a
Batch Validation error is corrected in the "live" source editor, the
icon on the left hand ruler will gray out, indicating that the problem
has (probably) been fixed. When you save the file, the error should then
disappear if the problem was in fact fixed.</p>
<img src="images/source-validation/gray-error.png" />

<a name="future"></a>
<h3>&nbsp;Future Enhancements</h3>

<p><b>Enablement Preferences</b></p>
It would be useful to have ability to turn off certain validators. For
example, maybe you want to turn off HTML validation for JSPs because
you're only working on the JSP code sections at the moment.

<p><b>"Partial Document" Validation</b></p>
Partial document validation would be a great performance improvement (if
a validator supports it).

<p><b>Other possible source validation:</b></p>
<ul>
	<li>links</li>
	<li>spelling</li>
</ul>
</body>
</html>
