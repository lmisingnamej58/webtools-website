<?php
	    $debug = false;
		ini_set("display_errors", "true");
    	error_reporting (E_ALL);
?>
<html>
<head>
<meta
    http-equiv="Content-Type"
    content="text/html; charset=iso-8859-1">
<title>Utility</title>
</head>
<body>
<h1>Utility List of bugzilla IDs</h1>
<table
    align="center"
    cellspacing="2"
    width="60%"
    border="1">
    <tr>
        <th>userid</th>
        <th>login_name</th>
        <th>realname</th>
    </tr>
    <?php

    require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";


    # Connect to database
    $sqlquery = "SELECT * FROM profiles
    		WHERE 
    		    login_name='kaloyan.raev@sap.com' OR
    		    login_name='neil.hauge@oracle.com' OR
    		    login_name='deboer@ca.ibm.com' OR
    		    login_name='naci.dai@eteration.com' OR
    		    login_name='raghunathan.srinivasan@oracle.com' OR
    		    login_name='cbridgha@us.ibm.com' OR
                  login_name='david_williams@us.ibm.com' 
    		";

    $dbc = new DBConnectionBugs();
    $dbh = $dbc->connect();
    $rs = mysql_query($sqlquery, $dbh);

    if(mysql_errno($dbh) > 0) {
    	echo "There was an error processing this request <br />".
    	$dbc->disconnect();
    	exit;
    }

    while($myrow = mysql_fetch_assoc($rs))
    {

    	if ($debug) {
    		echo "Debug: myrow: " ; echo print_r($myrow) ; echo  " <br />";
    	}
    	echo "<tr>";
    	echo "<td>" . $myrow['userid'] . "</td>";
    	echo "<td>" . $myrow['login_name'] . "</td>";
    	echo "<td>" . $myrow['realname'] . "</td>";
    	echo "</tr>";
    }

    $dbc->disconnect();
    $rs = null;
    $dbh = null;
    $dbc = null;
    ?>
</table>
</body>
</html>
