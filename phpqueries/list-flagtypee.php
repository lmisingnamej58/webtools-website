<?php
$debug = true;
// for reference
// See https://dev.eclipse.org/committers/committertools/dbo_bugs_schema.php
// and https://dev.eclipse.org/committers/committertools/dbo_index.php

if ($debug)
{
    ini_set("display_errors", "true");
    error_reporting (E_ALL);
}

require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";

?>
<html>
<head>
<meta
    http-equiv="Content-Type"
    content="text/html; charset=iso-8859-1">
<link
    rel="stylesheet"
    href="../../../default_style.css"
    type="text/css">
<link
    rel="stylesheet"
    href="../../wtp.css"
    type="text/css">
<title>List of flag types</title>
</head>
<body>
<table
    width="100%"
    cellspacing="5"
    cellpadding="2"
    border="0">
    <tr>
        <td
            width="60%"
            align="left"><font class="indextop">List of flag types</font> <br>
        <font class="indexsub">List of flag types</font></td>
        <td width="40%"><img
            width="207"
            hspace="50"
            height="129"
            align="middle"
            src="../../images/wtplogosmall.jpg"></td>
    </tr>
</table>
<table
    width="100%"
    cellspacing="5"
    cellpadding="2"
    border="0">
    <tr>
        <td
            valign="top"
            bgcolor="#0080c0"
            align="left"
            colspan="2"><b> <font
            face="Arial,Helvetica"
            color="#ffffff">List of flag types</font></b></td>
    </tr>
</table>
<table
    align="center"
    cellpadding="4" 
    width="80%"
    border="1">
    <tr>
        <th>Result row</th>
        <th>ID</th>
        <th>Flag type name</th>
        <th>Description</th>
    </tr>
    <?php


    # Connect to database
    $sqlquery = "SELECT
    id, name, description
    FROM
    flagtypes"; 

    $dbc = new DBConnectionBugs();
    $dbh = $dbc->connect();
    $rs = mysql_query($sqlquery, $dbh);

    if(mysql_errno($dbh) > 0) { 
        echo "There was an error processing this request <br />".
        $dbc->disconnect();
        exit;
    }
    $rowcount = 0;
    while($myrow = mysql_fetch_assoc($rs))
    {
        $rowcount++;
        if ($debug) {
            echo "Debug: myrow: " ;
            echo print_r($myrow) ;
            echo  " <br />";
        }
        echo "<tr>";
        echo "<td align='right'>".$rowcount."</td>";
        echo "<td align='right'>".$myrow['id']."</td>";
        echo "<td align='right'>".$myrow['name']."</td>";
        echo "<td align='right'>".$myrow['description']."</td>";
        echo "</tr>";
    }    
    ?>
</table>
</body>
</html>
