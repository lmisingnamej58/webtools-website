<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

#*****************************************************************************
#
# template.php
#
# Author: 		15022010
# Date:			2010-02-10
#
# Description: Type your page comments here - these are not sent to the browser
#
#
#****************************************************************************

#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Web Tools Platform (WTP) Project";
$pageKeywords	= "web,J2EE";
$pageAuthor		="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
// Import the functions that render news.
// require_once ($root . '/webtools/news.php');
// Import common WTP functions.
require_once ($root . '/webtools/common.php');
require_once ($root . '/webtools/newstohtml.php');

// Generate the HTML content for the news based on the news.xml file
// that appears in the root directory.
$newsTitle = "Eclipse Web Tools Platform Project: News";
$wtpnews = news_to_html($root . "/webtools/news.xml", "$newsTitle", "/webtools/news/", "/webtools/news.php", true, "long", "5");

// Get the project descriptions
$wtpoverview = read_file($root . '/webtools/project-info/description.html');
$commonoverview = read_file('project-info/common-description.html');
$ejboverview = read_file('project-info/ejb-description.html');
$jeeoverview = read_file('project-info/jee-description.html');
$rdboverview = read_file('project-info/rdb-description.html');
$serveroverview = read_file('project-info/server-description.html');
$sseoverview = read_file('project-info/sse-description.html');
$wsoverview = read_file('project-info/ws-description.html');
$atfoverview = read_file($root . '/webtools/project-info/atf-description.html');
$dalioverview = read_file($root . '/webtools/project-info/dali-description.html');
$jsfoverview = read_file($root . '/webtools/project-info/jsf-description.html');
$incoverview = read_file($root . '/webtools/project-info/inc-description.html');
# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links", 	"downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
			<table>
				<tr>
					<td>
					<p>
					$wtpoverview
					<br /> <a href="about.php">more about wtp &raquo;</a>
 					</p>
					</td>
					<td>
					<img src="/webtools/images/wtplogosmall.jpg" align="middle" height="129" hspace="50" width="207" />
					</td>
				</tr>
			</table>
		<div class="homeitem3col">
		$wtpnews
		</div>
		<div class="homeitem3col">
			<h3>Subprojects</h3>
			<ul>		
				<li>
					<b>AJAX Toolkit Framework (ATF) - incubating</b>&nbsp;<a href="atf/main.php"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$atfoverview
					<br /> <a href="atf/main.php">more about atf &raquo;</a>
    			</li>
				<li>
					<b>Common WTP stuff</b>&nbsp;<a href="common/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$commonoverview
					<br /> <a href="common/">more about common components &raquo;</a>
    			</li>
    			<li>
					<b>Dali JPA Tools</b>&nbsp;<a href="dali/main.php"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$dalioverview
					<br /> <a href="dali/main.php">more about dali &raquo;</a>
    			</li>
    			<li>
    				<b>EJB Tools</b>&nbsp;<a href="ejb/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$ejboverview
					<br /> <a href="ejb/">more about EJB&raquo;</a>
    			</li>
    			<li>
    				<b>Java EE Tools</b>&nbsp;<a href="jee/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$jeeoverview
					<br /> <a href="jee/">more about Java EE&raquo;</a>
    			</li>		
    			<li>
					<b>JavaServer Faces Tools (JSF)</b>&nbsp;<a href="jsf/main.php"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$jsfoverview
					<br /> <a href="jsf/main.php">more about jsf &raquo;</a>
    			</li>
    			<li>
    				<b>RDB</b>&nbsp;<a href="rdb/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$rdboverview
					<br /> <a href="rdb/">more about the RDB subproject&raquo;</a>
    			</li>
    			<li>
    				<b>Server Tools</b>&nbsp;<a href="server/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$serveroverview
					<br /> <a href="server/">more about Server Tools&raquo;</a>
    			</li>	
    			<li>
    				<b>Source Editors</b>&nbsp;<a href="sse/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$sseoverview
					<br /> <a href="sse/">more about source editors&raquo;</a>
    			</li>	
    			<li>
    				<b>Web Services</b>&nbsp;<a href="ws/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$wsoverview
					<br /> <a href="ws/">more about Web services&raquo;</a>
    			</li>
    			<li>
    				<b>WTP Incubator</b>&nbsp;<a href="ws/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$incoverview
					<br /> <a href="inc/">more about WTP Incubator&raquo;</a>
    			</li>
    		</ul>
		</div>
		<p></p>
		<p></p>
	</div>
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Discover WTP</h6>
			<ul>
				<li><a href="http://download.eclipse.org/webtools/downloads/">Downloads</a></li>
				<li><a href="community/communityresources.php">Resources</a></li>
				<li><a href="https://wiki.eclipse.org/WTP_FAQ">FAQ</a></li>
				<li><a href="/newsgroups/main.html">Newsgroups</a></li>
			</ul>
		</div>
	</div>
</div>
EOHTML;


					# Generate the web page
					$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
					?>
