<?php

// Note: the HTML input files should have &nbsp; coded as &#160; to survive xsl transforms
// also '&' should be coded as '&amp;'
//


require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();       include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageKeywords   = "WTP FAQ Source Eclipse Webtools Web Tools Platform";
$pageAuthor     = "David Williams";
$root = $_SERVER['DOCUMENT_ROOT'];

$meetingFile="GettingSource.html";

if (file_exists($meetingFile)) {

    # Generate the web page
    // Load the XML source
    $xml = new DOMDocument;
    $xml->loadHTMLFile($meetingFile);

    //Set the page title
    $xpath = new DOMXPath($xml);
    $titleNode = $xpath->query("/html/head/title")->item(0);
    // echo "<br />titleNodeValue: " . $titleNode->nodeValue;
    $pageTitle = ($titleNode != null) ? $titleNode->childNodes->item(0)->nodeValue : "eclipse.org webtools page";

    // Load the XSL source
    $xsl = DOMDocument::load($root . '/webtools/wtpphoenix.xsl');

    // Configure the transformer
    $proc = new XSLTProcessor;
    $proc->importStyleSheet($xsl); // attach the xsl rules

    // work on just the body of the original (not head, etc.)
    $xmlbody=$xml->getElementsByTagName('body')->item(0);
    $maincontent = $proc->transformToXML($xmlbody);
    $html = <<<EOHTML
    <div id="maincontent">
    <div id="midcolumn">
    <p>
    $maincontent
        </p>
    </div>
</div>

EOHTML;
    $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

}
?>
