<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle = "WTP Incubator Downloads";
$pageKeywords = "Eclipse WTP webtools XSL XML IDE";
$pageAuthor ="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

# Generate the web page
// Load the XML source
$xml = DOMDocument::load('index.xml');

//Set the page title
$xpath = new DOMXPath($xml);
$titleNode = $xpath->query("/html/head/title")->item(0);
$pageTitle = ($titleNode != null) ? $titleNode->nodeValue : "eclipse.org webtools page";

// Load the XSL source
$xsl = DOMDocument::load('../../wtpnova.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$html = $proc->transformToXML($xml);
$html .= $wtpTopButtons;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>