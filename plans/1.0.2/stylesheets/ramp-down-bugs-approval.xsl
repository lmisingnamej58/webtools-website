<?xml version="1.0" encoding="utf-8"?>

<!-- 
	2006-03-30: Jeffrey Liu <jeffliu@ca.ibm.com>
	- Created.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" xmlns:bz="http://www.bugzilla.org/rdf#"
	exclude-result-prefixes="bz">

	<xsl:template match="/">
		<xsl:processing-instruction name="xml-stylesheet">
			type="text/xsl" href="stylesheets/wtp.xsl"
		</xsl:processing-instruction>

		<html>
			<head>
				<title>Web Tools 1.0.2 Bugs</title>
				<meta content="../../" name="root" />
			</head>

			<body>
				<h1>wtp 1.0.2 bug list</h1>
				<h2>Introduction</h2>
				<p>
					This document lists bugs that requires PMC approval in WTP 1.0.2.
				</p>
				<xsl:apply-templates select="bugs" />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="bugs">

		<table cellspacing="4" width="95%" cellpadding="5" border="1">
			<tr>
				<th>Bug</th>
				<th>Status</th>
				<th>Contact</th>
				<th>Time Submitted</th>
				<th>Yes</th>
				<th>No</th>
			</tr>

			<xsl:apply-templates select="bug">
				<xsl:sort select="id" data-type="number" />
			</xsl:apply-templates>

		</table>

	</xsl:template>

	<xsl:template match="bug">
		<xsl:variable name="id" select="id" />
		<tr>
			<td>
				<a
					href="https://bugs.eclipse.org/bugs/show_bug.cgi?id={id}">
					<xsl:value-of select="id" />
				</a>
			</td>
			<td>
				<xsl:choose>
					<xsl:when test="status = 'Approved'">
						<font color="green"><b>Approved</b></font>
					</xsl:when>
					<xsl:otherwise>
						Pending...
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td>
				<xsl:value-of select="contact" />
			</td>
			<td>
				<xsl:value-of select="timestamp" />
			</td>

			<td>
				<xsl:if test="count(yes) = 0">
					&#160;
				</xsl:if>
				<xsl:for-each select="yes">
					<xsl:value-of select="." /><br/>
				</xsl:for-each>
			</td>

			<td>
				<xsl:if test="count(no) = 0">
					&#160;
				</xsl:if>
				<xsl:for-each select="no">
					<xsl:value-of select="." /><br/>
				</xsl:for-each>
			</td>

		</tr>
	</xsl:template>

</xsl:stylesheet>