<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../wtp.xsl"?>
<html>
    <head>
        <meta name="root" content="../../../www/" />
        <title>xml wtp 2.0 final test plan</title>
    </head>
    <body>
        <h1>xml wtp 2.0 final test plan</h1>
        <h2>Status of this Plan</h2>
        <p>Proposed Plan (5.14.07)</p>

        <h2>Overall goals</h2>
        <h3>
            <b>Co-developer Testing</b>
        </h3>
        <p>
            We will inspect &quot;runtime&quot; version of build to be
            sure extra source is not included, and more important, we'll
            inspect and test importing SDK version to be sure all
            relevant &quot;open source&quot; is included in that SDK
            build and that it correctly imports into a development
            environment.
        </p>
        <h3>
            <b>API Testing</b>
        </h3>
        <p>
            XML API unit tests are in the org.eclipse.wst.xml.core.tests
            and org.eclipse.wst.xml.ui.tests plugins, located in the
            <i>tests</i>
            folder in the wst.xml component.
        </p>
        <p>
            We do have several hundred unit tests which we expect to be
            running and passing for 2.0, which test various aspects of
            parsing, model creation, and correct charset handling, among
            other things.
        </p>

        <h3>
            <b>End User Testing</b>
        </h3>
        <p>
            The nature of the end-user testing is intentionally planned
            to be "ad hoc" instead of specifying step by step "how to"
            directions and specific "expected results" sections often
            seen in test cases. This is done because its felt leads to
            greater number of "paths" being tested, and allows end-users
            more motivation for logging "bugs" if things didn't work as
            <i>they</i>
            expected, even if it is working as designed.
        </p>

        <p>
            As we progress through milestones, we'll add more and more
            detail for special cases, special files, special projects,
            etc.When we do have special or sample test files and
            projects, we will keep those stored in CVS, as projects
            under a
            <i>testdata</i>
            directory under the
            <i>development</i>
            directory of relevant component so that testers (from
            immediate team, or community) can easily check out into the
            environment being tested.
        </p>

        <h3>
            <b>Platform Testing</b>
        </h3>
        <p>
            While we do not have any platform specific code, or
            function, we will have some team members do end-user tests
            on Linux, some on Windows. We will also confirm unit tests
            pass on both platforms.
        </p>

        <h3>
            <b>Performance Testing</b>
        </h3>
        <p>
            We have added (some) automated performance tests along the
            lines of the Eclipse base performance unit tests in future
            milestones. These are currently in the
            <b>org.eclipse.wst.*.ui.tests.performance</b>
            and
            <b>org.eclipse.jst.jsp.ui.tests.performance</b>
            plugins.
            <br />
            <br />
            We will continue to add more test cases in upcoming
            milestones.
        </p>
        <h2>Testing focus for 2.0</h2>
        <ul>
            <li>Verify quick fix for spelling mistakes works</li>
            <li>
                Verify quick fix/assist for xml-related purposes works
                (quote attribute value, surround with new element)
            </li>
            <li>Verify xml wizard works properly</li>
            <li>
                Formatting:
                <ul>
                    <li>
                        Verify whitespace handling (ex: content in
                        xsl:text should not be modified) works
                    </li>
                    <li>
                        Verify new formatting preference "Align final
                        bracket in multi-line element tags" is obeyed
                    </li>
                    <li>
                        Verify new formatting preference "Preserve
                        whitespace in tags with PCDATA content" is
                        obeyed
                    </li>
                    <li>
                        Verify format with no selection will format
                        document while format while text selected will
                        only format selected text
                    </li>
                    <li>
                        Make sure to run
                        <a
                            href="../../../../wst/components/sse/tests/formatting-test.html">
                            formatting regression test
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                Preferences:
                <ul>
                    <li>
                        Syntax highlighting preferences:
                        <ul>
                            <li>
                                Verify entity references syntax color
                                preference is followed
                            </li>
                            <li>
                                Verify font styles (bold, italics,
                                strikethrough, etc) are obeyed
                            </li>
                            <li>
                                Verify background syntax color
                                preference is followed
                            </li>
                        </ul>
                    </li>
                    <li>Verify keywords for preference pages works</li>
                    <li>
                        Verify text editor's smart home/end caret
                        positioning preference is followed
                    </li>
                    <li>
                        Verify hyperlinking preferences in text editor's
                        hyperlinking preference page is followed
                    </li>
                    <li>
                        Verify content assist proposal
                        background/foreground color preferences in
                        structured text editor's preference page are
                        followed
                    </li>
                </ul>
            </li>
            <li>
                Verify tree path of current selection is displayed in
                status line
            </li>
            <li>
                Verify attribute hint (shift+ctrl+space) works and bolds
                existing attributes
            </li>
            <li>
                Verify Task Tags preference/properties pages are correct
                and correctly followed for workspace vs project vs file.
            </li>
            <li>
                Make sure to run
                <a
                    href="../../../../wst/components/sse/tests/action-test.html">
                    structured text editor action regression test
                </a>
            </li>
        </ul>
        <h2>XML Tests</h2>
        <ul>
            <li>
                Check Source Editing features from feature
                <a href="../sse/test_plan.php#matrix">feature matrix</a>
                <br />
                Check samples of range of types of web resources
                (web.xml, tld files, schema source, etc)
                <br />
                especially any we (WTP) generate
            </li>
            <li>
                Test Design page, Source page, Outline view, Properties
                view synchronization
            </li>
            <li>
                Check content assist and source validation using content
                model from the following source:
                <ul>
                    <li>
                        Inferred content model (XML with no DOCTYPE or
                        schema specified)
                    </li>
                    <li>DTD from XML catalog</li>
                    <li>DTD from URI</li>
                    <li>Schema from XML catalog</li>
                    <li>Schema from URI</li>
                    <li>
                        Basic schema (with lots of complex types, and
                        lots of 'includes')
                    </li>
                    <li>Advanced schema (with lots of namespaces)</li>
                </ul>
            </li>
        </ul>
        <li>
            As you type validation:
            <ul>
                <li>
                    Job based, make sure squiggles show up when they
                    should and are removed when problems are fixed
                </li>
                <li>Test various partition types in the document</li>
                <li>Xerces-style validators (for as-you-type)</li>
                <li>XSD validator (for as-you-type)</li>
                <li>
                    Problem icons (batch validation) should gray out in
                    the editor when fixed.
                </li>
            </ul>
        </li>

        <li>
            Hyperlink open on:
            <ul>
                <li>schema location, dtd location</li>
            </ul>
        </li>

        <li>
            Preferences:
            <ul>
                <li>
                    Test our editors follow preferences in the "All Text
                    Editors" preference page
                </li>
                <li>
                    Check that editors follow content type specific
                    preferences under Web and XML preference pages
                </li>
                <li>
                    Make sure editors (already open and closed and
                    reopened) are updated when preferences change
                </li>
                <li>
                    Check preferences are saved when shutdown and
                    restart workbench
                </li>
            </ul>
        </li>

        <li>
            Tab preferences:
            <ul>
                <li>
                    Web and XML->Content Type Files -> Content Type
                    Source -> Indent using tabs / Indent using spaces ->
                    Indentation size
                </li>

                <li>Verify the correct tab character is used</li>
                <li>
                    Verify the correct number of tab characters is used
                </li>
                <li>
                    Verify Source->Shift Left/Shift Right and the
                    Shift-Tab/Tab key follow the preferences
                </li>
                <li>With nothing selected</li>
                <li>With multiple lines selected</li>
                <li>Verify Format follow the preferences</li>
                <li>
                    Verify when using tab characters, the displayed tab
                    width preference is followed (General->Editors->Text
                    Editors -> Displayed tab width)
                </li>
            </ul>
        </li>
        <li>
            New File Wizard:
            <ul>
                <li>make sure the template creates a valid file</li>
                <li>
                    Verify not entering an extension will generate a new
                    file with the default file extension you specified
                    in the preference.
                </li>
                <li>
                    Verify entering a file name that already exists
                    without the extension will still give you an error
                    saying the file already exists (for example, if
                    index.jsp already exists, typing "index" will tell
                    you that it already exists)
                </li>
                <li>
                    Verify entering a file name with valid/invalid
                    extension still works
                </li>
            </ul>
        </li>
        <li>
            Referenced Files
            <ul>
                <li>Test errors in referenced files</li>
                <li>
                    good xml, bad xsd. ie. XSD errors should not show in
                    XML resource. There should be however one message to
                    indicate that there are errors in the referenced
                    file
                </li>
                <li>Test similarly also for good xml, bad dtd</li>
            </ul>
        </li>
        <li>
            Validation Preferences
            <ul>
                <li>Test validate on file save</li>
                <li>Turn it on and off</li>
                <li>Test global vs project settings</li>
                <li>Test maximum number of messages</li>
            </ul>
        </li>
        <li>
            Run performance unit tests:
            <ul>
                <li>
                    Record results for comparison with future milestones
                </li>
            </ul>
        </li>

        <li>
            Profiling:
            <ul>
                <li>
                    Test basic editor functions and look for problem
                    areas (large memory consumption, intense cpu usage)
                </li>
            </ul>
        </li>

        <h3>XML Catalog</h3>
        <ul>
            <li>Test that validation works with the XML Catalog</li>
        </ul>
        <h3>General Scenario</h3>
        <ul>
            <li>
                Validate an invalid document and ensure problems are
                shown as annotations in the editor's source ruler and
                margin
            </li>

            <li>
                Check that error messages are listed in the problems
                view
            </li>
            <li>Check that navigation works for error markers</li>
            <li>Check that line numbers match error markers</li>
            <li>
                Correct errors and revalidate and ensure all markers and
                messages are removed
            </li>
            <li>
                Ensure popup message dialog appear with the correct
                validation message
            </li>
        </ul>
        <h3>XML Examples</h3>
        <ul>
            <li>
                Create the Editing and validating XML files Examples
            </li>
            <li>
                Verify that the sample files are created in the
                designated folder
            </li>
        </ul>

        <h3>XML Tutorials</h3>
        <ul>
            <li>Excercise XML Catalog tutorial</li>
            <li>Excercise XML Validation tutorial</li>
            <li>Excersice Creating XML Documents tutorial</li>
        </ul>
        <h2>
            <b>Regression Tests</b>
        </h2>
        <p>
            <a
                href="../../../../wst/components/sse/tests/viewerconfig-test.html">
                Structured Text Viewer Configuration tests
            </a>
            <br />
            <a
                href="../../../../wst/components/sse/tests/action-test.html">
                Structured Text Editor Action tests
            </a>
            <br />
            <a
                href="../../../../wst/components/sse/tests/formatting-test.html">
                Formatting tests
            </a>
            <br />
            <a
                href="../../../../wst/components/sse/tests/codefolding-test.html">
                Code Folding tests
            </a>
            <br />
        </p>

        <h2>Source Editing Test Plans</h2>
        <p>
            <a href="../sse/test_plan.php">org.eclipse.wst.sse</a>
            <br />
            <a href="../xml/test_plan.php">org.eclipse.wst.xml</a>
            <br />
            <a href="../html/test_plan.php">org.eclipse.wst.html</a>
            <br />
            <a href="../css/test_plan.php">org.eclipse.wst.css</a>
            <br />
            <a href="../dtd/test_plan.php">org.eclipse.wst.dtd</a>
            <br />
            <a href="../javascript/test_plan.php">
                org.eclipse.wst.javascript
            </a>
            <br />
            <a href="../../jst/jsp/test_plan.php">
                org.eclipse.jst.jsp
            </a>
        </p>
    </body>
</html>
