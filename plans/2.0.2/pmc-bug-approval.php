<html>
<head>
<meta
    http-equiv="Content-Type"
    content="text/html; charset=UTF-8">
<meta
    http-equiv="Content-Type"
    content="text/html; charset=iso-8859-1">
<link
    rel="stylesheet"
    href="../../../default_style.css"
    type="text/css">
<link
    rel="stylesheet"
    href="../../wtp.css"
    type="text/css">
<title>Web Tools 2.0.2 Bugs</title>
</head>
<body>
<table
    width="100%"
    cellspacing="5"
    cellpadding="2"
    border="0">
    <tr>
        <td
            width="60%"
            align="left"><font class="indextop">wtp 2.0.2 pmc review bug list</font> <br>
        <font class="indexsub">Web Tools 2.0.2 Bugs for PMC Review</font></td>
        <td width="40%"><img
            width="207"
            hspace="50"
            height="129"
            align="middle"
            src="../..//images/wtplogosmall.jpg"></td>
    </tr>
</table>
<table
    width="100%"
    cellspacing="5"
    cellpadding="2"
    border="0">
    <tr>
        <td
            valign="top"
            bgcolor="#0080c0"
            align="left"
            colspan="2"><b> <font
            face="Arial,Helvetica"
            color="#ffffff">Introduction</font></b></td>
    </tr>
    <tr>
        <td
            valign="top"
            align="right">&nbsp;</td>
        <td valign="top">This document lists bugs that require PMC approval in WTP 2.0.2.<br>
        <a href="http://wiki.eclipse.org/index.php/WTP_PMC_Defect_Review">The WTP PMC Review process defined.</a></td>
    </tr>
</table>
<br>
<table
    width="100%"
    cellspacing="5"
    cellpadding="2"
    border="0">
    <tr>
        <td
            valign="top"
            bgcolor="#0080c0"
            align="left"
            colspan="2"><b> <font
            face="Arial,Helvetica"
            color="#ffffff">Defect List</font></b></td>
    </tr>
</table>
<table
    cellspacing="4"
    width="95%"
    cellpadding="5"
    border="1">
    <tr>
        <th>Bug</th>
        <th>Status</th>
        <th>Owner</th>
        <th>Yes</th>
        <th>No</th>
    </tr>
    <?php
    // votes required to be considered "approved"
    $nvotesRequired = 2;
    $debug = false;
    // for reference
    // See https://dev.eclipse.org/committers/committertools/dbo_bugs_schema.php
    // and https://dev.eclipse.org/committers/committertools/dbo_index.php

    if ($debug) {
    	ini_set("display_errors", "true");
    	error_reporting (E_ALL);
    }

    require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";


    # Connect to database
    $sqlquery = "SELECT
                    BUG.bug_id,
                    USR.realname,
                    USR.login_name,
                    BUG.status_whiteboard
                FROM 
                    bugs AS BUG 
                    INNER JOIN products AS PROD ON PROD.id = BUG.product_id
                    INNER JOIN profiles AS USR ON USR.userid = BUG.assigned_to 
                WHERE 
                    (BUG.target_milestone='2.0.2 M202'
                    OR BUG.target_milestone='1.0.2')
                    AND BUG.status_whiteboard like '%PMC%'
                    AND (PROD.name='Web Tools'
                    OR PROD.name='Dali JPA Tools'
                    OR PROD.name='Java Server Faces')
                ORDER BY 
                    BUG.bug_id";

    $dbc = new DBConnectionBugs();
    $dbh = $dbc->connect();
    $rs = mysql_query($sqlquery, $dbh);

    if(mysql_errno($dbh) > 0) {
    	echo "There was an error processing this request <br />".
    	$dbc->disconnect();
    	exit;
    }

    while($myrow = mysql_fetch_assoc($rs))
    {

    	if ($debug) {
    		echo "Debug: myrow: " ; echo print_r($myrow) ; echo  " <br />";
    	}
    	echo "<tr>";
    	echo "<td><a href=\"https://bugs.eclipse.org/bugs/show_bug.cgi?id=".$myrow['bug_id']."\">".$myrow['bug_id']."</a></td>";
    	echo "<td>";
    	$yes = "";
    	$no = "";
    	$pmc_approved = stripos($myrow['status_whiteboard'], 'PMC_approved');
    	$sqlquery3 = "SELECT
                flags.type_id,
                flags.setter_id,
                flags.status
            FROM 
                flags
            WHERE
                (flags.setter_id='583'
                OR flags.setter_id='550'
                OR flags.setter_id='12789'
                OR flags.setter_id='17882'
                OR flags.setter_id='18449')
                AND flags.bug_id=".$myrow['bug_id'];

    	$rs3 = mysql_query($sqlquery3, $dbh);
    	if(mysql_errno($dbh) <= 0)
    	{
    		$votes=0;
    		while($myrow3 = mysql_fetch_assoc($rs3)) {
    			if ($debug) {
    				echo "Debug: myrow3: " ; echo print_r($myrow3) ; echo  " <br />";
    			}
    			$sqlquery4 = "SELECT
                    flagtypes.name 
                FROM 
                    flagtypes
                WHERE
                    flagtypes.name='pmc_approved'
                    AND flagtypes.id=".$myrow3['type_id'];

    			$rs4 = mysql_query($sqlquery4, $dbh);
    			while ($myrow4 = mysql_fetch_assoc($rs4)) {
    				if ($debug) {

    					echo "Debug: myrow4: " ; echo print_r($myrow4) ; echo  " <br />";
    				}
    				if($myrow3['setter_id']=="583") {
    					$realname="David M Williams";
    				} else if($myrow3['setter_id']=="550") {
    					$realname="Tim deBoer";
    				} else if($myrow3['setter_id']=="12789") {
    					$realname="Naci Dai";
    				} else if($myrow3['setter_id']=="17882") {
    					$realname="Neil Hauge";
    				}  else if($myrow3['setter_id']=="18449") {
    					$realname="Raghu Srinivasan";
    				}

    				if ($myrow3['status']=="+") {
    					if (strpos($yes, $realname) === false) {
    						$yes = $yes.$realname."<br>";
    						$votes++;
    					}
    				}
    				if ($pmc_approved === false && $myrow3['status']=="-") {
    					$no = $no.$realname."<br>";
    					$votes = -1;
    					break;
    				}
    			}
    		}

    		if ($debug) {
    			echo "Debug: votes: $votes required: $nvotesRequired <br />";
    		}
    		if ($votes < 0) {
    			echo "<font color=\"red\"><b>Rejected</b></font>";
    		}
    		else if ($votes >= $nvotesRequired ||  (!($pmc_approved === false))) {
    			echo "<font color=\"green\"><b>Approved</b></font>";
    		}
    		else if ($votes > 0) {
    			echo "<font color=\"yellow\"><b>Tentatively Approved</b></font>";
    		}
    		else {
    			echo "Pending...";
    		}
    	}
    	else {
    		echo "Error occured on sqlquery3: . " . mysql_errno($dbh) . "<br />";
    		echo "sqlquery: " . mysql_info($dbh) . "<br />";
    	}
    	$rs3 = null;

    	echo "</td>";
    	$owner = $myrow['realname'];
    	if (strlen($owner) > 0) {
    		echo "<td>".$owner."</td>";
    	}
    	else {
    		echo "<td>".$myrow['login_name']."</td>";
    	}
    	echo "<td>".$yes."&nbsp;</td>";
    	echo "<td>".$no."&nbsp;</td>";
    	echo "</tr>";
    }

    $dbc->disconnect();
    $rs = null;
    $dbh = null;
    $dbc = null;
    ?>
</table>
</body>
</html>
