<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../stylesheets/wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../../../" />
		<title>WTP Milestone 9</title>
	</head>
	<body>
		<h1>SSE Test Plan</h1>
		<h2>Status of this Plan</h2>
		<p>Proposed Plan (2005-11-21)</p>

		<h2>Overall goals</h2>
		<h3><b>New for 1.0M9</b></h3>
		<p>
			Not much has been added in terms of new functionality
			for 1.0M9.  Most of the work done was in the areas of cleaning up dead code,
			and cleaning up APIs,particularly ViewerConfiguration.
			<br/><br/>
		</p>
		<h3>
			<b>Co-developer Testing</b>
		</h3>
		<p>
			We will inspect &quot;runtime&quot; version of build to be
			sure extra source is not included, and more important, we'll
			inspect and test importing SDK version to be sure all
			relevant &quot;open source&quot; is included in that SDK
			build and that it correctly imports into a development
			environment.
		</p>
		<h3>
			<b>API Testing</b>
		</h3>
		<p>
			Here in 1.0M9 we don't consider we have any official API yet
			(since not spec'd as such) but will reserve this space for
			future plans to details were API Unit tests are, their
			coverage, etc.
		</p>
		<p>
			We do have several hundred unit tests which we expect to be
			running and passing for 1.0M9, which test various aspects of
			parsing, model creation, and correct charset handling, among
			other things.
		</p>

		<h3>
			<b>End User Testing</b>
		</h3>
		<p>
			The nature of the end-user testing is intentionally planned
			to be "ad hoc" instead of specifying step by step "how to"
			directions and specific "expected results" sections often
			seen in test cases. This is done because its felt leads to
			greater number of "paths" being tested, and allows end-users
			more motivation for logging "bugs" if things didn't work as
			<i>they</i>
			expected, even if it is working as designed.
		</p>

		<p>
			As we progress through milestones, we'll add more and more
			detail for special cases, special files, special projects,
			etc.When we do have special or sample test files and
			projects, we will keep those stored in CVS, as projects
			under a
			<i>testdata</i>
			directory under the
			<i>development</i>
			directory of relevant component so that testers (from
			immediate team, or community) can easily check out into the
			environment being tested.
		</p>

		<h3>
			<b>Platform Testing</b>
		</h3>
		<p>
			While we do not have any platform specific code, or
			function, we will have some team members do end-user tests
			on Linux, some on Windows. We will also confirm unit tests
			pass on both platforms.
		</p>

		<h3>
			<b>Performance Testing</b>
		</h3>
		<p>
			We have added (some) automated performance tests along the
			lines of the Eclipse base performance unit tests in future
			milestones. These are currently in the
			<b>org.eclipse.wst.*.ui.tests.performance</b>
			and
			<b>org.eclipse.jst.jsp.ui.tests.performance</b>
			plugins.
			<br />
			<br />
			We will continue to add more test cases in upcoming
			milestones.
		</p>


		<a name="matrix" />
		<h2>Common Eclipse Source Editing Features</h2>
		<p>
			The following table represents the features for our various
			source editors that will play a role in our testing,
			according to the following legend.
		</p>

		<ul>
			<li>
				<b>Y</b>
				- Yes, should work, will be tested
			</li>
			<li>
				<b>P</b>
				- Planned for later, will not be tested this milestone
			</li>
			<li>
				<b>N</b>
				- No, no plans, does not work, will not be tested
			</li>
		</ul>

		<table border="1" align="center" width="90%" cellspacing="2"
			cellpadding="0">
			<caption align="bottom">
				Source Editing Feature Matrix
			</caption>
			<tbody>
				<tr>
					<th style="text-align: left" />
					<th>XML</th>
					<th>JSP</th>
					<th>HTML</th>
					<th>XHTML</th>
					<th>CSS</th>
					<th>DTD</th>
					<th>JavaScript</th>
					<th>JSP EL</th>
				</tr>
				<tr>
					<td style="text-align: left">Highlighting</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: blue">P</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Semantic Highlighting
					</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<!-- xml, jsp, html, xhtml, css, dtd, js, jsp el-->
				<tr>
					<td style="text-align: left">Content Assist</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Parameter/Attribute Hints
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Document/Element Formatting
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Structured Selection
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">Hover Info</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: blue">P</td>
				</tr>
				<!-- xml, jsp, html, xhtml, css, dtd, js, jsp el-->
				<tr>
					<td style="text-align: left">Smart Edit</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Open On (hyperlinks)
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">Mark Occurrences</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">Linked Edit</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Find Element Occurrences
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				
				<!-- xml, jsp, html, xhtml, css, dtd, js, jsp el-->
				<tr>
					<td style="text-align: left">
						Validate as-you-type
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Delimiter Highlighting (bracket matching)
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: blue">P</td>
				</tr>
				<tr>
					<td style="text-align: left">Refactoring</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				<tr>
					<td style="text-align: left">Search Participant</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				<tr>
					<td style="text-align: left">Quick Fix/Assist</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				
				<!-- xml, jsp, html, xhtml, css, dtd, js, jsp el-->
				<tr>
					<td style="text-align: left">Property Sheet</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">Outline View</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">Folding</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: blue">P</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Annotation Navigation (go to next annotation)
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				<tr>
					<td style="text-align: left">External Files</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				
				<!-- xml, jsp, html, xhtml, css, dtd, js, jsp el-->
				<tr>
					<td style="text-align: left">Toggle Comment</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Quick Type Hierarchy View
					</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">Quick Outline</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; color: red">N</td>
				</tr>
				<tr>
					<td style="text-align: left">
						Task Tags in Source Comments
					</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
					<td style="text-align: center; color: red">N</td>
					<td style="text-align: center; font-weight:bold; color: green">Y</td>
				</tr>
			</tbody>
		</table>
		<p></p>

		<h2>Snippets View</h2>
		<p>
			A quick test of the Snippets View will be done, to be sure
			you can drop text, its inserted at right points, etc. and
			that users can define their own. There are many changes
			planned in this area, though, so we will test just enough to
			make sure it basically works, and does no harm (note: user
			defined ones in this milestone will most likely not be
			upwardly compatible with the next milestone).
		</p>

		<h2>Source Editing Test Plans</h2>
		<p>
			<a href="../../sse/M9/milestone_test_plan.html">org.eclipse.wst.sse</a>
			<br/><a href="../../xml/M9/milestone_test_plan.html">org.eclipse.wst.xml</a>
			<br/><a href="../../html/M9/milestone_test_plan.html">org.eclipse.wst.html</a>
			<br/><a href="../../css/M9/milestone_test_plan.html">org.eclipse.wst.css</a>
			<br/><a href="../../dtd/M9/milestone_test_plan.html">org.eclipse.wst.dtd</a>
			<br/><a href="../../javascript/M9/milestone_test_plan.html">org.eclipse.wst.javascript</a>
			
			<br/><a href="../../../jst/jsp/M9/milestone_test_plan.html">org.eclipse.jst.jsp</a>	
		</p>
	</body>
</html>
