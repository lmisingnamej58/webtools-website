<?xml version="1.0" encoding="UTF-8"?>

<!--
	
	This stylesheet generates an index of all the milestone plans from the release.xml file.
	The reports/report-milestone-overview.xml file is used to check for the existence of the milestone plan
	for a given component and milestone.
	
	ChangeLog:
	
	2005-10-13: Arthur Ryman <ryman@ca.ibm.com>
	- Created
	
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" encoding="UTF-8" indent="yes" />

	<!-- plans is the list of all plans that have a location attribute -->
	<xsl:variable name="plans"
		select="document('../reports/report-milestone-overview.xml')//plan[@location]" />

	<xsl:variable name="rel" select="'../'" />

	<!-- 
		This stylesheet generates:
		/1.0/milestone-plan-index.xml
		
		The root directory for CSS and images is:
		/1.0/www/
		
		So relative to the buglist.xml document, root is:
		www
	-->

	<xsl:template match="/">

		<xsl:processing-instruction name="xml-stylesheet">
			type="text/xsl" href="stylesheets/wtp.xsl"
		</xsl:processing-instruction>

		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="release">

		<html>
			<head>
				<title>
					<xsl:value-of select="@name" />
					milestone plan index
				</title>
				<meta name="root" content="www" />
			</head>
			<body>
				<h1>milestone plan index</h1>
				<h2>Introduction</h2>
				<p>
					This page lists all the milestone plans for
					<xsl:value-of select="@name" />
					.
				</p>

				<table cellspacing="4">
					<thead>
						<tr>
							<th align="right">Component</th>
							<xsl:for-each
								select="bz_target_milestone">
								<th>
									<xsl:value-of select="@milestone" />
								</th>
							</xsl:for-each>
						</tr>
					</thead>
					<tbody>
						<xsl:for-each select="bz_component">
							<xsl:sort select="@name" />
							<xsl:variable name="component-dir"
								select="@dir" />
							<tr>
								<th align="right">
									<xsl:value-of select="@name" />
								</th>
								<xsl:for-each
									select="../bz_target_milestone">
									<td align="center">
										<xsl:variable name="plan-dir"
											select="concat($component-dir,'/',@dir)" />
										<xsl:variable name="location"
											select="$plans[contains(@location,$plan-dir)]/@location" />
										<xsl:choose>
											<xsl:when
												test="$location">
												<xsl:variable
													name="item-count"
													select="count(document($location)//item)" />
												<a
													href="{$plan-dir}/milestone_plan.html">

													<xsl:value-of
														select="$item-count" />
													<xsl:choose>
														<xsl:when
															test="$item-count = 1">
															<xsl:text>
																&#160;item
															</xsl:text>
														</xsl:when>
														<xsl:otherwise>
															<xsl:text>
																&#160;items
															</xsl:text>
														</xsl:otherwise>
													</xsl:choose>
												</a>

											</xsl:when>
											<xsl:otherwise>
												<xsl:text>n/a</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</td>
								</xsl:for-each>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>

			</body>
		</html>

	</xsl:template>

</xsl:stylesheet>
