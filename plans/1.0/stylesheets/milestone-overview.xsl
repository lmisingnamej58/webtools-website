<?xml version="1.0" encoding="UTF-8"?>

<!--

	ChangeLog:

	2005-10-01 Arthur Ryman, ryman@ca.ibm.com
	- add fragment ids to urls for items in milestone plans

	2005-09-26 Nitin Dahyabhai, nitind@us.ibm.com
	- switch back to Mxx naming scheme for milestones
	- fix rel value
	
	2005-08-24 Nitin Dahyabhai, nitind@us.ibm.com
	- switch to n.nMxx naming scheme for milestones
	
	2005-01-31 Arthur Ryman, ryman@ca.ibm.com
	- generate links to milestone_plan.html instead of milestone_plan.xml
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="html" encoding="UTF-8" indent="yes" />
	<xsl:include href="milestone-common.xsl" />

	<xsl:variable name="plans" select="document(/report/milestone[substring(@name, 2) >= substring(/report/@current-milestone, 2)]/plan/@location)/plan" />
	<xsl:variable name="rel" select="'../'" />
	<xsl:template match="report">
		<html>
			<head>
				<title>
					<xsl:value-of select="/report/title" />
				</title>
				<link href="../stylesheets/images/default_style.css" type="text/css" rel="stylesheet" />
			</head>
			<body>

				<xsl:call-template name="printHeader">
					<xsl:with-param name="rel" select="$rel" />
					<xsl:with-param name="title" select="/report/title" />
					<xsl:with-param name="description" select="/report/description" />
				</xsl:call-template>

				<table width="100%" cellspacing="0" cellpadding="4">
					<tr>
						<td height="20" bgcolor="#0080c0" colspan="2">
							<b>
								<font color="#ffffff">Status of this document</font>
							</b>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<xsl:value-of select="/report/status" />
						</td>
					</tr>
					<xsl:for-each select="milestone">                                                  
						<tr>
                            <xsl:if test="@name = /report/@current-milestone">
                                <xsl:attribute name="bgcolor">yellow</xsl:attribute>
                            </xsl:if>
							<td vAlign="top" width="3%">
								<p align="right">
									<img height="16" src="../stylesheets/images/Adarrow.gif" width="16" align="bottom" border="0" name="Image2" alt="" />
								</p>
							</td>
							<td width="97%">
								<p>
									<a href="#{@name}">
										<b>
											<xsl:value-of select="description" />
										</b>
									</a>
									<xsl:if test="statusDescription">
										-
										<xsl:copy-of select="statusDescription" />
									</xsl:if>
									<br>
										<xsl:copy-of select="contentDescription" />
									</br>
								</p>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			<xsl:for-each select="/report/milestone[substring(@name, 2) >= substring(/report/@current-milestone, 2)]">
					<xsl:sort data-type="number" select="@name" />
	 				<table width="100%">
						<tr>
							<td height="20" bgcolor="#0080c0" width="100%">
                                <a name="{@name}"/>
								<b>
									<font color="#ffffff">
										<xsl:value-of select="description" />
									</font>
								</b>
							</td>
						</tr>
						<xsl:call-template name="prioritySection">
							<xsl:with-param name="title" select="'High Priority'" />
							<xsl:with-param name="priorityValue" select="'high'" />
							<xsl:with-param name="milestoneName" select="@name" />
						</xsl:call-template>
						<xsl:call-template name="prioritySection">
							<xsl:with-param name="title" select="'Medium Priority'" />
							<xsl:with-param name="priorityValue" select="'medium'" />
							<xsl:with-param name="milestoneName" select="@name" />
						</xsl:call-template>
						<xsl:call-template name="prioritySection">
							<xsl:with-param name="title" select="'Low Priority'" />
							<xsl:with-param name="priorityValue" select="'low'" />
							<xsl:with-param name="milestoneName" select="@name" />
						</xsl:call-template>
						<!-- 
							<xsl:call-template name="prioritySection">
							<xsl:with-param name="title" select="'Unprioritized'" />
							<xsl:with-param name="priorityValue" select="'none'" />
							<xsl:with-param name="milestoneName" select="@name" />
							</xsl:call-template>
						-->
					</table>
				</xsl:for-each>
				<br />
				<br />
				<xsl:call-template name="printLegend">
					<xsl:with-param name="rel" select="$rel" />
				</xsl:call-template>
				<br />
				<br />
				<br />
				<p>
					Please see our
					<a href="http://eclipse.org/legal/privacy.html">privacy policy</a>
					and website
					<a href="http://eclipse.org/legal/termsofuse.html">terms of use</a>
					. For problems with the eclipse.org site, please contact the
					<a href="mailto:webmaster@eclipse.org">webmaster</a>
					or read the
					<a href="http://eclipse.org/webmaster/index.html">webmaster FAQ</a>
					for answers to common questions!
				</p>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="prioritySection">
		<xsl:param name="title" />
		<xsl:param name="priorityValue" />
		<xsl:param name="milestoneName" />
		<tr>
			<td>				
				<xsl:variable name="jstItems" select="$plans/component[@subproject='jst']//item[$priorityValue='all' or (not(@priority) and $priorityValue='none')or @priority=$priorityValue][ancestor::milestone/@name=$milestoneName]"></xsl:variable>
				<xsl:variable name="wstItems" select="$plans/component[@subproject='wst']//item[$priorityValue='all' or (not(@priority) and $priorityValue='none')or @priority=$priorityValue][ancestor::milestone/@name=$milestoneName]"></xsl:variable>
				<xsl:if test="count($jstItems) + count($wstItems) > 0">
					<b>
						<xsl:value-of select="$title" />
					</b>
					<ul>
						<xsl:call-template name="subprojectSection">
							<xsl:with-param name="subprojectName" select="'jst'" />
							<xsl:with-param name="priorityItems" select="$jstItems" />
						</xsl:call-template>
						<xsl:call-template name="subprojectSection">
							<xsl:with-param name="subprojectName" select="'wst'" />
							<xsl:with-param name="priorityItems" select="$wstItems" />
						</xsl:call-template>
					</ul>
				</xsl:if>
			</td>
		</tr>
	</xsl:template>

	<xsl:template name="subprojectSection">
		<xsl:param name="subprojectName" />
		<xsl:param name="priorityItems" />
		<xsl:variable name="theItems" select="$priorityItems[ancestor::component/@subproject = $subprojectName]" />
		<xsl:if test="count($theItems) > 0">
		    <table>
                <tr><td width="15" colspan="2">
				<b><xsl:value-of select="$subprojectName"/></b>
			    </td>
                <td/>
                </tr>		
				<xsl:apply-templates select="$priorityItems">
					<xsl:sort select="ancestor::component/@name" />
					<xsl:sort select="description" />
				</xsl:apply-templates>
			</table>
		</xsl:if>
	</xsl:template>


	<xsl:template match="item">
		<xsl:variable name="milestoneName" select="ancestor::component/milestone[1]/@name" />
		<xsl:variable name="component" select="ancestor::component" />
		<xsl:variable name="url" select="concat($rel, $component/@subproject, '/', $component/@name, '/', $milestoneName, '/', 'milestone_plan.html')" />
		<tr>
			<td/>
            <td width="20" align="center">
				<xsl:call-template name="drawStatus">
					<xsl:with-param name="rel" select="$rel"></xsl:with-param>
					<xsl:with-param name="value" select="@status" />
					<xsl:with-param name="defaultImage" select="'default.gif'" />
				</xsl:call-template>
            </td>  
			<td>
				<xsl:value-of select="'['" />
				<a href="{concat($url,'#',translate(concat(../@name,'.',description),' ','-'))}">
					<xsl:value-of select="$component/@name" />
				</a>
				<xsl:value-of select="'] '" />

				<xsl:copy-of select="description" />
				<xsl:if test="@helpWanted">
  				    <xsl:value-of select="'  '" />                
					<xsl:call-template name="drawHelpWanted">
						<xsl:with-param name="rel" select="$rel" />
					</xsl:call-template>
				</xsl:if>
			</td>            
		</tr>
	</xsl:template>
	
</xsl:stylesheet>