<?php
// votes required to be considered "approved"
$nvotesRequired = 3;
$targetMilestone="3.2 RC4";
$targetMilestoneJPA="2.3 RC4";
$debug = false;
// for reference
// See https://dev.eclipse.org/committers/committertools/dbo_bugs_schema.php
// and https://dev.eclipse.org/committers/committertools/dbo_index.php

if ($debug)
{
    ini_set("display_errors", "true");
    error_reporting (E_ALL);
}

require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";

$approvedList = "";
$pendingList = "";

?>
<html>
<head>
<meta
    http-equiv="Content-Type"
    content="text/html; charset=iso-8859-1">
<link
    rel="stylesheet"
    href="../../../default_style.css"
    type="text/css">
<link
    rel="stylesheet"
    href="../../wtp.css"
    type="text/css">
<title>Web Tools 3.2 Bugs</title>
</head>
<body>
<table
    width="100%"
    cellspacing="5"
    cellpadding="2"
    border="0">
    <tr>
        <td
            width="60%"
            align="left"><font class="indextop">wtp 3.2 RC4 pmc review bug list</font> <br>
        <font class="indexsub">Web Tools 3.2 RC4 Bugs for PMC Review</font></td>
        <td width="40%"><img
            width="207"
            hspace="50"
            height="129"
            align="middle"
            src="../../images/wtplogosmall.jpg"></td>
    </tr>
</table>
<table
    width="100%"
    cellspacing="5"
    cellpadding="2"
    border="0">
    <tr>
        <td
            valign="top"
            bgcolor="#0080c0"
            align="left"
            colspan="2"><b> <font
            face="Arial,Helvetica"
            color="#ffffff">Introduction</font></b></td>
    </tr>
    <tr>
        <td
            valign="top"
            align="right">&nbsp;</td>
        <td valign="top"><?php
        echo 'This document lists bugs that require PMC approval in WTP ' . $targetMilestone . ' or for Dali ' . $targetMilestoneJPA . '<br />';
        echo '    Currently there is (are) ' . $nvotesRequired . ' vote(s) required for PMC Approval during this phase of development. <br />'
        ?> <p>For details, refer to the <a href="http://wiki.eclipse.org/index.php/WTP_PMC_Defect_Review">WTP PMC Review process.</a> and 
        the <a href="http://wiki.eclipse.org/WTP_3.2_Ramp_down_Plan_for_Helios">WTP ramp down plan for Helios</a>.</p>
        <p>If a bug does not showing up in this list, that you think should show up in this list, be sure the bug's target is set appropriately, that 
        status whiteboard has "PMC" in it, and that 
        that the review flags are for "pmc_approved" (not merely the regular "review" flag). If so, and doesn't show up after several minutes (say, 30 minutes or so), 
        feel free to 
        send a note to <a href="mailto:wtp-releng@eclipse.org">wtp-releng</a> to call attention to the problem.</p> 
        </td>
    </tr>
</table>
<br>
<table
    width="100%"
    cellspacing="5"
    cellpadding="2"
    border="0">
    <tr>
        <td
            valign="top"
            bgcolor="#0080c0"
            align="left"
            colspan="2"><b> <font
            face="Arial,Helvetica"
            color="#ffffff">Defect List</font></b></td>
    </tr>
</table>
<table
    align="center"
    cellpadding="4" 
    width="80%"
    border="1">
    <tr>
        <th>Count</th>
        <th>Bug</th>
        <th>Review Status</th>
       <!-- <th>Bug Status</th>  --> 
        <th>Owner</th>
        <th>Yes</th>
        <th>No</th>
    </tr>
    <?php


    # Connect to database
    $sqlquery = "SELECT
    BUG.bug_id,
    USR.realname,
    USR.login_name,
    BUG.status_whiteboard
    FROM
    bugs AS BUG
    INNER JOIN products AS PROD ON PROD.id = BUG.product_id
    INNER JOIN profiles AS USR ON USR.userid = BUG.assigned_to
    WHERE
    BUG.status_whiteboard like '%PMC%'
    AND
    (
    (
    (BUG.target_milestone like '$targetMilestone%'
    AND BUG.target_milestone not like '$targetMilestone.%')
    AND (
           PROD.name='Web Tools'
        OR PROD.name='Java Server Faces'
        OR PROD.name='JSDT'
        OR PROD.name='WTP Common Tools'
        OR PROD.name='WTP Datatools'
        OR PROD.name='WTP EJB Tools'
        OR PROD.name='WTP Incubator'
        OR PROD.name='WTP Java EE Tools'
        OR PROD.name='WTP Releng'
        OR PROD.name='WTP ServerTools'
        OR PROD.name='WTP Source Editing'
        OR PROD.name='WTP Webservices'
        OR PROD.name='WTP Website'
        )
    )
    OR
    (
    BUG.target_milestone like '$targetMilestoneJPA%'
    AND BUG.target_milestone not like '$targetMilestoneJPA.%'
    AND PROD.name='Dali JPA Tools'
    )
    )
    ORDER BY
    BUG.bug_id";

    $dbc = new DBConnectionBugs();
    $dbh = $dbc->connect();
    $rs = mysql_query($sqlquery, $dbh);

    if(mysql_errno($dbh) > 0) {
        echo "There was an error processing this request <br />".
        $dbc->disconnect();
        exit;
    }
    $rowcount = 0;
    while($myrow = mysql_fetch_assoc($rs))
    {
        $rowcount++;
        if ($debug) {
            echo "Debug: myrow: " ;
            echo print_r($myrow) ;
            echo  " <br />";
        }
        echo "<tr>";
        echo "<td align='right'>".$rowcount."</td>";
        
        echo "<td align='center'><a href=\"https://bugs.eclipse.org/bugs/show_bug.cgi?id=".$myrow['bug_id']."\">".$myrow['bug_id']."</a></td>";
        echo "<td>";
        $yes = "";
        $no = "";
        $pmc_approved = stripos($myrow['status_whiteboard'], 'PMC_approved');
        $sqlquery3 = "SELECT
                flags.type_id,
                flags.setter_id,
                flags.status
            FROM
                flags
            WHERE
                (flags.setter_id='583'
                OR flags.setter_id='550'
                OR flags.setter_id='12789'
                OR flags.setter_id='17882'
                OR flags.setter_id='18449'
                OR flags.setter_id='26550')
                AND flags.bug_id=".$myrow['bug_id'];

        $rs3 = mysql_query($sqlquery3, $dbh);
        if(mysql_errno($dbh) <= 0)
        {
            $votes=0;
            while($myrow3 = mysql_fetch_assoc($rs3)) {
                if ($debug) {
                    echo "Debug: myrow3: " ; echo print_r($myrow3) ; echo  " <br />";
                }
                $sqlquery4 = "SELECT
                                        flagtypes.name
                                      FROM
                                        flagtypes
                                      WHERE
                                        flagtypes.name='pmc_approved'
                                        AND flagtypes.id=".$myrow3['type_id'];

                $rs4 = mysql_query($sqlquery4, $dbh);
                while ($myrow4 = mysql_fetch_assoc($rs4)) {
                    if ($debug) {
                        echo "Debug: myrow4: " ; echo print_r($myrow4) ; echo  " <br />";
                    }
                    if($myrow3['setter_id']=="583") {
                        $realname="David Williams";
                    } else if($myrow3['setter_id']=="550") {
                        $realname="Tim deBoer";
                    } else if($myrow3['setter_id']=="12789") {
                        $realname="Naci Dai";
                    } else if($myrow3['setter_id']=="17882") {
                        $realname="Neil Hauge";
                    } else if($myrow3['setter_id']=="18449") {
                        $realname="Raghu Srinivasan";
                    } else if ($myrow3['setter_id']=="26550") {
                        $realname="Kaloyan Raev";
                    } else $realname = "UNKNOWN!";

                    if ($myrow3['status']=="+") {
                        if (strpos($yes, $realname) === false) {
                            if ($votes > 0) {
                                $yes = $yes . "<br />" . $realname;
                            }
                            else {
                                $yes = $realname;
                            }
                            // don't increment, if already veto'd
                            if ($votes >= 0) {
                                $votes++;
                            }
                        }
                    }
                    else if ($pmc_approved === false && $myrow3['status']=="-") {
                        if ($vote < 0) {
                            $no = $no . "<br>" . $realname;
                        }
                        else {
                            $no = $realname;
                        }
                        $votes = -1;
                        break;
                    }
                }
            }

            if ($debug) {
                echo "Debug: votes: $votes required: $nvotesRequired <br />";
            }
            if ($votes < 0) {
                echo "<font color=\"red\"><b>Deferred</b></font>";
            }
            else if ($votes >= $nvotesRequired || (!($pmc_approved === false))) {
                if (strlen($approvedList) > 0) {
                    $approvedList=$approvedList . "," . $myrow['bug_id'];
                } else {
                    $approvedList=$myrow['bug_id'];
                }
                echo "<font color=\"green\"><b>Approved</b></font>";
            }
            else if ($votes > 0) {
                if (strlen($pendingList) > 0) {
                    $pendingList=$pendingList . "," . $myrow['bug_id'];
                } else {
                    $pendingList=$myrow['bug_id'];
                }
                // echo "<font color=\"yellow\"><b>Tentatively Approved</b></font>";
                echo "<font color=\"yellow\"><b>Pending...</b></font>";
            }
            else {
                // $votes == 0
                if (strlen($pendingList) > 0) {
                    $pendingList=$pendingList . "," . $myrow['bug_id'];
                } else {
                    $pendingList=$myrow['bug_id'];
                }
                echo "Pending...";
            }
        }
        else {
            echo "Error occured on sqlquery3: . " . mysql_errno($dbh) . "<br />";
            echo "sqlquery: " . mysql_info($dbh) . "<br />";
        }
        $rs3 = null;

        echo "</td>";
        
        
        //echo "<td>";
        //echo "bug status";
        //echo "</td>";
        
        
        $owner = $myrow['realname'];
        if (strlen($owner) > 0) {
            echo "<td>".$owner."</td>";
        }
        else {
            echo "<td>".$myrow['login_name']."</td>";
        }
        echo "<td>".$yes."&nbsp;</td>";
        echo "<td>".$no."&nbsp;</td>";
        echo "</tr>";
    }

    $dbc->disconnect();
    $rs = null;
    $dbh = null;
    $dbc = null;
    ?>
</table>
    <?php
    if (strlen($pendingList) > 0 || strlen($approvedList) > 0) {
        echo "<p>You can use";
        if (strlen($pendingList) > 0) {
            echo " this <a href=\"https://bugs.eclipse.org/bugs/buglist.cgi?bug_id=" . $pendingList ."\">pending list</a>";
        }
        if (strlen($pendingList) > 0 && strlen($approvedList) > 0) {
            echo " or ";
        }
        if (strlen($approvedList) > 0) {
            echo " this <a href=\"https://bugs.eclipse.org/bugs/buglist.cgi?bug_id=" . $approvedList ."\">approved list</a>";
        }
        echo " for above table to first list all the bugs in that table with the bugzilla web interface, and then narrow or sort the result how ever you would like.</p>";
    }
    ?>
</body>
</html>
